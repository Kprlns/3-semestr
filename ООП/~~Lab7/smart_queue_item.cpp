#include "smart_queue_item.h"
#include "../Lab1/figure.h"
#include "../Lab6/Tree.h"
#include "../Lab5/smart_queue_item.h"


template<class T>
SmartQueueItem<T>::SmartQueueItem(/*std::shared_ptr<T>& figure*/) {
    this->item = std::shared_ptr< Tree< std::shared_ptr<T> > > (new Tree<std::shared_ptr< T > >);
    this->next = nullptr;
}
template<class T>
SmartQueueItem<T>::~SmartQueueItem() {}

template<class T>
 SmartQueueItem<T>* SmartQueueItem< T >::GetNext() {
    return next->item;
}
template<class T>
std::shared_ptr< Tree< std::shared_ptr<T> > > SmartQueueItem< T > ::GetItem() const{
    return item;
}
template<class T>
void SmartQueueItem<T>::SetLast(SmartQueueItem< Tree<T> >* set) {
    this->next=set;
}

template<class T>
std::ostream& operator<<(std::ostream& out, const SmartQueueItem< T >& it) {
    std::shared_ptr<Figure> f = it.item->node;
    f->Print();
}

template <class T>
void* SmartQueueItem<T>::operator new(size_t size) {
    //std::cout <<"weofjnelw";
    return ItemAllocator.Allocate();
}

template <class T>
void SmartQueueItem<T>::operator delete(void* ptr) {
    ItemAllocator.Deallocate(ptr);
}

template <class T>
void SmartQueueItem<T>::Print() {
    this->item->Print();
}

#include "../Lab1/figure.h"
template class SmartQueueItem< Tree< std::shared_ptr<Figure> > >;
template  std::ostream& operator<<(std::ostream& out,const SmartQueueItem< Tree< std::shared_ptr<Figure> > >& obj);