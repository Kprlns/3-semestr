#include <iostream>
#include "smart_queue.h"
#include "iterator.h"
#include "../Lab6/smart_queue.h"

template<class T>
SmartQueue<T>::SmartQueue(int max): head(nullptr), tail(nullptr), maxInTree(max) {}

template<class T>
SmartQueue<T>::~SmartQueue() {}

template<class T>
std::shared_ptr< SmartQueueItem<T> > SmartQueue<T>::GetHead() const {
    return head;
}

template<class T>
bool SmartQueue<T>::Empty() const {
    return ((head == nullptr) && (tail == nullptr));
}

template<class T>
void SmartQueue<T>::push(std::shared_ptr<T>&& figure) {

    //std::shared_ptr< SmartQueueItem<T> > it(new SmartQueueItem<T>(figure));
    if(this->Empty()){
        std::shared_ptr< SmartQueueItem < Tree<T> > > newItem(new SmartQueueItem< Tree<T> >);
        head->SetLast(newItem);
        tail->SetLast(newItem);

        tail->SetLast(nullptr);

        tail->item->Insert(figure);
    }
    else {
        if(tail->item->children_size < maxInTree) {
            tail->item->Insert(figure);
        }
        else {
            std::shared_ptr< SmartQueueItem < Tree<T> > > newItem(new SmartQueueItem< Tree<T> >);
            tail->SetLast(newItem);
            tail->GetNext();
            tail->item->Insert(figure);
        }
    }
}

template<class T>
std::shared_ptr<T> SmartQueue<T>::first(){
    return head->item->node;
}
template<class T>
std::shared_ptr<T> SmartQueue<T>::pop() {
    std::shared_ptr<T> it;

    if( head->item->children_size == 0 && !head->item->empty ) {
        it=head->item->node;
        if(head == tail) {
            head = nullptr;
            tail = nullptr;
        }
        else {
            head = head->next;
        }
    }
    else {
        it=head->item->Pull(0);
    }

    return it;
}

template<class T>
std::ostream& operator<<(std::ostream& out, const SmartQueue<T>& queue) {
    if(queue.Empty()) {
        out << "Queue is Empty\n\n";
    } else {
        std::shared_ptr< SmartQueueItem<T> > iterator(queue.head);
        while(iterator->next != nullptr) {
            iterator->Print();
            out << "\n";
            iterator=iterator->next;
        }
        iterator->Print();
        out << "\n\n";
    }
    return out;
}
template<class T>
void SmartQueue<T>::del() {
    while(!this->Empty()) {
        this->pop();
    }

}

template<class T> Iterator <SmartQueueItem<T>, T> SmartQueue<T>::begin() {
    return Iterator<SmartQueueItem< Tree< std::shared_ptr<T> > >, T>(head);
}

template <class T> Iterator<SmartQueueItem<T>, T> SmartQueue<T>::end() {
    return Iterator<SmartQueueItem<Tree< std::shared_ptr<T> >>, T>(nullptr);
}


#include "../Lab1/figure.h"
template class SmartQueue<  Tree< std::shared_ptr<Figure> > >;
template  std::ostream& operator<<(std::ostream& out,const SmartQueue<Tree< std::shared_ptr<Figure> > >& obj);