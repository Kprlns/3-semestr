#ifndef SMART_QUEUE_ITEM
#define SMART_QUEUE_ITEM

#include <memory>
#include "Tree.h"
#include "../Lab1/figure.h"
#include "../Lab1/pentagon/pentagon.h"
#include "../Lab1/rhombus/rhombus.h"
#include "../Lab1/trapeze/trapeze.h"
#include "AllcationBlock.h"
template<class T>
class SmartQueueItem {

public:
    SmartQueueItem(/*std::shared_ptr<T>& figure*/);
    virtual ~SmartQueueItem();

    std::shared_ptr< SmartQueueItem<T> > GetNext();

    std::shared_ptr<T> GetItem() const;

    void Print(std::shared_ptr<Figure>& ptr);

    void SetLast( SmartQueueItem<T>* set);


    void* operator new(size_t size);
    void operator delete(void* ptr);
    static AllocationBlock ItemAllocator;

    const int maxTreeSize = 3;
    Tree<std::shared_ptr< Figure > >* item;
    SmartQueueItem<T>* next;
};


template <class T> AllocationBlock
        SmartQueueItem<T>::ItemAllocator(sizeof(SmartQueueItem<T>), 5);


#endif