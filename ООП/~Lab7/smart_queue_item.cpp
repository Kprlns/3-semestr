#include "smart_queue_item.h"
#include "../Lab1/figure.h"


template<class T>
SmartQueueItem<T>::SmartQueueItem(/*std::shared_ptr<T>& figure*/) {
    this->item = new Tree< std::shared_ptr<T> >;

    this->next = nullptr;
}
template<class T>
SmartQueueItem<T>::~SmartQueueItem() {}

template<class T>
std::shared_ptr< SmartQueueItem<T> > SmartQueueItem<T>::GetNext() {
    return next;
}
template<class T>
std::shared_ptr<T> SmartQueueItem<T>::GetItem() const{
    return item->node;
}
template<class T>
void SmartQueueItem<T>::SetLast( SmartQueueItem<T>*  set) {
    this->next=set;
}

template <class T> void SmartQueueItem<T>::Print(std::shared_ptr<Figure>& ptr) {
    ptr->Print();
};


template<class T>
std::ostream& operator<<(std::ostream& out, const SmartQueueItem<T>& it) {
    //std::shared_ptr<Figure> f = it.GetItem();
    //f->Print();
    it.item->node->Print();
}

template <class T>
void* SmartQueueItem<T>::operator new(size_t size) {
    //std::cout <<"weofjnelw";
    return ItemAllocator.Allocate();
}

template <class T>
void SmartQueueItem<T>::operator delete(void* ptr) {
    ItemAllocator.Deallocate(ptr);
}


#include "../Lab1/figure.h"
template class SmartQueueItem<Figure>;
template  std::ostream& operator<<(std::ostream& out,const SmartQueueItem<Figure>& obj);