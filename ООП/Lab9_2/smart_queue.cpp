#include <iostream>
#include "smart_queue.h"
#include "iterator.h"


template<class T>
int SmartQueue<T>::GetSize() {
    return size;
}
template<class T>
SmartQueue<T>::SmartQueue(): head(nullptr), tail(nullptr), size(0) {}

template<class T>
SmartQueue<T>::~SmartQueue() {}

template<class T>
std::shared_ptr< SmartQueueItem<T> > SmartQueue<T>::GetHead() const {
    return head;
}

template<class T>
bool SmartQueue<T>::Empty() const {
    return (size == 0);
}

template<class T>
void SmartQueue<T>::push(std::shared_ptr<T>& figure) {
    std::shared_ptr< SmartQueueItem<T> > it;
    try {
        it = std::shared_ptr<SmartQueueItem<T> > (new SmartQueueItem<T>);
    }
    catch(std::bad_alloc) {
        std::cerr << "Memory limit\n";
        return;
    }

    if(it == nullptr) {
        std::cout << "ALARM\n";
        exit(0);
    }
    if(this->Empty()){
        head=it;
    }
    else if(size == 1) {
        tail = it;
        head->next = tail;
        it->next = nullptr;
    }
    else if(size > 1) {
        tail->next = it;
        tail = tail->next;
    }
    size++;
}
template<class T>
std::shared_ptr<T> SmartQueue<T>::first(){
    return head->GetItem();
}
template<class T>
std::shared_ptr<T> SmartQueue<T>::pop() {
    std::shared_ptr<T> it;
    if(this->Empty()) {
        std::cout << "ALARM\n";
        exit(0);
    }
    if(head == nullptr) {
        std::cout << "ALARM1\n";
    }

    if(size == 1) {
        it=head->item;
        //head = nullptr;
    }
    else if(size > 1) {
        it=head->item;
        head = head->next;
    }
    size--;
    return it;
}

template<class T>
std::ostream& operator<<(std::ostream& out, const SmartQueue<T>& queue) {
    if(queue.Empty()) {
        out << "Queue is Empty\n\n";
    } else {
        int cnt = 1;
        std::shared_ptr< SmartQueueItem<T> > iterator(queue.GetHead());
        while(iterator->GetNext() != nullptr) {
            std::cout << cnt <<"\n";
            iterator->GetItem()->Print();
            out << "\n";
            iterator=iterator->GetNext();
            cnt++;
        }
        iterator->GetItem()->Print();
        out << "\n\n";
    }
    return out;
}
template<class T>
void SmartQueue<T>::del() {
    while(!this->Empty()) {
        this->pop();
    }

}

template<class T> Iterator <SmartQueueItem<T>, T> SmartQueue<T>::begin() {
    return Iterator<SmartQueueItem<T>, T>(head);
}

template <class T> Iterator<SmartQueueItem<T>, T> SmartQueue<T>::end() {
    return Iterator<SmartQueueItem<T>, T>(nullptr);
}

template <class T>
void SmartQueue<T>::sort() {
    if(this->size > 1) {
        std::shared_ptr<T> middle = this->pop();
        SmartQueue<T> left, right;

        while(!this->Empty()) {
            std::shared_ptr<T> item = this->pop();
            if(item->Square() < middle->Square()) {
                left.push(item);
            }
            else {
                right.push(item);
            }
        }

        left.sort();
        right.sort();

        while(!left.Empty()) {
            std::shared_ptr<T> it = left.pop();
            this->push(it);
        }
        this->push(middle);
        while(!right.Empty()) {
            std::shared_ptr<T> it = right.pop();
            this->push(it);
        }
    }
}

template <class T>
void SmartQueue<T>::parallelSort() {
    if(this->size <= 1) {
        return;
    }
        std::shared_ptr< SmartQueueItem<T> > tmp = this->head;
        std::shared_ptr<T> middle = this->pop();
        SmartQueue<T> left, right;

        while(!this->Empty()) {
            std::shared_ptr<T> it = this->pop();
            if(it->Square() < middle->Square()) {
                left.push(it);
            }
            else {
                right.push(it);
            }
        }
        if(left.GetSize() > 1) {
            std::future<void> left1 = std::async(std::launch::async, &SmartQueue<T>::parallelSort, &left);
            left1.get();
        }
        if(right.GetSize() > 1) {
            std::future<void> left2 = std::async(std::launch::async, &SmartQueue<T>::parallelSort, &right);
            left2.get();
        }



        while(!left.Empty()) {
            std::shared_ptr<T> it = left.pop();
            this->push(it);
        }
        this->push(middle);


        while(!right.Empty()) {
            std::shared_ptr<T> it = right.pop();
            this->push(it);
        }

}


#include "../Lab7/figure.h"
template class SmartQueue<Figure>;
template  std::ostream& operator<<(std::ostream& out,const SmartQueue<Figure>& obj);