cmake_minimum_required(VERSION 3.6)
project(OOP)


set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++11)



set ( ZMQ_LIBRARIES ${ZMQ_LIBRARY} )
set ( ZMQ_INCLUDE_DIRS ${ZMQ_INCLUDE_DIR} )

set(KMP ДА/Lab4/KMP/KMP.cpp)
set(OCLab5 ./ОС/Lab5/main1.cpp)
set(Serv ./ОС/Lab6/server.c)
set(Cl ./ОС/Lab6/client.c)
set(KPCl ./ОС/KP/client.c)
set(KPServ ./ОС/KP/server.c)
set(tes ./ОС/KP/test.c)

set(SufTr ./ДА/Lab5/main.cpp ./ДА/Lab5/SuffixTree.cpp)

add_executable(KMP ${KMP})
add_executable(OC5 ${OCLab5})
add_executable(Server ${Serv})
add_executable(Client ${Cl})
add_executable(KPClient ${KPCl})
add_executable(KPServer ${KPServ})
add_executable(tes ${tes})
add_executable(SufTree ${SufTr})