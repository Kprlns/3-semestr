#include <iostream>
#include <stdlib.h>

struct Pair {
    int index;
    unsigned long long value;
};

class TVector {

public:
    TVector();
    TVector(int cap);

    void Read(std::istream& is);
    void Print();
    void Resize();
    //void Resize(int cap);
    TVector* Sort();

    ~TVector();

private:
    int capacity;
    int amount;
    Pair* data;
};
