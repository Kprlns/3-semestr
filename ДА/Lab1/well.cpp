#include <iostream>
#include <stdlib.h>

const int MAX_INDEX=1000000;
const double MULTIPLIER=1.5;

void *safe_realloc(void* ptr, size_t size) {
    void* newptr = realloc(ptr,size);
    if(NULL == newptr) {
        free(ptr);
    }
    return newptr;
}


struct Pair {
    int index;
    unsigned long long value;
};

struct Vector {
    int amount;
    int capacity;
    Pair* data;
};

void Create(Vector* v) {
    v->amount=0;
    v->capacity=10;
    v->data = (Pair*)malloc(v->capacity * sizeof(Pair));
}

void Resize(Vector* v) {
    v->capacity = (int)(v->capacity * MULTIPLIER);

    v->data = (Pair*)safe_realloc(v->data,v->capacity * sizeof(Pair));

}

void Read(Vector* v) {
    int i=0;
    while( std::cin >> v->data[i].index >> v->data[i].value ) {
        //std::cout <<i <<"$ " <<v->amount <<"$ ";
        i++;
        v->amount++;
        if( (v->amount-1) == v->capacity) {
            Resize(v);
        }
    }
}

void Sort(Vector* v) {
    std::cout <<"kek";
    int counter[MAX_INDEX] = {0};
    for(int i = 0;i < v->amount; i++) {
        counter[ v->data[i].index ]++;
    }

    for( int i = 1; i < MAX_INDEX; i++ ) {
        counter[i] += counter[i-1];
    }

    Pair* output = (Pair*)malloc(v->amount * sizeof(Pair));
    //Pair output[v->amount];
    for(int i = v->amount - 1; i >= 0 ; i++) {

        output[counter[v->data[i].index]-1].index = v->data[i].index;
        output[counter[v->data[i].index]-1].value = v->data[i].value;
        counter[v->data[i].index]--;
    }

    for(int i = 0; i < v->amount; i++) {

        int k = 0; // количество значащих разрядов в индексе
        int ind = v->data[i].index;
        while(ind) {
            ind /= 10;
            k++;
        }

        for(int j = k;j < 6;j++) {
            std::cout << "0";
        }
        if(v->data[i].index != 0) {
            std::cout  << v->data[i].index;
        }
        std::cout << '\t' << v->data[i].value << '\n';
    }
    free(output);
}

/*void Delete(Vector* v) {
    free(v->data);

}*/

int main() {
    Vector* vector= (Vector*)malloc(sizeof(vector));

    Create(vector);

    Read(vector);
    std::cout <<"kek";
    Sort(vector);
    //Delete(vector);
    //free(vector->data);
    free(vector);
    return 0;
}