#include <iostream>
#include <stdlib.h>
#include "vector.cpp"

int main() {
    TVector* v=new TVector();
    v->Read(std::cin);
    v->Print();

    std::cout << '\n' << '\n';

    TVector* v1=v->Sort();
    v1->Print();
    delete v1;
}