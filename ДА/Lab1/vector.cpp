#include <stdlib.h>
#include <iostream>
#include "vector.h"

TVector::TVector() {
    capacity=10;
    amount=0;
    data=(Pair*)malloc(capacity*sizeof(Pair));
}

TVector::TVector(int cap) {
    capacity=cap;
    amount=cap;
    data=(Pair*)malloc(capacity* sizeof(Pair));
}

void TVector::Resize() {
    float k=1.05+(41/(2*capacity+1));
    capacity=(int)(capacity*k);
    data=(Pair*)realloc(data,capacity);
}

/*void TVector::Resize(int cap) {
    capacity=cap;
    data=(Pair*)realloc(data,capacity);
}*/

void TVector::Print() {
    for (int i = 0; i<amount; i++) {
        std::cout << data[i].index << '\t' << data[i].value << '\n';
    }
}

void TVector::Read(std::istream &is) {
    Pair read;
    while(is >> read.index >> read.value) {
        if(amount==capacity-1) {
            this->Resize();
        }
        data[amount]=read;
        std::cout << "(" <<data[amount].index <<" " <<data[amount].value <<")" <<'\n' <<'\n' ;
        amount++;
    }
}

TVector::~TVector() {
    free(data);
    //std::cout <<"success";
}

TVector* TVector::Sort() {
    TVector* output=new TVector(this->amount);
    //Pair* output=(Pair*)malloc(capacity*sizeof(Pair));
    Pair swap;
    int count[1000000];
    for(int i=0;i<1000000;i++) {
        count[i]=0;
    }

    for (int i=0;i <this->amount-1;++i) {
        count[this->data[i].index]++;
    }

    for (int i=1;i<1000000;++i) {
        count[i]+=count[i-1];
    }

    for(int i=this->amount-1;i>=0;i--) {
        output->data[count[this->data[i].index]]=this->data[i];
        count[this->data[i].index]--;
    }
    //free(data);
    //this->data=output;
    this->~TVector();
    return output;
}