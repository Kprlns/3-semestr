#include <iostream>
#include <stdlib.h>

const int MAX_INDEX=1000000;
const double MULTIPLIER=1.5;

void *safe_realloc(void* ptr, size_t size) {
    void* newptr=realloc(ptr,size);
    if(NULL == newptr) {
        free(ptr);
    }
    return newptr;
}


struct Pair {
    int index;
    unsigned long long value;
};

class TVector {

public:
    TVector();
    TVector(int cap);

    void Read(std::istream& is);
    void Print();
    void Resize();
    //void Resize(int cap);
    void Sort();

    ~TVector();

private:
    int capacity;
    int amount;
    Pair* data;
};


TVector::TVector() {
    capacity=10;
    amount=0;
    data = (Pair*)malloc(capacity*sizeof(Pair));
}

TVector::TVector(int cap) {
    capacity=cap;
    amount=cap;

    data = (Pair*)malloc(capacity* sizeof(Pair));
}

void TVector::Resize() {

    capacity = (int)(capacity * MULTIPLIER);
    data = (Pair*)safe_realloc(data,capacity);

}

/*void TVector::Resize(int cap) {
 capacity=cap;
 data=(Pair*)realloc(data,capacity);
 }*/

void TVector::Print() {
    for (int i = 0; i < amount; i++) {

        int k = 0; // количество значащих разрядов в индексе
        int ind = data[i].index;
        while(ind) {
            ind /= 10;
            k++;
        }

        for(int j = k;j < 6;j++) {
            std::cout << "0";
        }

        if(data[i].index != 0) {
            std::cout  << data[i].index;
        }
        std::cout << '\t' << data[i].value << '\n';
    }
}

void TVector::Read(std::istream &is) {
    while(is >> data[amount].index >> data[amount].value) {
        if(amount == capacity-1) {
            this->Resize();
        }
        //data[amount]=read;
        //std::cout << "(" <<data[amount].index <<" " <<data[amount].value <<")" <<'\n' <<'\n' ;
        amount++;
    }
}

TVector::~TVector() {
    free(data);
    //std::cout <<"success";
}

void TVector::Sort() {
    //TVector* output = new TVector(this->amount);
    Pair* output=(Pair*)malloc(capacity*sizeof(Pair));
    //Pair swap;
    int count[MAX_INDEX] = {0};



    for (int i = 0;i < this->amount;++i) {
        count[this->data[i].index]++;
    }

    for (int i = 1;i < MAX_INDEX;++i) {
        count[i] += count[i-1];
    }

    for(int i = this->amount - 1;i >= 0;i--) {
        output[count[this->data[i].index]-1].index = this->data[i].index;
        output[count[this->data[i].index]-1].value = this->data[i].value;
        count[this->data[i].index]--;
    }
    //free(data);
    //this->data=output;
    for (int i = 0; i < amount; i++) {

        int k = 0; // количество значащих разрядов в индексе
        int ind = output[i].index;
        while(ind) {
            ind /= 10;
            k++;
        }
        //std::cout << "$" << k << "$";
        for(int j = k;j < 6;j++) {
            std::cout << "0";
        }

        if(output[i].index != 0) {
            std::cout  << output[i].index;
        }
        std::cout << '\t' << output[i].value << '\n';
    }

    free(output);
    //std::cout << "$$$\n\n\n\n";


}

int main() {
    TVector* v = new TVector();
    v->Read(std::cin);

    //v->Print();

    //std::cout << '\n' << '\n';
    //std::cout << "kek";
    //TVector* v1 = new TVector();
    v->Sort();
    std::cout << "$$$\n\n\n\n\n";
    //delete v;
    //v1->Print();
    //delete v;
    delete v;
}