#include <iostream>
#include <stdlib.h>

const size_t START_SIZE = 0;
const size_t START_MAX_SIZE = 1;
const size_t MAX_NUMBER_IN_RANGE=65536;

struct TElement {
    int key;
    char value[65];
};
char* StrCopy(char* dest,const char* src) {
    char* temp = dest;
    int i;
    for (i = 0; src[i]!='\0'; i++) {
        dest[i] = src[i];
    }
    for(;i<64;i++){
        dest[i]=0;
    }
    temp[64]='\0';
    return temp;
}
void PrintArray(TElement *array, int size) {
    for (int i = 0; i < size ; i++) {
        std::cout << array[i].key << '\t' << array[i].value << std::endl;
    }
}
void Zeroing(int *array, int size) {
    for (int i = 0; i < size; i++) {
        (array)[i] = 0;
    }
}
TElement* CountingSort(TElement *array, int size) {
    int* counting = (int*) malloc(sizeof(size_t) * MAX_NUMBER_IN_RANGE);
    Zeroing(counting, MAX_NUMBER_IN_RANGE);
    for (int i = 0; i < size; i++) {
        counting[array[i].key]++;
    }
    for (int i = 1; i < MAX_NUMBER_IN_RANGE; i++) {
        counting[i] += counting[i - 1];
    }
    TElement* sortedArray = (TElement*) malloc(sizeof(TElement) * (size));
    for (int i = size - 1; i >= 0; i -= 1) {
        int position = counting[array[i].key] - 1;
        sortedArray[position] = array[i];
        counting[array[i].key]--;
    }
    free(counting);
    return sortedArray;
}
TElement* ReallocArray(TElement* poiner,int size) {
    TElement* newptr = (TElement*) realloc(poiner, sizeof(TElement) * size);
    if (NULL == newptr) {
        free(poiner);
    }
    return newptr;
}
int main() {
    int key;
    char value[64];
    int currentSize = START_SIZE;
    int maxSize = START_MAX_SIZE;
    TElement* array;
    array = (TElement*) malloc(sizeof(TElement) * maxSize);
    if (array == NULL) {
        std::cout << "ERROR: Cannot allocate memory for start array" << std::endl;
        free(array);
        return 0;
    }
    while (std::cin >> key) {
        std::cin.ignore(1, '\t');
        std::cin >> value;
        array[currentSize].key = key;
        StrCopy(array[currentSize].value , value);
        currentSize++;
        if (currentSize >= maxSize - 1) {
            maxSize = maxSize + (maxSize >> 1) + 1;
            array = ReallocArray(array, maxSize);//(TElement*) realloc(array, sizeof(TElement) * maxSize);
        }
        if (array == NULL) {
            std::cout << "ERROR:Cannot allocate more memory for array";
            return 0;
        }
    }
    TElement* sortedArray = CountingSort(array, currentSize);
    PrintArray(sortedArray, currentSize);
    free(sortedArray);
    free(array);
    return 0;
}