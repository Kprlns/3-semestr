#include <iostream>
#include <stdlib.h>

const int MAX_INDEX = 1000000;
const int INITIAL_VECTOR_AMOUNT = 0;
const int INITIAL_VECTOR_SIZE = 20;

struct TPair {
    int index;
    unsigned long long value;
};

TPair* Realloc(TPair* pointer, int size) {

    TPair* new_pointer = (TPair*)realloc(pointer, size * sizeof(TPair));
    if (new_pointer == NULL) {
        free(pointer);
    }
    return new_pointer;
}

int main() {

    int amount = INITIAL_VECTOR_AMOUNT;
    int capacity = INITIAL_VECTOR_SIZE;

    TPair* vector = (TPair*)malloc(INITIAL_VECTOR_SIZE * sizeof(TPair));

    if ( vector == NULL) {
        std::cout << "Error\n";
        return 0;
    }

    //int counter[MAX_INDEX] = {0};
    int *counter = (int*)malloc(MAX_INDEX * sizeof(int));
    while(std::cin >> vector[amount].index >> vector[amount].value) {
    //чтение
        counter[ vector[amount].index ]++;
        amount++;
        if ( amount >= capacity - 1 ) {

            capacity += capacity ;
            vector = Realloc(vector,capacity);
        }
        if (vector == NULL) {
            std::cout <<"Error\n";
            return 0;
        }
    }

    for (int i = 1; i < MAX_INDEX; ++i) {
        //префикс-сумма
        counter[i] += counter[i-1];
    }

    TPair* sortedVector = (TPair*)malloc(amount * sizeof(TPair));
    for (int i = amount - 1; i >= 0; --i) {
        //сортировка
        counter[ vector[i].index ]--;
        sortedVector[ counter[vector[i].index] ].index = vector[i].index;
        sortedVector[ counter[vector[i].index] ].value = vector[i].value;

    }
    free(vector);

    for (int i = 0; i < amount; ++i) {

        int k = 0; // количество значащих разрядов в индексе
        int ind = sortedVector[i].index;
        while( ind != 0 ) {
            ind /= 10;
            k++;
        }

        for(int j = k;j < 6;j++) {
            std::cout << "0";
        }

        if(sortedVector[i].index != 0) {
            std::cout << sortedVector[i].index;
        }

        //<< sortedVector[i].index
        std::cout << '\t' <<  sortedVector[i].value << '\n';
    }
    free(sortedVector);
}