#include <iostream>
#include <stdlib.h>

const int MAX_INDEX=1000000;
void *safe_realloc(void* ptr, size_t size) {
    void* newptr=realloc(ptr,size);
    if(NULL == newptr) {
        free(ptr);
        std::cout << "Unsuccesful reallocate";
    }
    return newptr;
}


struct Pair {
    int index;
    unsigned long long value;
};

class TVector {

public:
    TVector();
    TVector(int cap);

    void Read(std::istream& is);
    void Print();
    void Resize();
    //void Resize(int cap);
    void Sort();

    ~TVector();

private:
    int capacity;
    int amount;
    Pair* data;
};


TVector::TVector() {
    capacity = 10;
    amount=0;
    data = (Pair*)malloc(capacity*sizeof(Pair));
}

TVector::TVector(int cap) {
    capacity = cap;
    amount = cap;

    data = (Pair*)malloc(capacity* sizeof(Pair));
}

void TVector::Resize() {

    float k = 1.05 + (41/(2*capacity+1));
    capacity = (int)(capacity*k);
    data = (Pair*)safe_realloc(data,capacity);

}

/*void TVector::Resize(int cap) {
    capacity=cap;
    data=(Pair*)realloc(data,capacity);
}*/

void TVector::Print() {
    for (int i = 0; i < amount; i++) {

        int k = 0; // количество значащих разрядов в индексе
        int ind = data[i].index;
        while(ind) {
            ind /= 10;
            k++;
        }
        //std::cout <<"$" << k << "$";
        for(int j = k;j < 6;j++) {
            std::cout << "0";
        }
        if(data[i].index != 0) {
            std::cout  << data[i].index;
        }
        std::cout << '\t' << data[i].value << '\n';
    }
}

void TVector::Read(std::istream &is) {
    //Pair read;
    while(is >> data[amount].index >> data[amount].value /*is >>read >>*/) {
        //std::cout << "$" <<capacity <<"$";
        if(amount == capacity) {
            this->Resize();
        }
        //data[amount]=read;
        //std::cout << "(" <<data[amount].index <<" " <<data[amount].value <<")" <<'\n' <<'\n' ;
        amount++;

    }
}

TVector::~TVector() {
    free(data);
    //std::cout <<"success";
}

/*void TVector::Sort() {
    //TVector* output = new TVector(this->amount);
    Pair* output=(Pair*)malloc(capacity*sizeof(Pair));
    //Pair swap;
    int count[1000000] = {0};


    for (int i = 0;i < this->amount;++i) {
        count[this->data[i].index]++;
    }

    for (int i = 1;i < 1000000;++i) {
        count[i] += count[i-1];
    }

    for(int i = this->amount - 1;i>=0;i--) {
        output[count[data[i].index]-1].index = data[i].index;
        output[count[data[i].index]-1].value = data[i].value;
        count[this->data[i].index]--;
    }
    //std::cout <<"blabla";
    free(data);
    this->data=output;

    //return output;
}*/

void TVector::Sort() {

    Pair* output = (Pair*)malloc(capacity * sizeof(Pair));

    int counter[MAX_INDEX] = {0};

    for(int i = 0; i < this->amount; i++ ) {
        counter[this->data[i].index]++;
    }

    for(int i = 1; i < MAX_INDEX ; i++) {
        counter[i] += counter[i-1];
    }

    for(int i = this->amount - 1; i >= 0; i++ ) {
        counter[this->data[i].index]--;
        output[counter[this->data[i].index]].index = this->data[i].index;
        output[counter[this->data[i].index]].value = this->data[i].value;

    }

    /*for(int i = 0; i < amount; i++) {
        std::cout << setfill('0') << setw(6) << data[i].index << '\t' << data[i].value << '\n';
    }*/
    for (int i = 0; i < amount; i++) {

        int k = 0; // количество значащих разрядов в индексе
        int ind = data[i].index;
        while(ind) {
            ind /= 10;
            k++;
        }
        //std::cout <<"$" << k << "$";
        for(int j = k;j < 6;j++) {
            std::cout << "0";
        }
        if(data[i].index != 0) {
            std::cout  << data[i].index;
        }
        std::cout << '\t' << data[i].value << '\n';
    }

}


int main() {
    TVector* v = new TVector();
    v->Read(std::cin);

    //v->Print();

    //std::cout << '\n' << '\n';

    //TVector* v1 = new TVector();

    v->Sort();
    //delete v;


    //v->Print();
    //delete v;
    delete v;
}


