#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <iomanip>

const int MAX_INDEX = 1000000;
const int INITIAL_VECTOR_AMOUNT = 0;
const int INITIAL_VECTOR_SIZE = 20;


struct TPair {
    int index;
    unsigned long long value;
};

TPair* Realloc(TPair* pointer, int size) {

    TPair* new_pointer = (TPair*)realloc(pointer, size * sizeof(TPair));
    if (new_pointer == NULL) {
        free(pointer);
    }
    return new_pointer;
}



int main()
{

    int amount = INITIAL_VECTOR_AMOUNT;
    int capacity = INITIAL_VECTOR_SIZE;

    TPair* vector = (TPair*)malloc(INITIAL_VECTOR_SIZE * sizeof(TPair));

    if ( vector == NULL) {
        std::cout << "Error\n";
        return 0;
    }

    int counter[MAX_INDEX] = {0};
    while(std::cin >> vector[amount].index >> vector[amount].value) {
        //чтение
        counter[ vector[amount].index ]++;
        amount++;
        if ( amount >= capacity - 1 ) {

            capacity += capacity ;
            vector = Realloc(vector,capacity);
        }
        if (vector == NULL) {
            std::cout <<"Error\n";
            return 0;
        }
    }
    int t1=clock();
    for (int i = 1; i < MAX_INDEX; ++i) {
        //префикс-сумма
        counter[i] += counter[i-1];
    }

    TPair* sortedVector = (TPair*)malloc(amount * sizeof(TPair));
    for (int i = amount - 1; i >= 0; --i) {
        //сортировка
        counter[ vector[i].index ]--;
        sortedVector[ counter[vector[i].index] ].index = vector[i].index;
        sortedVector[ counter[vector[i].index] ].value = vector[i].value;

    }
    int t2 = clock();
    free(sortedVector);


    /*for (int i = 0; i < amount; ++i) {

        int k = 0; // количество значащих разрядов в индексе
        int ind = sortedVector[i].index;
        while( ind != 0 ) {
            ind /= 10;
            k++;
        }

        for(int j = k;j < 6;j++) {
            std::cout << "0";
        }

        if(sortedVector[i].index != 0) {
            std::cout  << sortedVector[i].index;
        }

        //<< sortedVector[i].index
        std::cout << '\t' <<  sortedVector[i].value << '\n';
    }*/



    std::cout << "Linear sort: " <<std::setprecision(6) << ((t2-t1) / ((double)CLOCKS_PER_SEC)) <<" sec.\n";


    std::vector < std::vector<int> > vec;
    int ind;
    unsigned long long val;
    for(int i = 0; i < amount; ++i) {
        std::vector<int> pair(2);

        pair[0] = vector[i].index;
        pair[1] = vector[i].value;
        vec.push_back(pair);

    }
    free(vector);
    int t3 = clock();
    std::stable_sort(vec.begin(),vec.end());
    int t4 = clock();
    std::cout << "Std stable sort: " <<std::setprecision(6) << ((t4-t3) / ((double)CLOCKS_PER_SEC)) << " sec.\n";
    /*for(int i = 0; i < vec.size(); ++i) {
        std::cout << vec[i][0] <<'\t' <<vec[i][1] << '\n';
    }*/


}