#ifndef VECTOR_H
#define VECTOR_H
#include <cstdlib>
const size_t START_SIZE = 4;
template <typename T>
class TVector {
    size_t size;
    size_t capacity;
    T* vector;
public:
    TVector(size_t cap=START_SIZE): size(0), capacity(cap), vector(new T[capacity]) {

    }

    bool Empty() const {
        return size == 0;
    }

    void ReNew() {
        delete[] vector;
        size = 0;
        capacity = START_SIZE;
        vector = new T[capacity];
    }
    size_t Size() const {
        return size;
    }

    void AddLast(T& data) {
        if (size + 1 >= capacity) {
            Resize(size + 1);
        }

        vector[size] = data;
        size++;
    }

    void Insert(size_t pos, T& data) {

        if(!(pos < 0 || pos > size)) {
            if(size + 1 > capacity) {
                Resize(size + 1);
            }

            for(size_t i = size; i > pos; i--) {
                vector[i] = vector[i - 1];
            }

            vector[pos] = data;
            size++;
        }
    }
    T& DelLast() {
        --size;
        T& buf = vector[size];
        return buf;
    }
    T& Pull(size_t pos) {
        T& buf = vector[pos];
        size--;
        for (size_t i = pos; i < size; i++)
            vector[i] = vector[i + 1];
        if (size < capacity / 2)
            Resize(size);
        return buf;
    }
    void Resize(size_t cap) {
        if (cap > capacity) {

            while (cap > capacity) {
                capacity *= 2;
            }
            T* result = new T[capacity];
            for (size_t i = 0; i < size; i++)
                result[i] = vector[i];
            delete[] vector;
            vector = result;
        }
    }

    void Concatenation(TVector& tail) {
        for (size_t i = 0; i < tail.size; i++)
            AddLast(tail.vector[i]);
        tail.ReNew();
    }
    T& operator[](size_t pos) {
        return vector[pos];
    }
    void operator=(const TVector& tail) {
        size = tail.size;
        capacity = tail.capacity;
        for (size_t i = 0; i < size; i++)
            vector[i] = tail.vector[i];
    }
    ~TVector() {
        delete[] vector;
    }
};
#endif
