#ifndef BTREE_H
#define BTREE_H
#include "Vector.h"
#include <cstring>
#include <memory>
using std::shared_ptr;
typedef const char* TStatus;
#ifndef TBTREE_IMPLEMENTATION
extern TStatus EXIST;
extern TStatus NOT_FOUND;
extern TStatus OK;
extern TStatus MEMORY_ERROR;
extern TStatus FILE_ERROR;
extern TStatus DESERIALIZATION_ERROR;
extern TStatus JUST_RETURN;
#endif
const size_t BTREE_T = 50;
struct TData {
    TData();
    TData(char *a, unsigned long long b);
    ~TData();
    char* key;
    unsigned long long value;
};
class TBTree {
    struct TNode {
        TNode();
        TNode(const TNode& orig);
        TNode& operator=(const TNode& other);
        ~TNode();
        bool leaf;
        TVector< shared_ptr<TData> > data;
        TVector< shared_ptr<TNode> > children;
    };

    shared_ptr<TNode> root;

    void Saver(FILE* file, shared_ptr<TNode>& node);

    TStatus Loader(FILE* file, shared_ptr<TNode>& node);

    TStatus SplitChild(shared_ptr<TNode>& node, int i);
    TStatus InsertNonFull(shared_ptr<TNode>& node, shared_ptr<TData>& data);

    TStatus DeleteFromLeaf(shared_ptr<TNode>& node, int i, char* key);
    TStatus DeleteFromNode(shared_ptr<TNode>& node, int i, char* key);
    TStatus DeleteNonEmpty(shared_ptr<TNode>& node, char* key);
    TStatus MergeTree(shared_ptr<TNode>& node, int& i);

public:
    TBTree();
    shared_ptr<TNode>& GetRoot();

    TStatus InsertNode(shared_ptr<TData>& data);
    TStatus Search(shared_ptr<TNode>& node, char* key);
    TStatus DeleteNode(char* key);
    TStatus Save(char* path);
    TStatus Load(char* path);
};
#endif
