#include <iostream>
#include <string.h>
#include "BTree.h"
#include <memory>
#include <string.h>
#include <ctime>
#include <iomanip>

using namespace std;

int main() {
    char cmd[300];
    unsigned long long v;
    ios::sync_with_stdio(false); 
    //cin.tie(0);
    long long time1 = clock();
    shared_ptr<TBTree> bTree(new TBTree);
    while (cin >> cmd) {
        if(cmd[0]=='+') {
            cin >> cmd >> v;
            long long length = strlen(cmd);

            for(int i = 0; i < length; i++) {
                cmd[i] = tolower(cmd[i]);
            }

            shared_ptr<TData> tmp(new TData(cmd, v));
            //cout << bTree->InsertNode(tmp);
        }
        else if(cmd[0]=='-') {
            cin >> cmd;
            long long length = strlen(cmd);

            for(int i = 0; i < length; i++) {
                cmd[i] = tolower(cmd[i]);
            }
           // cout << bTree->DeleteNode(cmd);
        }
        else if(cmd[0]=='!') {
            cin >> cmd;

            if (cmd[0] == 'S') {
                cin >> cmd;
                //cout << bTree->Save(cmd);
            }
            else if(cmd[0] == 'L') {
                cin >> cmd;
                shared_ptr<TBTree> newBTree(new TBTree);
                TStatus buf = newBTree->Load(cmd);
                //cout << buf;
                if (buf == OK) bTree = newBTree;
            }
        }
        else {
            long long length = strlen(cmd);

            for(int i = 0; i < length; i++) {
                cmd[i] = tolower(cmd[i]);
            }
            //cout << bTree->Search(bTree->GetRoot(), cmd);
        }
    }
    long long time2 = clock();

    std::cout << std::setprecision(6) << ((time2-time1) / ((double)CLOCKS_PER_SEC)) <<" sec.\n";
    return 0;
}
