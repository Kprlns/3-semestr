#include <strings.h>
#include <string.h>
#include <iostream>
#define TBTREE_IMPLEMENTATION
#include "BTree.h"
using std::shared_ptr;
TStatus   EXIST = "Exist\n";
TStatus   NOT_FOUND = "NoSuchWord\n";
TStatus   OK = "OK\n";
TStatus   MEMORY_ERROR = "ERROR: Oops, smth wrong with memory\n";
TStatus   FILE_ERROR = "ERROR: Oops, smth wrong with file";
TStatus   LOAD_ERROR = "ERROR: Oops, smth wrong with load";
TStatus   NOTHING = "";


TData::TData() : key(nullptr), value(0) {

}
TData::TData(char *a, unsigned long long b) {
    key = strdup(a);
    value = b;
}
TData::~TData() {
    if (key) {
        free(key);
    }
}
TBTree::TNode::TNode() : leaf(true) {
}
TBTree::TNode::TNode(const TNode& tail) {
    leaf = tail.leaf;
    data = tail.data;
    if (!leaf) children = tail.children;
}
TBTree::TNode::~TNode() {
}



TBTree::TNode& TBTree::TNode::operator=(const TBTree::TNode& tail) {
    if (this != &tail) {
        leaf = tail.leaf;
        data = tail.data;
        if (!leaf) children = tail.children;
    }
    return *this;
}


TBTree::TBTree() {
    this->root = shared_ptr<TBTree::TNode>(new TNode);
}


shared_ptr<TBTree::TNode>& TBTree::GetRoot() {
    return root;
}


static inline int BinarySearch(TVector<shared_ptr<TData> >& data, char* key) {
    int left = 0, right = data.Size(), mid = 0, cmp;
    while (left < right) {
        mid = left + ((right - left) >> 1);
        cmp = strcmp(data[mid]->key, key);
        if (cmp > 0) {
            right = mid;
        }
        else if (cmp < 0) {
            left = mid + 1;
        }
        else {
            return mid;
        }
    }
    return left - 1;
}



TStatus TBTree::Search(shared_ptr<TBTree::TNode>& node, char* key) {
    int i = BinarySearch(node->data, key);

    if(i >= 0 && strcmp(node->data[i]->key, key) == 0) {
            //std::cout << "OK: " << node->data[i]->value << std::endl;
            return NOTHING;
    }
    if (node->leaf) {
        return NOT_FOUND;
    }
    return Search(node->children[i + 1], key);
}


TStatus TBTree::SplitChild(shared_ptr<TBTree::TNode>& node, int i) {

    shared_ptr<TBTree::TNode> additionalNode(new TNode);
    shared_ptr<TBTree::TNode> existingNode = node->children[i];

    if (!additionalNode) {
        return MEMORY_ERROR;
    }

    additionalNode->leaf = existingNode->leaf;
    additionalNode->data.Resize(BTREE_T - 1);

    for (size_t j = 0; j < BTREE_T - 1; j++) {
        additionalNode->data.AddLast(existingNode->data[BTREE_T]);
        existingNode->data.Pull(BTREE_T);
    }


    if (!existingNode->leaf) {
        additionalNode->children.Resize(BTREE_T);

        for (size_t j = 0; j < BTREE_T; j++) {
            additionalNode->children.AddLast(existingNode->children[BTREE_T]);
            existingNode->children.Pull(BTREE_T);
        }

        existingNode->children.Resize(BTREE_T);
    }

    node->data.Insert(i, existingNode->data[BTREE_T - 1]);
    existingNode->data.Pull(BTREE_T - 1);

    node->children[i] = existingNode;
    node->children.Insert(i + 1, additionalNode);
    return OK;
}


TStatus TBTree::InsertNonFull(shared_ptr<TBTree::TNode>& node, shared_ptr<TData>& data) {
    int i = BinarySearch(node->data, data->key);

    if ( i >= 0 && strcmp(node->data[i]->key, data->key) == 0) {
        return EXIST;
    }

    if (node->leaf)
        node->data.Insert(i + 1, data);
    else {
        i++;
        if (node->children[i]->data.Size() == 2 * BTREE_T - 1) {
            SplitChild(node, i);
            if (strcmp(node->data[i]->key, data->key) < 0) i++;
        }
        return InsertNonFull(node->children[i], data);
    }
    return OK;
}
TStatus TBTree::InsertNode(shared_ptr<TData>& data) {

    if (root->data.Empty()) {
        root->data.AddLast(data);
        return OK;
    }

    else if (root->data.Size() == 2 * BTREE_T - 1) {

        shared_ptr<TBTree::TNode> newRoot(new TNode);
        if (!newRoot) {
            return MEMORY_ERROR;
        }

        newRoot->leaf = false;
        newRoot->children.AddLast(root);
        root = newRoot;

        if (SplitChild(root, 0) != OK) {
            return MEMORY_ERROR;
        }
    }
    return InsertNonFull(root, data);
}


TStatus TBTree::MergeTree(shared_ptr<TBTree::TNode>& node, int& i) {


    if (i > 0 && node->children[i - 1]->data.Size() > BTREE_T - 1) {
        node->children[i]->data.Insert(0, node->data[i - 1]);

        node->children[i]->children.
                Insert(0, node->children[i - 1]->children[node->children[i - 1]->children.Size() - 1]);

        node->children[i - 1]->children.DelLast();
        node->data[i - 1] =
                node->children[i - 1]->data[node->children[i - 1]->data.Size() - 1];

        node->children[i - 1]->data.DelLast();
    }
    else if ((size_t)(i + 1) < node->children.Size()
             && node->children[i + 1]->data.Size() > BTREE_T - 1)
    {
        node->children[i]->data.AddLast(node->data[i]);

        node->children[i]->children.AddLast(node->children[i + 1]->children[0]);

        node->children[i + 1]->children.Pull(0);
        node->data[i] = node->children[i + 1]->data[0];

        node->children[i + 1]->data.Pull(0);
    }
    else {
        if (i > 0) i--;
        node->children[i]->data.AddLast(node->data[i]);
        node->data.Pull(i);

        node->children[i]->data.Concatenation(node->children[i + 1]->data);
        node->children[i]->children.Concatenation(node->children[i + 1]->children);

        node->children.Pull(i + 1);

        if (node == root && !node->data.Size()) {
            root = shared_ptr<TBTree::TNode>(node->children[i]);
            return NOTHING;
        }
    }
    return OK;
}


TStatus TBTree::DeleteFromNode(shared_ptr<TBTree::TNode>& node, int i, char* key) {

    if (node->children[i]->data.Size() > BTREE_T - 1) {
        shared_ptr<TBTree::TNode> tmp = node->children[i];
        while (!tmp->leaf) {
            tmp = tmp->children[tmp->children.Size() - 1];
        }

        node->data[i] = tmp->data[tmp->data.Size() - 1];

        if (node->children[i]->leaf) {
            return DeleteFromLeaf(node, i, node->data[i]->key);
        }
        else {
            return DeleteNonEmpty(node->children[i], tmp->data[tmp->data.Size() - 1]->key);
        }
    }
    else if (node->children[i + 1]->data.Size() > BTREE_T - 1) {
        shared_ptr<TBTree::TNode> tmp = node->children[i + 1];

        while (!tmp->leaf) {
            tmp = tmp->children[0];
        }

        node->data[i] = tmp->data[0];

        if (node->children[i + 1]->leaf) {
            return DeleteFromLeaf(node, i + 1, node->data[i]->key);
        }
        else {
            return DeleteNonEmpty(node->children[i + 1], tmp->data[0]->key);
        }
    }
    else {
        node->children[i]->data.AddLast(node->data[i]);
        node->children[i]->data.Concatenation(node->children[i + 1]->data);

        if (!node->children[i]->leaf) {
            node->children[i]->children.Concatenation(node->children[i + 1]->children);
        }

        node->children.Pull(i + 1);
        node->data.Pull(i);

        if (node == root && !node->data.Size()) {
            root = shared_ptr<TBTree::TNode>(node->children[i]);
            return DeleteNode(key);
        }
        if (node->children[i]->leaf) {
            return DeleteFromLeaf(node, i, key);
        }
        else {
            return DeleteNonEmpty(node->children[i], key);
        }
    }
}



TStatus TBTree::DeleteFromLeaf(shared_ptr<TBTree::TNode>& node, int i, char* key) {
    int ind = BinarySearch(node->children[i]->data, key);

    if (!(ind >= 0 && strcmp(node->children[i]->data[ind]->key, key) == 0)) {
        return NOT_FOUND;
    }

    node->children[i]->data.Pull(ind);

    if (node->children[i]->data.Size() >= BTREE_T - 1) {
        return OK;
    }

    if (i > 0 && node->children[i - 1]->data.Size() > BTREE_T - 1) {
        node->children[i]->data.Insert(0, node->data[i - 1]);

        node->data[i - 1] =
                node->children[i - 1]->data[node->children[i - 1]->data.Size() - 1];
        node->children[i - 1]->data.DelLast();
    }
    else if ((size_t)(i + 1) < node->children.Size() && node->children[i + 1]->data.Size() > BTREE_T - 1) {
        node->children[i]->data.AddLast(node->data[i]);

        node->data[i] = node->children[i + 1]->data[0];
        node->children[i + 1]->data.Pull(0);
    }
    else {
        if (i > 0) i--;
        node->children[i]->data.AddLast(node->data[i]);
        node->data.Pull(i);

        node->children[i]->data.Concatenation(node->children[i + 1]->data);
        node->children.Pull(i + 1);
        if (node == root && !node->data.Size()) {
            root = shared_ptr<TBTree::TNode>(node->children[i]);
        }

    }
    return OK;
}



TStatus TBTree::DeleteNonEmpty(shared_ptr<TBTree::TNode>& node, char* key) {
    int i = BinarySearch(node->data, key);

    if (i >= 0 && !strcmp(node->data[i]->key, key)) {
        return DeleteFromNode(node, i, key);
    }
    else if (node->children[i + 1]->leaf)
        return DeleteFromLeaf(node, i + 1, key);
    else {
        i++;
        if (!node->children[i]->leaf
            && node->children[i]->data.Size() == BTREE_T - 1
            && MergeTree(node, i) == NOTHING)
        {
                return DeleteNode(key);
        }
        return (DeleteNonEmpty(node->children[i], key));
    }
}


TStatus TBTree::DeleteNode(char* key) {

    if (root->leaf) {
        int i = BinarySearch(root->data, key);

        if (!(i >=0 && !strcmp(root->data[i]->key, key))) {
            return NOT_FOUND;
        }

        if (root->data.Size() == 1) {
            root = shared_ptr<TBTree::TNode>(new TNode);
        }

        else if (root->data.Size() > 1) {
            root->data.Pull(i);
        }
        return OK;
    }
    return DeleteNonEmpty(root, key);
}



TStatus TBTree::Save(char* path) {
    FILE *file = fopen(path, "wb");
    if (!file) {
        return FILE_ERROR;
    }
    Saver(file, root);

    fclose(file);
    return OK;
}
void TBTree::Saver(FILE* file, shared_ptr<TBTree::TNode>& node) {
    fwrite(&node->leaf, sizeof(bool), 1, file);

    size_t cnt = node->data.Size();
    fwrite(&cnt, sizeof(size_t), 1, file);

    if (!node->data.Empty()) {
        for(size_t i = 0; i < cnt; i++) {
            size_t len = strlen(node->data[i]->key) + 1;
	        fwrite(&len, sizeof(len), 1, file);

            fwrite(node->data[i]->key, sizeof(char)*len, 1, file);

            fwrite(&(node->data[i]->value), sizeof(node->data[i]->value), 1, file);
    	}
        if (!node->leaf) {
            for (size_t i = 0; i <= cnt; i++) {
                Saver(file, node->children[i]);
            }
        }
    }
}


TStatus TBTree::Load(char* path) {
    FILE *file = fopen(path, "rb");
    if (!file) {
        return FILE_ERROR;
    }
    TStatus buf = Loader(file, root);

    fclose(file);
    return buf;
}

TStatus TBTree::Loader(FILE* file, shared_ptr<TBTree::TNode>& node) {
    if(fread(&node->leaf, sizeof(bool), 1, file) != 1) {
        return LOAD_ERROR;
    }
    size_t cnt;
    if (fread(&cnt, sizeof(size_t), 1, file) != 1) {
        return LOAD_ERROR;
    }

    node->data.Resize(cnt);
    for (size_t i = 0; i < cnt; i++) {
        shared_ptr<TData> data(new TData);

        size_t len;
        if (fread(&len, sizeof(len), 1, file) != 1) {
            return LOAD_ERROR;
        }

        data->key = new char[len];
        if (fread(data->key, sizeof(char)*len, 1, file) != 1) {
            return LOAD_ERROR;
        }
        if (fread(&(data->value), sizeof(data->value), 1, file) != 1) {
            return LOAD_ERROR;
        }

        node->data.AddLast(data);
    }
    if (!node->leaf)
        for (size_t i = 0; i <= cnt; i++) {
            shared_ptr<TBTree::TNode> tmp(new TNode);

            node->children.AddLast(tmp);
            Loader(file, node->children[i]);
        }
    return OK;
}
