#include <iostream>
#include <map>
#include <string>
#include <iomanip>

void ToLower(std::string& s) {
    for(int i = 0; i < s.length();i++) {
        if(s[i] >= 'A' && s[i] <= 'Z') {
            s[i] += 32;
        }
    }
}

int main() {

    std::map<std::string,unsigned long long> tree;
    unsigned long long val;
    std::string str;
long long time1 = clock();
    while(std::cin >> str) {
        //std::cout <<"menu\n";
        if(str[0] == '+') {
            //std::cout << "add\n";
            std::cin >> str;
            ToLower(str);

            auto finder = tree.find(str);
            if(finder != tree.end()) {
                //std::cout << "Exist\n";
                continue;
            }

            std::cin >> val;
            tree.insert( std::make_pair(str,val));
            //std::cout << "OK\n";
            continue;
        }
        if(str[0] == '-') {
            //std::cout <<"erase\n";
            std::cin >> str;
            ToLower(str);
            size_t abc = tree.erase(str);
            if(!abc) {
                //std::cout <<"NoSuchWord\n";
            }
            else {
                //std::cout <<"OK\n";
            }
            continue;
        }
        if(str[0] == '!' && str[1] == 'p') {
            auto it = tree.begin();
            //std::cout << "\n";
            for (it = tree.begin(); it != tree.end(); ++it) {
                std::cout << it->first << " " << it->second << '\n';
            }
            continue;
        }
        if(str[0] == '!' && str[1] == 'c') {
            tree.clear();
            continue;
        }
        if(str[0] == '!' && str[1] == 'S') {
            continue;
        }
        if(str[0] == '!' && str[1] == 'L') {
            continue;
        }
        ToLower(str);
        auto finder = tree.find(str);
        if(finder == tree.end()) {
        //    std::cout << "NoSuchWord\n";
        }
        else {
        //    std::cout << "OK: " << finder->second << "\n";
        }
    }
    long long time2 = clock();
    double res = (time2 - time1)/CLOCKS_PER_SEC;
    std::cout << std::setprecision(6) << ((time2-time1) / ((double)CLOCKS_PER_SEC)) <<" sec.\n";
}