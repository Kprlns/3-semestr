import random
import string

f = open("gprofTest","w")
act = ["+","-","!"]
for i in range(0,100000):
    rnd = int(random.randint(0, 2))
    ch = act[rnd]
    if ch == "+":
        rndDist = random.randint(10,20)
        x = "A".join(random.choice(string.ascii_uppercase+string.ascii_lowercase) for x in range(5,rndDist))
        f.write("+ "+x+" "+str(random.randint(0,10000000))+'\n')
    elif ch == "-":
        x = "".join(random.choice(string.ascii_uppercase + string.ascii_uppercase))
        f.write("- " + x+'\n')
    else:
        x = "".join(random.choice(string.ascii_uppercase + string.ascii_uppercase))
        f.write(x + '\n')
