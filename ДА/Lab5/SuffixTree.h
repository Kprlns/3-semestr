#include "Node.h"
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <queue>
#ifndef OOP_SUFFIXTREE_H
#define OOP_SUFFIXTREE_H


const char UNIQUE_SYMBOL1 = '$';
const char UNIQUE_SYMBOL2 = '#';

//struct Entry {

//};

class TSuffixTree {
public:
    TSuffixTree(std::string& text);

    void BuildTree();
    void PrintTree(TNode* node, int lvl);
    void LongestSubstring(int textLength);
    void DeleteTree(TNode* node);

    int DoTraversal(TNode *node, int labelHeight, int* maxHeight, std::queue<int>& entries, int textLength);

    ~TSuffixTree();

    TNode* root = new TNode(nullptr, -1, new int(-1));
private:
    void ExtendTree(int pos);


    int EdgeLength(TNode* node);

    std::string text;

    TNode *lastNewNode = nullptr;

    int remaining = 0;
    TNode *activeNode = nullptr;
    int activeEdge = -1;
    int activeLength = 0;



    int leafEnd = -1;

};


#endif //OOP_SUFFIXTREE_H
