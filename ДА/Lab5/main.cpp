//
// Created by Никита on 06.01.17.
//
#include "SuffixTree.h"

int main() {
    std::string text1, text2;
    std::cin >> text1 >> text2;
    std::string text = text1 + UNIQUE_SYMBOL1 + text2 + UNIQUE_SYMBOL2;
    TSuffixTree tree(text);
    //tree.PrintTree(tree.root,1);

    size_t textLength = text1.length();
    tree.LongestSubstring((int)textLength);
}