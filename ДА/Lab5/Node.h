//
// Created by Никита on 06.01.17.
//
#include <iostream>
#include <string>
#include <vector>
#include <map>

#ifndef OOP_NODE_H
#define OOP_NODE_H

class TSuffixTree;

class TNode {
public:
    friend class TSuffixTree;

    TNode(TNode* link, int begin, int* end) : TNode(link, begin, end, -1) { }
    TNode(TNode* link, int begin, int* end, int ind) : suffixLink(link), begin(begin), end(end), suffixIndex(ind) { }


private:

    std::map<char, TNode*> children;
    TNode* suffixLink;
    int begin;
    int* end;
    int suffixIndex;

    //bool has
};


#endif //OOP_NODE_H
