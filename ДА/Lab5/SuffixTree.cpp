//
// Created by Никита on 06.01.17.
//
#include <map>
#include <queue>
#include "SuffixTree.h"

TSuffixTree::TSuffixTree(std::string& input) : text(input) {
    //text += UNIQUE_SYMBOL1;
    BuildTree();
}

int TSuffixTree::EdgeLength(TNode* node) {
    return *(node->end) - (node->begin) + 1;
}

void TSuffixTree::BuildTree() {
    activeNode = root;
    size_t lenSize = text.length();
    for (size_t i = 0; i < lenSize; i++) {
        ExtendTree((int)i);
    }
}

void TSuffixTree::PrintTree(TNode* node, int lvl) {
    std::string spaces;
    for(int i = 0; i < lvl; i++) {
        spaces += ' ';
    }

    std::cout << std::endl;
    std::cout << spaces << "'";

    if(node->begin != -1) {
        for(int k = node->begin; k <= *node->end; k++) {
            std::cout << text[k];
        }
    }

    std::cout << "'" << std::endl;

    std::cout << spaces << "children: {";
    for (auto iter : node->children)
        PrintTree(iter.second, lvl + 4);
    std::cout << spaces << "}" << std::endl ;

}

void TSuffixTree::ExtendTree(int pos) {

    leafEnd = pos;
    remaining++;
    lastNewNode = nullptr;
    while (remaining != 0) {
        if (activeLength == 0) {
            activeEdge = pos;
        }
        std::map<char, TNode*>::iterator node = activeNode->children.find(text[activeEdge]);

        if (node == activeNode->children.end()) {

            TNode* tmp = new TNode(root, pos, &leafEnd, pos - remaining + 1);
            activeNode->children.insert(std::make_pair(text[activeEdge], tmp));

            if (lastNewNode != nullptr) {
                lastNewNode->suffixLink = activeNode;
                lastNewNode = nullptr;
            }
        }
        else {
            TNode* next = node->second;

            int edgeLen = EdgeLength(next);
            if (activeLength >= edgeLen) {
                activeEdge += edgeLen;
                activeLength -= edgeLen;
                activeNode = next;
                continue;
            }

            if (text[next->begin + activeLength] == text[pos]) {
                if (lastNewNode && activeNode != root)
                    lastNewNode->suffixLink = activeNode;
                activeLength++;
                break;
            }

            TNode* newNode = new TNode(root, next->begin, new int(next->begin + activeLength - 1));
            activeNode->children[text[activeEdge]] = newNode;
            next->begin += activeLength;

            TNode* tmp = new TNode(root, pos, &leafEnd, pos - remaining + 1);
            newNode->children.insert(std::make_pair(text[pos], tmp));
            newNode->children.insert(std::make_pair(text[next->begin], next));

            if (lastNewNode) {
                lastNewNode->suffixLink = newNode;
            }
            lastNewNode = newNode;
        }

        remaining--;
        if (activeNode == root && activeLength > 0) {
            activeLength--;
            activeEdge = pos - remaining + 1;
        }

        else if (activeNode != root) {
            activeNode = activeNode->suffixLink;
        }
    }
}

TSuffixTree::~TSuffixTree() {
    DeleteTree(this->root);
}

void TSuffixTree::DeleteTree(TNode* node) {
    for(auto it: node->children) {
        DeleteTree(it.second);
    }
    if(node->suffixIndex == - 1) {
        delete(node->end);
    }
    delete(node);
}

void TSuffixTree::LongestSubstring(int textLength) {
    int maxHeight = 0;
    int substringBeginIndex = -999;
    //size_t textLength = this->text.length();

    std::queue<int> entries;
    DoTraversal(this->root,0,&maxHeight,entries, (int)textLength);

    std::cout << maxHeight << std::endl;
    while(!entries.empty()) {
        if(substringBeginIndex == entries.front()) {
            entries.pop();
            continue;
        }

        substringBeginIndex = entries.front();
        entries.pop();
        int i = 0;
        for(i = 0; i < maxHeight; ++i) {
            std::cout << text[i + substringBeginIndex];
        }
        if(i == 0) {
            std::cout << "No common string";
        }
        std::cout << std::endl;
    }


}

int TSuffixTree::DoTraversal(TNode* node, int labelHeight, int* maxHeight, std::queue<int>& entries, int textLength) {

    if(node == nullptr) {
        return -100;
    }

    int i = 0;
    int tmp = -1;

    if(node->suffixIndex < 0) {
        for(auto it: node->children) {
            TNode* curr = it.second;
            tmp = DoTraversal(curr,labelHeight + EdgeLength(curr),maxHeight,entries,textLength);

            if(node->suffixIndex == -1) {
                node->suffixIndex = tmp;
            }
            else if((node->suffixIndex == -2 && tmp == -3) || (node->suffixIndex == -3 && tmp == -2) || (node->suffixIndex == -4)) {
                node->suffixIndex = -4;

                if(*maxHeight < labelHeight) {
                    *maxHeight = labelHeight;
                    while( !entries.empty() ) {
                        entries.pop();
                    }
                    entries.push(*(node->end) - labelHeight + 1);
                }
                if(*maxHeight == labelHeight) {
                    entries.push(*(node->end) - labelHeight + 1);
                }

            }
        }
    }
    else if( node->suffixIndex > - 1 && node->suffixIndex < textLength) {
        return -2;
    }
    else if(node->suffixIndex >= textLength) {
        return -3;
    }
    return node->suffixIndex;
}

