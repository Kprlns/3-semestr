#include <iostream>
#include <vector>

#include "KMP.h"


int main() {

    std::string pattern;
    std::getline(std::cin, pattern);

    std::vector<unsigned int> z(pattern.size(), 0);
    std::vector<unsigned int> sp2(z.size(), 0); //sp'

    ZFunc(z, pattern);
    SPCount(z, sp2);

    std::vector<std::pair<unsigned int, std::pair<unsigned int, unsigned int> > > text;

    std::vector<unsigned int> res;
    KMP2(sp2,pattern,res);
    PrintRes(res);
}
