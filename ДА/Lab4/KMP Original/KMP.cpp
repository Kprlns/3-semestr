#include <iostream>
#include <string>
#include <vector>

#include "KMP.h"


int main() {

    std::string patt;
    std::getline(std::cin, patt);
    std::vector<unsigned int> pattern;

    ParseString(patt, pattern);

    std::vector<unsigned int> z(pattern.size(), 0);
    std::vector<unsigned int> sp2(z.size(), 0); //sp'

    ZFunc(z, pattern);
    SPCount(z, sp2);

    std::vector<std::pair<unsigned int, std::pair<unsigned int, unsigned int> > > text;



    KMP2(sp2,pattern,text);
}
