#include <vector>
#include <string>
#include <unistd.h>
#include <iostream>
#include <stdio.h>
#ifndef OOP_KMP_H
#define OOP_KMP_H

void PrintVector(std::vector<unsigned int>& vec, int k) {
    for(int i = 0; i < vec.size(); i++) {
        std::cout << vec[i] << " ";
    }
    while(k > 0) {
        std::cout << "\n";
        k--;
    }
}

void ParseString(std::string& str, std::vector<unsigned int>& num) {

    unsigned int tmp = 0;
    for (int i = 0; i < str.length(); ++i) {
        if(str[i] == ' ') {
            if((i != 0) && (str[i-1] != ' ')) {
                num.push_back(tmp);
                tmp = 0;
            }
            continue;
        }

        tmp *= 10;
        tmp += str[i] - '0';
    }
    if(str[str.length() - 1] != ' ') {
        num.push_back(tmp);
    }
}


void ZFunc(std::vector<unsigned int>& z, std::vector<unsigned int>& text) {

    z[0] = 0;
    for(int i = 1; i < text.size(); ++i) {

        for(int j = i; j < text.size(); ++j) {
            if (text[j] == text[j - i]) {
                z[i]++;
            }
            else {
                break;
            }
        }
    }
}

void SPCount(std::vector<unsigned int>& z, std::vector<unsigned int>& sp2) {
    sp2[0] = 0;

    for (int j = (int)z.size() - 1; j > 0; --j) {

        int i = j + z[j] - 1;
        sp2[i] = z[j];
    }

    /*sp1[z.size() - 1] = sp2[z.size() - 1];

    for (int i = (int)z.size() - 2; i > 0; --i) {

        if( (sp1[i + 1]) - 1 > sp2[i] ) {
            sp1[i] = sp1[i + 1] - 1;
        }
        else {
            sp1[i] = sp2[i];
        }
    }*/
}



void ParseText(std::vector< std::pair<unsigned int,unsigned int> >& text) {

    int tmp = 0;
    int ind = 1;

    std::string str;

    while(std::getline(std::cin,str)){
        if(str.length() != 0) {
            for (int i = 0; i < str.length(); i++) {
                if (str[i] == ' ') {
                    if ((i != 0) && (str[i - 1] != ' ')) {
                        text.push_back(std::make_pair(tmp, ind));
                        tmp = 0;
                    }
                    continue;
                }
                tmp *= 10;
                tmp += str[i] - '0';
            }

            if (str[str.length() - 1] != ' ') {
                text.push_back(std::make_pair(tmp, ind));
                tmp = 0;
            }
        }
        ind++;
    }
}

void CutVector(std::vector< std::pair<unsigned int,std::pair<unsigned int, unsigned int> > >& vec, int k) {
    if(k < 0 || k > vec.size()) {
        std::cout << "ALARM";
        return;
    }
    else if(k == 0) {
        return;
    }
    else {
        for(int i = k; i < vec.size() ;i++) {
            vec[i - k] = vec[i];
        }
        while(k > 0) {
            k--;
            vec.pop_back();
        }
    }
}


void PrintVectorPair(std::vector< std::pair<unsigned int,unsigned int> >& text, int j) {

    for(int i = 0; i < text.size(); i++) {
        std::cout <<"[ " <<text[i].first <<" " <<text[i].second << " ] ";
        if( !(i % 4) && i > 0 ) {
            std::cout << "\n";
        }

    }
    for(int z = 0; z < j; z++) {
        std::cout <<"\n";
    }
}

bool GetNums(std::vector< std::pair<unsigned int, std::pair<unsigned int, unsigned int> > >& text, unsigned long max) {
    char c;
    bool check = false;
    static unsigned int strInd = 0;
    static unsigned int posInd = 0;
    unsigned int tmp = 0;

    while(scanf("%c",&c) != EOF) {

        if(c == ' ' && check) {
            posInd++;
            text.push_back(std::make_pair(tmp,std::make_pair(strInd,posInd)));
            check = false;

            tmp = 0;

            if(text.size() >= max) {
                return true;
            }
        }
        else if(c == '\n'){
            if(check) {
                posInd++;
                text.push_back(std::make_pair(tmp,std::make_pair(strInd,posInd)));
                check = false;
            }
            posInd = 0;
            strInd++;
            tmp = 0;

            if(text.size() >= max) {
                return true;
            }
        }

        if(c >= '0' && c <= '9') {
            check = true;
            tmp *= 10;
            tmp += (c - '0');
        }

    }
    return false;
}

/*
bool GetNums(std::vector< std::pair<unsigned int, std::pair<unsigned int, unsigned int> > >& text, unsigned long max) {

    std::string str;
    bool check = false;

    static unsigned int strInd = 0;
    unsigned int tmp = 0;
    unsigned int pos = 1;

    while(std::getline(std::cin,str)) {

        for (int i = 0; i < str.length(); ++i) {

            if(str[i] == ' ' && check) {
                text.push_back(std::make_pair(tmp,std::make_pair(strInd,pos)));
                check = false;
                pos++;
                tmp = 0;
            }

            if(str[i] >= '0' && str[i] <= '9') {
                check = true;
                tmp *= 10;
                tmp += (str[i] - '0');
            }
        }

        if(check) {
            text.push_back(std::make_pair(tmp,std::make_pair(strInd,pos)));
        }
        strInd++;

        if(text.size() >= max) {
            return true;
        }
    }
    return false;
}
*/

void KMP2(std::vector<unsigned int>& sp2, std::vector<unsigned int>& pattern,
          std::vector< std::pair<unsigned int,std::pair<unsigned int, unsigned int> > >& text)
{

    bool notEnd = GetNums(text, pattern.size() * 5);

    int n = (int) pattern.size() - 1;
    int m = (int) text.size() - 1;

    std::vector< std::pair<unsigned int,unsigned int> > res;

    unsigned int cCnt = 0;
    unsigned int pCnt = 0;

    if (text.empty() || pattern.empty()) {
        return;
    }

    while ((cCnt - pCnt + n) <= m) {

        if (cCnt >= (int)(text.size()/2) && notEnd) {
            CutVector(text, cCnt - pCnt - 1);
            cCnt -= (cCnt - pCnt - 1);
            notEnd = GetNums(text, pattern.size() * 5);
            m = (int)text.size() - 1;
        }

        while ((pCnt <= n) && (cCnt <= m) && (pattern.at(pCnt) == text.at(cCnt).first)) {
            pCnt++;
            cCnt++;

            if (cCnt >= (int)(text.size()/2) && notEnd) {
                CutVector(text, cCnt - pCnt - 1);
                cCnt -= (cCnt - pCnt - 1);
                notEnd = GetNums(text, pattern.size() * 5);
                m = (int)text.size() - 1;
            }
        }

        if (pCnt == n + 1) {
            //std::cout << text[cCnt - n - 1].second << ", " << IndexCount(text,cCnt - n - 1) << '\n';
            res.push_back(text[cCnt - n - 1].second);
        }

        if (pCnt == 0) {
            cCnt++;
        } else {
            pCnt = sp2.at(pCnt - 1);
        }
    }

    for (unsigned int i = 0; i < res.size(); ++i) {
        std::cout << res.at(i).first + 1 << ", " << res.at(i).second << '\n';
    }
}

#endif //OOP_KMP_H
