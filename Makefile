TARGET = Lab2
CC=g++
FLAGS = -std=gnu++11 -Wall -pedantic -g
default: 
	g++ *.cpp -o Lab2 $(FLAGS)
clean:
	rm -rf da2 *gcov *gcno *gcda
