#include <iostream>
#include <string.h>
#include "BTree.h"
#include <memory>
#ifndef TBTREE_IMPLEMENTATION
extern TStatus EXIST;
extern TStatus NOT_FOUND;
extern TStatus OK;
extern TStatus MEMORY_ERROR;
extern TStatus FILE_ERROR;
extern TStatus DESERIALIZATION_ERROR;
extern TStatus NOTHING;
#endif


using namespace std;

int main() {
    char cmd[257];
    unsigned long long v;
    ios::sync_with_stdio(false);
    cin.tie();
    shared_ptr<TBTree> bTree(new TBTree);
    while (cin >> cmd) {
        if(cmd[0]=='+') {
            cin >> cmd >> v;
            shared_ptr<TData> data(new TData(cmd, v));
            cout << bTree->Insert(data);
        }
        else if(cmd[0]=='-') {
            cin >> cmd;
            cout << bTree->Delete(cmd);
        }
        else if(cmd[0]=='!') {
            cin >> cmd;
            if (cmd[0] == 'S') {
                cin >> cmd;
                cout << bTree->Save(cmd);
            }
            else if(cmd[0] == 'L') {
                cin >> cmd;
                shared_ptr<TBTree> newBTree(new TBTree);
                TStatus buf = newBTree->Load(cmd);
                cout << buf;
                if (buf == OK) bTree = newBTree;
            }
        }
        else {
            cout << bTree->Search(bTree->Root(), cmd);
        }

    }
    return 0;
}
