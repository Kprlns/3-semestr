#include <strings.h>
#include <string.h>
#include <iostream>
#define TBTREE_IMPLEMENTATION
#include "BTree.h"
TStatus   EXIST = "Exist\n";
TStatus   NOT_FOUND = "NoSuchWord\n";
TStatus   OK = "OK\n";
TStatus   MEMORY_ERROR = "ERROR: Oops, smth wrong with memory\n";
TStatus   FILE_ERROR = "ERROR: Oops, smth wrong\n";
TStatus   DESERIALIZATION_ERROR = "ERROR: Oops, smth wrong with loading\n";
TStatus   NOTHING = "";

TBTree::TNode::TNode() : leaf(true) {
}


TBTree::TNode::TNode(const TNode& orig) {
    leaf = orig.leaf;
    data = orig.data;
    if (!leaf) {
        children = orig.children;
    }
}
TBTree::TNode::~TNode() {
}


TBTree::TNode& TBTree::TNode::operator=(const TBTree::TNode& eq) {
    if (this != &eq) {
        leaf = eq.leaf;
        data = eq.data;
        if (!leaf) {
            children = eq.children;
        }
    }
    return *this;
}
TBTree::TBTree() {
    this->root = shared_ptr<TBTree::TNode>(new TNode);
}                                              
shared_ptr<TBTree::TNode>& TBTree::Root() {
    return this->root;
}
int BinarySearch(TVector< std::shared_ptr<TData> >& data, char *key) {
    int l = 0;
    int r = data.Size() - 1;
    int mid = 0;
    int cmp;
    while (l < r) {
        mid = (int)((l + r)/2);
        cmp = strcasecmp(data[mid]->key, key);
        if (cmp > 0) {
            r = mid;
        }
        else if (cmp < 0) {
            l = mid + 1;
        }
        else {
            return mid;
        }
    }
    return l - 1;
}


TStatus TBTree::Search(shared_ptr<TBTree::TNode>& node, char* key) {
    int i = BinarySearch(node->data, key);

    if( i >= 0 && strcasecmp(node->data[i]->key, key) == 0) {
            std::cout << "OK: " << node->data[i]->value << std::endl;
            return NOTHING;
    }
    if (node->leaf) {
        return NOT_FOUND;
    }
    return Search(node->children[i + 1], key);
}



TStatus TBTree::SplitChild(shared_ptr<TBTree::TNode>& node, int i) {
    shared_ptr<TBTree::TNode> additionalNode(new TNode);
    shared_ptr<TBTree::TNode> existingNode = node->children[i];

    if (!additionalNode) {
        return MEMORY_ERROR;
    }


    additionalNode->leaf = existingNode->leaf;
    additionalNode->data.Resize(DEGREE - 1);

    for (size_t j = 0; j < DEGREE - 1; j++) {
        // existingNode->data[DEGREE];
        additionalNode->data.SetLast(existingNode->data[DEGREE]);
        existingNode->data.Pull(DEGREE);
    }
    if (!existingNode->leaf) {
        additionalNode->children.Resize(DEGREE );

        for (size_t j = 0; j < DEGREE; j++) {
            std::shared_ptr<TBTree::TNode> tmp = existingNode->children.Pull(DEGREE);
            additionalNode->children.SetLast(tmp);

        }

        existingNode->children.Resize(DEGREE );
    }

    node->data.Insert(i, existingNode->data[DEGREE - 1]);
    existingNode->data.Pull(DEGREE - 1);
    node->children[i] = existingNode;
    node->children.Insert(i + 1, additionalNode);
    return OK;
}


TStatus TBTree::InsertNonFull(shared_ptr<TBTree::TNode>& node, std::shared_ptr<TData>& data) {
    int i = BinarySearch(node->data, data->key);

    if (i >= 0 && strcasecmp(node->data[i]->key, data->key) == 0) {
        return EXIST;
    }
    if (node->leaf) {
        node->data.Insert(i + 1, data);
    }
    else {
        i++;
        if (node->children[i]->data.Size() == 2 * DEGREE - 1) {
            SplitChild(node, i);
            if (strcasecmp(data->key, node->data[i]->key) > 0) {
                i++;
            }
        }
        return InsertNonFull(node->children[i], data);
    }
    return OK;
}

TStatus TBTree::Insert(std::shared_ptr<TData>& data) {
//    std::cout <<"I\n";
    if (root->data.Empty()) {
        root->data.SetLast(data);
        return OK;
    }
    else {
        if (root->data.Size() == 2 * DEGREE - 1) {
            shared_ptr<TBTree::TNode> newRoot(new TNode);
            if (!newRoot) {
                return MEMORY_ERROR;
            }

            newRoot->leaf = false;
            newRoot->children.SetLast(root);
            root = newRoot;
            if (SplitChild(root, 0) != OK) {
                return MEMORY_ERROR;
            }
        }
    }
    return InsertNonFull(root, data);
}
TStatus TBTree::MergeTree(shared_ptr<TBTree::TNode>& node, int& i) {

    if (i > 0 && node->children[i - 1]->data.Size() > DEGREE - 1) {
        node->children[i]->data.Insert(0, node->data[i - 1]);
        int ind = node->children[i - 1]->children.Size() - 1;
        node->children[i]->children.
                Insert(0, node->children[i - 1]->children[ind]);

        node->children[i - 1]->children.DelLast();

        ind = node->children[i - 1]->data.Size() - 1;
        node->data[i - 1] = node->children[i - 1]->data[ind];

        node->children[i - 1]->data.DelLast();
    }
    else {

        if ( ((size_t)(i + 1) < node->children.Size()) && (node->children[i + 1]->data.Size() > DEGREE - 1)) {
            node->children[i]->data.SetLast(node->data[i]);
            node->children[i]->children.SetLast(node->children[i + 1]->children[0]);


            node->children[i + 1]->data.Pull(0);
            node->data[i] = node->children[i + 1]->data[0];
            node->children[i + 1]->children.Pull(0);
        }
        else {
            if (i > 0) {
                i--;
            }
            node->children[i]->data.SetLast(node->data[i]);
            node->data.Pull(i);

            node->children[i]->data.Concatenation(node->children[i + 1]->data);
            node->children[i]->children.Concatenation(node->children[i + 1]->children);

            node->children.Pull(i + 1);

            if (node == root && node->data.Size() == 0) {
                root = shared_ptr<TBTree::TNode>(node->children[i]);
                return NOTHING;
            }
        }
    }
    return OK;
}




TStatus TBTree::DeleteFromLeaf(shared_ptr<TBTree::TNode>& node, int i, char* key) {
    int ind = BinarySearch(node->children[i]->data, key);

    if( !(ind >= 0 && !strcasecmp(node->children[i]->data[ind]->key, key))) {
        return NOT_FOUND;
    }
    node->children[i]->data.Pull(ind);
    if (node->children[i]->data.Size() >= DEGREE - 1) {
        return OK;
    }

    if (i > 0 && node->children[i - 1]->data.Size() > DEGREE - 1) {
        node->children[i]->data.Insert(0, node->data[i - 1]);

        node->data[i - 1] = node->children[i - 1]->data[node->children[i - 1]->data.Size() - 1];
        node->children[i - 1]->data.DelLast();
    }
    else if ((size_t)(i + 1) < node->children.Size() && node->children[i + 1]->data.Size() > DEGREE - 1) {
        node->children[i]->data.SetLast(node->data[i]);
        node->data[i] = node->children[i + 1]->data[0];
        node->children[i + 1]->data.Pull(0);
    }
    else {
        if (i > 0)  {
            i--;
        }
        node->children[i]->data.SetLast(node->data[i]);
        node->data.Pull(i);

        node->children[i]->data.Concatenation(node->children[i + 1]->data);
        node->children.Pull(i + 1);
        if (node == root && !node->data.Size())
            root = shared_ptr<TBTree::TNode>(node->children[i]);
    }
    return OK;
}



TStatus TBTree::DeleteFromNode(shared_ptr<TBTree::TNode>& node, int i, char* key) {

    if (node->children[i]->data.Size() > DEGREE - 1) {
        shared_ptr<TBTree::TNode> temp = node->children[i];

        while (!temp->leaf) {
            temp = temp->children[temp->children.Size() - 1];
        }
        node->data[i] = temp->data[temp->data.Size() - 1];

        if (!node->children[i]->leaf) {
            return DeleteNonEmpty(node->children[i], temp->data[temp->data.Size() - 1]->key);
        }
        else {
            return DeleteFromLeaf(node, i, node->data[i]->key);
        }

    }
    else {
        if (node->children[i + 1]->data.Size() > DEGREE - 1) {
            shared_ptr<TBTree::TNode> temp = node->children[i + 1];
            while (!temp->leaf) {
                temp = temp->children[0];
            }
            node->data[i] = temp->data[0];

            if (!node->children[i + 1]->leaf) {
                return DeleteNonEmpty(node->children[i + 1], temp->data[0]->key);
            }
            else {
                return DeleteFromLeaf(node, i + 1, node->data[i]->key);
            }

        }
        else {
            node->children[i]->data.SetLast(node->data[i]);
            node->children[i]->data.Concatenation(node->children[i + 1]->data);
            if (!node->children[i]->leaf) {
                node->children[i]->children.Concatenation(node->children[i + 1]->children);
            }

            node->children.Pull(i + 1);
            node->data.Pull(i);

            if (node == root && !node->data.Size()) {
                root = shared_ptr<TBTree::TNode>(node->children[i]);
                return Delete(key);
            }
            if (!node->children[i]->leaf) {
                return DeleteNonEmpty(node->children[i], key);
            }
            else {
                return DeleteFromLeaf(node, i, key);
            }

        }
    }
}

TStatus TBTree::DeleteNonEmpty(shared_ptr<TBTree::TNode>& node, char* key) {
    int i = BinarySearch(node->data, key);

    if( i >= 0 && !strcasecmp(node->data[i]->key, key)) {
        return DeleteFromNode(node, i, key);
    }
    else if (node->children[i + 1]->leaf)
        return DeleteFromLeaf(node, i + 1, key);
    else {
        i++;
        if (!node->children[i]->leaf
            && node->children[i]->data.Size() == DEGREE - 1
            && MergeTree(node, i) == NOTHING)
            {
                return Delete(key);
            }
        return (DeleteNonEmpty(node->children[i], key));
    }
}
TStatus TBTree::Delete(char* key) {
    if (root->leaf) {
        int i = BinarySearch(root->data, key);

        if( !(i >= 0 && !strcasecmp(root->data[i]->key, key))) {
            return NOT_FOUND;
        }
        if (root->data.Size() == 1)
            root = shared_ptr<TBTree::TNode>(new TNode);
        else if (root->data.Size() > 1)
            root->data.Pull(i);
        return OK;
    }
    return DeleteNonEmpty(root, key);
}

TStatus TBTree::Save(char* fileName) {
    FILE *file = fopen(fileName, "wb");
    if (!file) {
        return FILE_ERROR;
    }
    Saver(file, root);
    fclose(file);
    return OK;
}

void TBTree::Saver(FILE* file, shared_ptr<TBTree::TNode>& node) {
    fwrite(&node->leaf, sizeof(bool), 1, file);
    size_t cnt = node->data.Size();
    fwrite(&cnt, sizeof(size_t), 1, file);
    if (!node->data.Empty()) {
        for(size_t i = 0; i < cnt; i++) {
            size_t length = strlen(node->data[i]->key) + 1;
	        fwrite(&length, sizeof(length), 1, file);
            fwrite(node->data[i]->key, sizeof(char)*length, 1, file);
            fwrite(&(node->data[i]->value), sizeof(node->data[i]->value), 1, file);
    	}
        if (!node->leaf) {
            for (size_t i = 0; i <= cnt; i++) {
                Saver(file, node->children[i]);
            }
        }
    }
}



TStatus TBTree::Load(char* fileName) {
    FILE *file = fopen(fileName, "rb");
    if (!file) {
        return FILE_ERROR;
    }
    TStatus tree = Loader(file, root);
    fclose(file);
    return tree;
}

TStatus TBTree::Loader(FILE* file, shared_ptr<TBTree::TNode>& node) {
    if (fread(&node->leaf, sizeof(bool), 1, file) != 1) {
        return DESERIALIZATION_ERROR;
    }
    size_t cnt;
    if (fread(&cnt, sizeof(size_t), 1, file) != 1) {
        return DESERIALIZATION_ERROR;
    }
    node->data.Resize(cnt);
    for (size_t i = 0; i < cnt; i++) {
        std::shared_ptr<TData> data(new TData);
        size_t length;
        if (fread(&length, sizeof(length), 1, file) != 1) {
            return DESERIALIZATION_ERROR;
        }
        data->key = new char[length];
        if (fread(data->key, sizeof(char) * length, 1, file) != 1) {
            return DESERIALIZATION_ERROR;
        }
        if (fread(&(data->value), sizeof(data->value), 1, file) != 1) {
            return DESERIALIZATION_ERROR;
        }
        node->data.SetLast(data);
    }
    if (!node->leaf)
        for (size_t i = 0; i <= cnt; i++) {
            shared_ptr<TBTree::TNode> sNode(new TNode);
            node->children.SetLast(sNode);
            Loader(file, node->children[i]);
        }
    return OK;
}

