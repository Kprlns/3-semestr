#ifndef BTREE_H
#define BTREE_H
#include "Vector.h"
#include <cstring>
#include <iostream>
#include <memory>
const int DEGREE = 120;
using std::shared_ptr;
typedef const char* TStatus;
#ifndef TBTREE_IMPLEMENTATION
extern TStatus EXIST;
extern TStatus NOT_FOUND;
extern TStatus OK;
extern TStatus MEMORY_ERROR;
extern TStatus FILE_ERROR;
extern TStatus DESERIALIZATION_ERROR;
extern TStatus NOTHING;
#endif

struct TData {
    TData() : key(nullptr), value(0) {}
    TData(char *k, unsigned long long v) {
        key = strdup(k);
        value = v;
    }
    ~TData() {
        if (key) {
            free(key);
        }
    }
    char* key;
    unsigned long long value;
};
class TBTree {

public:

    struct TNode {
        TNode();
        TNode(const TNode& orig);
        TNode& operator=(const TNode& ep);
        ~TNode();
        bool leaf;
        TVector< shared_ptr<TData> > data;
        TVector< shared_ptr<TNode> > children;
    };

    TBTree();
    shared_ptr<TNode>& Root();

    TStatus Delete(char* key);
    TStatus DeleteFromLeaf(shared_ptr<TNode>& node, int i, char* key);
    TStatus DeleteFromNode(shared_ptr<TNode>& node, int i, char* key);
    TStatus DeleteNonEmpty(shared_ptr<TNode>& node, char* key);
    TStatus MergeTree(shared_ptr<TNode>& node, int& i);

    TStatus Insert(shared_ptr<TData>& data);
    TStatus SplitChild(shared_ptr<TNode>& node, int i);
    TStatus InsertNonFull(shared_ptr<TNode>& node, shared_ptr<TData>& data);

    TStatus Search(shared_ptr<TNode>& node, char* key);
    friend int BinarySearch(TVector<shared_ptr<TData> >& data, char *key);

    void PrintTree(shared_ptr<TNode> node, size_t lvl, size_t k);

    TStatus Save(char* fileName);
    TStatus Load(char* fileName);
    void Saver(FILE* file, shared_ptr<TNode>& node);
    TStatus Loader(FILE* file, shared_ptr<TNode>& node);

private:

    shared_ptr<TNode> root;


};
#endif
