#ifndef VECTOR_H
#define VECTOR_H
#include <cstdlib>
const size_t START_SIZE = 8;
template <typename T>
class TVector {

public:
    // constructor DONE
    // destructor DONE
    // Empty & Size & Resize DONE
    // ReNew DONE
    // Insert && Pull DONE
    // SetLast & DelLast DONE
    // operators DONE
    // concat DONE

    TVector(size_t cap=START_SIZE): size(0), capacity(cap), vector(new T[capacity]) {

    }

    bool Empty() {
        return size == 0;
    }

    size_t Size() {
        return size;
    }

    void ReNew() {

        size = 0;
        capacity = START_SIZE;
        delete[] vector;
        vector = new T[capacity];
    }

    void SetLast(T& value) {
        if (size + 5 > capacity) {
            Resize(size + 5);
        }

        vector[size] = value;
        size++;
    }


    void Insert(size_t ind, T& value) {

        if (ind >= 0 && ind <= size) {
            if (size + 1 > capacity)
                Resize(size + 1);
            for (size_t i = size; i > ind; i--)
                vector[i] = vector[i - 1];
            vector[ind] = value;
            size++;
        }
    }
    T& DelLast() {
        size--;
        T& last = vector[size];
        if (size < capacity / 2) {
            Resize(size);
        }

        return last;
    }

    T& Pull(size_t ind) {
        T& data = vector[ind];
        size--;
        for (size_t i = ind; i < size; i++) {
            vector[i] = vector[i + 1];
        }

        if (size < capacity / 2) {
            Resize(size);
        }
        return data;
    }

    void Resize(size_t cap) {
        if (cap == capacity) {
            return;
        }
        else {
            if(cap) {

                if (cap > capacity) {
                    while (cap > capacity) {
                        capacity *= 2;
                    }
                }
                else {
                    capacity = cap;
                    if (size > cap) {
                        size = cap;
                    }
                }

                T *result = new T[capacity];
                for (size_t i = 0; i < size; i++) {
                    result[i] = vector[i];
                }
                delete[] vector;
                vector = result;
            }
        }
    }

    void Concatenation(TVector& tail) {
        for (size_t i = 0; i < tail.size; i++) {

            SetLast(tail.vector[i]);
        }
        tail.ReNew();
    }


    T& operator[](size_t ind) {
        return vector[ind];
    }

    void operator=(const TVector& eq) {
        size = eq.size;
        capacity = eq.capacity;
        for (size_t i = 0; i < size; i++) {
            vector[i] = eq.vector[i];
        }
    }

    ~TVector() {
        size = 0;
        capacity = 0;
        delete[] vector;
    }

private:
    size_t size;
    size_t capacity;
    T* vector;
};
#endif
