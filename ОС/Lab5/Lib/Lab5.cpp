#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "Lab5.h"


Tree* CreateNode(char* val) {
    Tree* newNode = (Tree*)malloc(sizeof(Tree));
    newNode->left = NULL;
    newNode->right = NULL;
    newNode->val = val;
    return newNode;
}

Tree* Min(Tree* root) {
    if(root == NULL) {
        return NULL;
    }
    Tree* tmp = root;
    while(tmp->left != NULL) {
        tmp = tmp->left;
    }
    return tmp;
}

Tree* Max(Tree* root) {
    if(root == NULL) {
        return NULL;
    }
    Tree* tmp = root;
    while(tmp->right != NULL) {
        tmp = tmp->right;
    }
    return tmp;
}

Tree* Insert(Tree* root, char* val) {
    if(root == NULL) {
        printf("Inserted\n");
        return CreateNode(val);
    }

    int cmp = strcmp(val,root->val);
    if(cmp < 0) {
        root->left = Insert(root->left,val);
    }
    if(cmp > 0) {
        root->right = Insert(root->right,val);
    }
    if(cmp == 0) {
        printf("Existing\n");
        free(val);
    }


    return root;
}


Tree* Delete(Tree* root,char* val) {
    if(root == NULL) {
        printf("Not found\n");
        return root;
    }

    int cmp = strcmp(val,root->val);
    if(cmp < 0) {
        root->left = Delete(root->left,val);
    }
    else if(cmp > 0) {
        root->right = Delete(root->right,val);
    }
    else if(cmp == 0) {
        if(root->left == NULL) {
            Tree* tmp = root->right;
            free(root->val);
            free(root);
            return tmp;
        }
        else if(root->right == NULL) {
            Tree* tmp = root->left;
            free(root->val);
            free(root);
            return tmp;
        }

        Tree* tmp = Min(root->right);
        root->val =  strcpy(root->val,tmp->val);
        root->right = Delete(root->right,tmp->val);
    }
    return root;
}

void Print(Tree* root) {
    if(root != NULL) {
        Print(root->left);
        printf("%s\n",root->val);
        Print(root->right);
    }
}

Tree* Search(Tree* root, char* val) {
    if(root == NULL) {
        return NULL;
    }
    int cmp = strcmp(val,root->val);
    if(!cmp) {
        return root;
    }
    if(cmp > 0) {
        return Search(root->right,val);
    }
    if(cmp < 0) {
        return Search(root->left,val);
    }
}



void Menu() {
    printf("1.     Insert\n");
    printf("2.     Delete\n");
    printf("3.     Print\n");
    printf("4.     Search\n");
    printf("5.     Min\n");
    printf("6.     Max\n");
    printf("7.     Delete tree\n");
    printf("8.     Exit\n");
}
