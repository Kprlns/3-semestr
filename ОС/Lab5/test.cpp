#include <stdio.h>
#include <stdlib.h>

struct Test;

struct Test {
    int val;
    Test* next;
};


int main() {
    Test* t1 = (Test*)malloc(sizeof(Test));
    Test* t2 = (Test*)malloc(sizeof(Test));

    t1->val = 1;
    t2->val = 2;

    t1->next = t2;

    free(t2);

    if(t1->next == NULL) {
        printf("NULL\n");
    }
    else {
        printf("HUYULL\n");
    }
    free(t1);
}