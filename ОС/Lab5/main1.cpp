#include <stdio.h>
#include <stdlib.h>
#include "./Lib/Lab5.h"
int main() {
    Tree* root = NULL;
    while(1) {
        Menu();
        int cmd;
        scanf("%d",&cmd);
        if(cmd == 1) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            root = Insert(root,tmp);

        }
        if(cmd == 2) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            root = Delete(root,tmp);
            free(tmp);
        }
        if(cmd == 3) {
            Print(root);
        }
        if(cmd == 4) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            Tree* srch = Search(root,tmp);
            if(srch == NULL) {
                printf("Not found\n");
            }
            else {
                printf("Found\n");
            }
            free(tmp);
        }
        if(cmd == 5) {
            Tree* tmp = Min(root);
            printf("%s\n",tmp->val);
        }
        if(cmd == 6) {
            Tree* tmp = Max(root);
            printf("%s\n",tmp->val);
        }
        if(cmd == 7) {
            while(root != NULL) {
                root = Delete(root,root->val);
            }
        }
        if(cmd == 8) {
            while(root != NULL) {
                root = Delete(root,root->val);
            }
            break;
        }
    }
}