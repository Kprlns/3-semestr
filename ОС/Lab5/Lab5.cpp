#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "Lab5.h"


Tree* CreateNode(char* val) {
    Tree* newNode = (Tree*)malloc(sizeof(Tree));
    newNode->left = NULL;
    newNode->right = NULL;
    newNode->val = val;
    return newNode;
}

Tree* Min(Tree* root) {
    Tree* tmp = root;
    while(tmp->left != NULL) {
        tmp = tmp->left;
    }
    return tmp;
}

Tree* Max(Tree* root) {
    Tree* tmp = root;
    while(tmp->right != NULL) {
        tmp = tmp->right;
    }
    return tmp;
}

Tree* Insert(Tree* root, char* val) {
    if(root == NULL) {
        printf("Inserted\n");
        return CreateNode(val);
    }

    int cmp = strcmp(val,root->val);
    if(cmp < 0) {
        root->left = Insert(root->left,val);
    }
    if(cmp > 0) {
        root->right = Insert(root->right,val);
    }
    if(cmp == 0) {
        printf("Existing\n");
        free(val);
    }


    return root;
}


Tree* Delete(Tree* root,char* val) {
    if(root == NULL) {
        printf("Not found\n");
        return root;
    }

    int cmp = strcmp(val,root->val);
    if(cmp < 0) {
        root->left = Delete(root->left,val);
    }
    else if(cmp > 0) {
        root->right = Delete(root->right,val);
    }
    else if(cmp == 0) {
        if(root->left == NULL) {
            Tree* tmp = root->right;
            free(root->val);
            free(root);
            return tmp;
        }
        else if(root->right == NULL) {
            Tree* tmp = root->left;
            free(root->val);
            free(root);
            return tmp;
        }

        Tree* tmp = Min(root->right);
        root->val =  strcpy(root->val,tmp->val);
        root->right = Delete(root->right,tmp->val);
    }
    return root;
}

void Print(Tree* root) {
    if(root != NULL) {
        Print(root->left);
        printf("%s\n",root->val);
        Print(root->right);
    }
}

Tree* Search(Tree* root, char* val) {
    if(root == NULL) {
        return NULL;
    }
    int cmp = strcmp(val,root->val);
    if(!cmp) {
        return root;
    }
    if(cmp > 0) {
        return Search(root->right,val);
    }
    if(cmp < 0) {
        return Search(root->left,val);
    }

}
/*
void Insert(Tree* root, char* val) {
    if(root == NULL) {
        root = CreateNode(val);
        return;
    }
    else {
        Tree* iter = root;
        while(1) {
            int cmp = strcmp(val, iter->val);
            if(!cmp) {
                printf("Exist!\n");
                free(val);
                return;
            }
            else {
                if(cmp == 1) {
                    if(iter->right == NULL) {
                        iter->right = CreateNode(val);
                        printf("Inserted.\n");
                        return;
                    }
                    else {
                        iter = iter->right;
                    }
                }
                if(cmp == -1) {
                    if(iter->left == NULL) {
                        iter->left = CreateNode(val);
                        printf("Inserted.\n");
                        return;
                    }
                    else {
                        iter = iter->left;
                    }
                }
            }
        }
    }
}
 */



/*
void Delete(Tree* root,char* val) {
    if(root == NULL) {
        printf("Tree is Empty");
        return;
    }
    Tree* iter = root;
    int cmp = strcmp(val, iter->val);
    if(iter->left == NULL && iter->right == NULL) {
        if(!cmp) {
            free(root->val);
            free(root);
            free(val);
            printf("Deleted.\n");
            root = NULL;
            return;
        }
        else {
            printf("Not found\n");
        }
    }
    Tree* iter1;
    bool right;
    while(1) {
        cmp = strcmp(val, iter->val);
        if(!cmp) {
            //Удаление
            if(iter->left == NULL && iter->right == NULL) {
                if(right) {
                    iter1->right = NULL;
                }
                else {
                    iter1->left = NULL;
                }
                free(iter->val);
                free(iter);
                printf("Deleted.\n");
                return;
            }
            if(iter->left == NULL) {
                free(iter->val);
                iter->val = iter->right->val;
                Tree* toDel = iter->right;
                iter->right = iter->right->right;
                iter->left = iter->right->left;
                free(toDel);
                printf("Deleted.\n");
                return;
            }
            if(iter->right == NULL) {
                free(iter->val);
                iter->val = iter->left->val;
                Tree* toDel = iter->left;
                iter->right = iter->left->right;
                iter->left = iter->left->left;
                free(toDel);
                printf("Deleted.\n");
                return;
            }
            if(iter->left->right != NULL) {
                Tree* iter2 = iter->left;
                while(iter2->right->right != NULL) {
                    iter2 = iter2->right;
                }
                free(iter->val);
                iter->val = iter2->right->val;
                free(iter2->right);
                iter2->right = NULL;
                printf("Deleted.\n");
                return;
            }
            if(iter->right->left != NULL) {
                Tree* iter2 = iter->right;

            }
        }
        else {
            if(cmp == 1) {
                if(iter->right == NULL) {
                    printf("Not found.\n");
                    free(val);
                    return;
                }
                right = true;
                iter1 = iter;
                iter = iter->right;
            }
            if(cmp == -1){
                if(iter->left == NULL) {
                    printf("Not found.\n");
                    free(val);

                    return;
                }
                right = false;
                iter1 = iter;
                iter = iter->right;
            }
        }
    }
}
*/


void Menu() {
    printf("1.     Insert\n");
    printf("2.     Delete\n");
    printf("3.     Print\n");
    printf("4.     Search\n");
    printf("5.     Min\n");
    printf("6.     Max\n");
    printf("7.     Delete tree\n");
    printf("8.     Exit\n");
}
/*
int main() {
    Tree* root = NULL;
    while(1) {
        Menu();
        int cmd;
        scanf("%d",&cmd);
        if(cmd == 1) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            root = Insert(root,tmp);

        }
        if(cmd == 2) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            root = Delete(root,tmp);
            free(tmp);
        }
        if(cmd == 3) {
            Print(root);
        }
        if(cmd == 4) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            Tree* srch = Search(root,tmp);
            if(srch == NULL) {
                printf("Not found\n");
            }
            else {
                printf("Found\n");
            }
            free(tmp);
        }
        if(cmd == 5) {
            Tree* tmp = Min(root);
            printf("%s",tmp->val);
        }
        if(cmd == 6) {
            Tree* tmp = Max(root);
            printf("%s",tmp->val);
        }
        if(cmd == 7) {
            while(root != NULL) {
                root = Delete(root,root->val);
            }
        }
        if(cmd == 8) {
            while(root != NULL) {
                root = Delete(root,root->val);
            }
            break;
        }
    }
}
*/