#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include "Lab5.h"
#pragma GCC diagnostic push    //Save actual diagnostics state
#pragma GCC diagnostic ignored "-Wpedantic"    //Disable pedantic


//extern void Menu();

int main() {
    Tree* root = NULL;


    void* lib = dlopen("libtree1.dylib",RTLD_NOW | RTLD_GLOBAL);
    if(lib == NULL) {
        printf("Library want't open!\n");
        exit(1);
    }
    void (*Menu)() = dlsym(lib,"Menu");
    void (*Print)(Tree*) = dlsym(lib,"Print");

    Tree* (*Insert)(Tree*,char*) = dlsym(lib,"Insert");
    Tree* (*Delete)(Tree*,char*) = dlsym(lib,"Delete");
    Tree* (*Search)(Tree*,char*) = dlsym(lib,"Search");

    Tree* (*Max)(Tree*) = dlsym(lib,"Max");
    Tree* (*Min)(Tree*) = dlsym(lib,"Min");



    while(1) {
        Menu();
        int cmd;
        scanf("%d",&cmd);
        if(cmd == 1) {
            printf("Enter string:\n");
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            root = Insert(root,tmp);
        }
        if(cmd == 2) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            root = Delete(root,tmp);
            free(tmp);
        }
        if(cmd == 3) {
            Print(root);
        }
        if(cmd == 4) {
            char* tmp = (char*)malloc(257 * sizeof(char));
            scanf("%s",tmp);
            Tree* srch = Search(root,tmp);
            if(srch == NULL) {
                printf("Not found\n");
            }
            else {
                printf("Found\n");
            }
            free(tmp);
        }
        if(cmd == 5) {
            Tree* tmp = Min(root);
            if(tmp == NULL) {
                printf("Tree is empty\n");
            }
            else {
                printf("%s\n", tmp->val);
            }
        }
        if(cmd == 6) {
            Tree* tmp = Max(root);
            if(tmp == NULL) {
                printf("Tree is empty\n");
            }
            else {
                printf("%s\n",tmp->val);

            }
        }
        if(cmd == 7) {
            while(root != NULL) {
                root = Delete(root,root->val);
            }
        }
        if(cmd == 8) {
            while(root != NULL) {
                root = Delete(root,root->val);
            }
            break;
        }
    }
    dlclose(lib);
}