#ifndef OC_LAB5_H
#define OC_LAB5_H

#include <strings.h>

typedef struct Tree {
    char* val;
    struct Tree* left;
    struct Tree* right;
}Tree;

Tree* Insert(Tree* root, char* str);
Tree* Delete(Tree* root, char* val);
Tree* Search(Tree* root, char* val);

void Print(Tree* root);

Tree* Max(Tree* root);
Tree* Min(Tree* root);

Tree* CreateNode(char* val);



void Menu();

#endif //OC_LAB5_H
