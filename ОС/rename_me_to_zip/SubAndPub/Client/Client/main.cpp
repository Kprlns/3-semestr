#include <time.h>
#include <string.h>
#include <stdio.h>
#include "zmq.h"
#include <Windows.h>

typedef struct MD
{
	int clientId;
	int messageNumber;
	char message[128];
} MessageData;

int main(int argc, char const *argv[])
{
	srand(time(0));
	int id = rand();
	void* context = zmq_ctx_new();
	printf("Publisher %d Starting...\n", id);

	void* publishSocket = zmq_socket(context, ZMQ_PUB);
	zmq_bind(publishSocket, "tcp://*:4040");
	int count = 0;
	for (;;)
	{
		
		MessageData md;
		md.clientId = id;
		md.messageNumber = count;
		memcpy(md.message, "Hello world\0", strlen("Hello world\0")+1);
		zmq_msg_t zmqMessage;
		zmq_msg_init_size(&zmqMessage, sizeof(MessageData));
		memcpy(zmq_msg_data(&zmqMessage), &md, sizeof(MessageData));

		printf("Sending: - %d\n", count);
		zmq_send(publishSocket, "lol", 3, ZMQ_SNDMORE);
		int send = zmq_msg_send(&zmqMessage, publishSocket, 0);
		zmq_msg_close(&zmqMessage);

		Sleep(1000);
		count++;
	}
	zmq_close(publishSocket);
	zmq_ctx_destroy(context);

	return 0;
}


