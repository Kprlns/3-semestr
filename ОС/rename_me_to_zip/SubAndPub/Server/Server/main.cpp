#include <string.h>
#include <stdio.h>
#include "zmq.h"
#include <windows.h>

typedef struct MD
{
	int clientId;
	int messageNumber;
	char message[128];
} MessageData;

int main(int argc, char const *argv[])
{
	void* context = zmq_ctx_new();
	void* serverSocket = zmq_socket(context, ZMQ_SUB);
	zmq_connect(serverSocket, "tcp://localhost:4040");

	zmq_setsockopt(serverSocket, ZMQ_SUBSCRIBE, "lol", 3);

	printf("Starting...\n");
	for (;;)
	{
		zmq_msg_t message;
		char filter[32];
		zmq_msg_init(&message);
		zmq_recv(serverSocket, filter, 4, 0);
		filter[3] = '\0';
		printf("%s\n", filter);
		zmq_msg_recv(&message, serverSocket, 0);
		MessageData *m = (MessageData *)zmq_msg_data(&message);
		printf("Message from client: %d  messageId: %d message: %s\n", m->clientId, m->messageNumber, m->message);
		zmq_msg_close(&message);
	}
	zmq_close(serverSocket);
	zmq_ctx_destroy(context);

	return 0;
}