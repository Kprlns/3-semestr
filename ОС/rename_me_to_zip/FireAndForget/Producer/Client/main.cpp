#include <time.h>
#include <string.h>
#include <stdio.h>
#include "zmq.h"
#include <Windows.h>

typedef struct MD
{
	int clientId;
	int messageNumber;
	char message[128];
} MessageData;

int main(int argc, char const *argv[])
{
	void* context = zmq_ctx_new();
	srand(time(0));
	printf("Producer starting...\n");

	void* senderSocket = zmq_socket(context, ZMQ_PUSH);
	zmq_bind(senderSocket, "tcp://*:4041");
	int count = 0;
	for (;;)
	{
		MessageData md;
		md.clientId = 0;
		md.messageNumber = count;
		memcpy(md.message, "Hello world\0", strlen("Hello world\0")+1);
		zmq_msg_t zmqMessage;
		zmq_msg_init_size(&zmqMessage, sizeof(MessageData));
		memcpy(zmq_msg_data(&zmqMessage), &md, sizeof(MessageData));

		printf("Sending: - %d\n", count);
		int send = zmq_msg_send(&zmqMessage, senderSocket, 0);
		zmq_msg_close(&zmqMessage);

		Sleep(500);
		count++;
	}
	// We never get here though.
	zmq_close(senderSocket);
	zmq_ctx_destroy(context);

	return 0;
}


