#include <string.h>
#include <stdio.h>
#include "zmq.h"
#include <windows.h>

typedef struct MD
{
	int clientId;
	int messageNumber;
	char message[128];
} MessageData;

int main(int argc, char const *argv[])
{
	void* context = zmq_ctx_new();
	void* pullSocket = zmq_socket(context, ZMQ_PULL);
	zmq_connect(pullSocket, "tcp://localhost:4041");
	printf("Starting...\n");
	for (;;)
	{
		zmq_msg_t message;
		zmq_msg_init(&message);
		zmq_msg_recv(&message, pullSocket, 0);
		MessageData *m = (MessageData *)zmq_msg_data(&message);
		printf("Message from client: %d  messageId: %d message: %s\n", m->clientId, m->messageNumber, m->message);
		zmq_msg_close(&message);
		Sleep(2000);
	}
	zmq_close(pullSocket);
	zmq_ctx_destroy(context);

	return 0;
	return 0;
}