#include <string.h>
#include <stdio.h>
#include "zmq.h"
#include <windows.h>

typedef struct MD
{
	int clientId;
	int messageNumber;
	char message[128];
} MessageData;

int main(int argc, char const *argv[])
{
	void* context = zmq_ctx_new();
	void* serverSocket = zmq_socket(context, ZMQ_REP);
	zmq_bind(serverSocket, "tcp://*:4040");
	printf("Starting...\n");
	for (;;)
	{
		zmq_msg_t message;
		zmq_msg_init(&message);
		zmq_msg_recv(&message, serverSocket, 0);
		MessageData *m = (MessageData *)zmq_msg_data(&message);
		printf("Message from client: %d  messageId: %d message: %s\n", m->clientId, m->messageNumber, m->message);
		zmq_msg_close(&message);

		zmq_msg_t reply;
		zmq_msg_init_size(&reply, strlen("ok")+1);
		memcpy(zmq_msg_data(&reply), "ok\0", 3);
		zmq_msg_send(&reply, serverSocket, 0);
		zmq_msg_close(&reply);
	}
	zmq_close(serverSocket);
	zmq_ctx_destroy(context);

	return 0;
}