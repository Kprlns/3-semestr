#include <time.h>
#include <string.h>
#include <stdio.h>
#include "zmq.h"
#include <Windows.h>

typedef struct MD
{
	int clientId;
	int messageNumber;
	char message[128];
} MessageData;

int main(int argc, char const *argv[])
{
	void* context = zmq_ctx_new();
	srand(time(0));
	int clientId = rand();
	printf("Client %d Starting...\n", clientId);

	void* senderSocket = zmq_socket(context, ZMQ_REQ);
	zmq_connect(senderSocket, "tcp://localhost:4040");
	int count = 0;
	for (;;)
	{
		MessageData md;
		md.clientId = clientId;
		md.messageNumber = count;
		memcpy(md.message, "Hello world\0", strlen("Hello world\0")+1);
		zmq_msg_t zmqMessage;
		zmq_msg_init_size(&zmqMessage, sizeof(MessageData));
		memcpy(zmq_msg_data(&zmqMessage), &md, sizeof(MessageData));

		printf("Sending: - %d\n", count);
		int send = zmq_msg_send(&zmqMessage, senderSocket, 0);
		zmq_msg_close(&zmqMessage);

		zmq_msg_t reply;
		zmq_msg_init(&reply);
		zmq_msg_recv(&reply, senderSocket, 0);
		size_t repSize = zmq_msg_size(&reply);
		printf("Received: - %d %s\n", repSize, zmq_msg_data(&reply));
		zmq_msg_close(&reply);

		Sleep(1000);
		count++;
	}
	// We never get here though.
	zmq_close(senderSocket);
	zmq_ctx_destroy(context);

	return 0;
}


