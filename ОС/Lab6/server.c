#include <zmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "structures.h"

static List* clients = NULL;

void Error() {
    printf("Oops, smth wrong!\n");
    exit(1);
}
/*************************************************
 *                   Messages                    *
 *************************************************/
Request* GetMessage(void* server) {
    zmq_msg_t message;
    if( zmq_msg_init(&message) ) Error();
    if( zmq_msg_recv(&message,server,0) == -1 ) Error();

    Request* req =(Request*)malloc(sizeof(Request));
    memcpy(req,zmq_msg_data(&message), sizeof(Request));
    if( zmq_msg_close(&message) ) Error();
    return req;
}

void SendMessage(void* server, Reply* rep) {
    zmq_msg_t reply;
    zmq_msg_init_size(&reply, sizeof(Reply));
    memcpy(zmq_msg_data(&reply),rep, sizeof(Reply));
    if( zmq_msg_send(&reply,server,0) == -1 ) Error();
    if( zmq_msg_close(&reply) ) Error();
}

void PrintRequest(Request* request) {
    printf("ID: %.20lf\n",request->id);
    printf("ID2: %.20lf\n",request->recipientId);
    printf("Request: %d\n",request->req);
    printf("Value: %d\n",request->val);
}


/*************************************************
 *                Clients list                   *
 *************************************************/

List* newClient(double id, unsigned int balance) {
    List* client = (List*)malloc(sizeof(List));

    client->id = id;
    client->balance = balance;
    client->next = NULL;
    return client;
}

List* Search(double id) {
    List* iterator = clients;
    while(iterator != NULL) {
        if(iterator->id == id) {
            return iterator;
        }
        iterator = iterator->next;
    }
    return iterator;
}

void addClient(double id, unsigned int balance) {
    List* client = newClient(id,balance);
    printf("Added new client: %lf\n",client->id);
    if(clients == NULL) {
        clients = client;
    }
    else {
        client->next = clients;
        clients = client;
    }
}

/*************************************************
 *                    Requests                   *
 *************************************************/

void GetCash(Request* request,Reply* reply) {
    List* client = Search(request->id);
    if(client == NULL) {
        reply->success = -1;
    }
    else if(client->balance < request->val) {
        reply->success = 0;
    }
    else {
        reply->success = 1;
        client->balance -= request->val;
        reply->val = client->balance;
    }
}

void Transfer(Request* request, Reply* reply) {
    List* sender = Search(request->id);
    if( sender == NULL ) {
        reply->success = -1;
        return;
    }
    List* recipient = Search(request->recipientId);

    if(recipient == NULL) {
        reply->success = -2;
        return;
    }

    if(sender->balance < request->val) {
        reply->success = 0;
        return;
    }
    sender->balance -= request->val;
    recipient->balance += request->val;
    reply->success = 1;
    reply->val = sender->balance;
}

unsigned int AddCash(Request* request) {
    List* iter = Search(request->id);
    unsigned int res;
    if(iter == NULL) {
        addClient(request->id,request->val);
        res = request->val;
    }
    else {
        iter->balance += request->val;
        iter->balance;
    }
    return res;
}

/*************************************************
 -1 - нет счета клиента-отправителя
 -2 - нет счета клиента-получателя
  0 - недостаточно средств у отправителя
  1 - успешно проведено
 *************************************************/

/*************************************************
 *                   Database                    *
 *************************************************/

void Load(char* filename) {
    if(access(filename,F_OK) != -1) {
        FILE* file = fopen(filename,"rb");
        if(file == NULL) Error();
        Pair client;
        while(fread(&client, sizeof(Pair),1,file) == 1) {
            addClient(client.id,client.balance);
        }
        fclose(file);
    }
    else {
        FILE* file = fopen(filename, "wb");
        fclose(file);
    }
}



void Update(char* filename) {

    Pair client;
    size_t len = strlen(filename);
    char* tmpName = (char*)malloc((len + 1)* sizeof(char));
    memcpy(tmpName,filename,len);
    tmpName[len] = 't';
    tmpName[len + 1] = '\0';

    FILE* tmpFile = fopen(tmpName,"wb");
    if(tmpFile == NULL) Error();
    List* iter = clients;
    while(iter != NULL) {
        client.id = iter->id;
        client.balance = iter->balance;
        fwrite(&client, sizeof(Pair), 1, tmpFile);
        iter = iter->next;
    }
    fclose(tmpFile);
    if(remove(filename)) {
        printf("Error while deleting file\n");
        exit(1);
    }
    if(rename(tmpName,filename)) {
        printf("Rename error\n");
        exit(1);
    }
    free(tmpName);
}


/*************************************************
 *                      Main                     *
 *************************************************/


int main(int argc, char* argv[]) {

    if(argc != 2) {
        printf("Wrong args\n");
        exit(1);
    }

    char* port = (char*)malloc(32 * sizeof(char));
    memcpy(port,"tcp://*:",strlen("tcp://*:"));
    int i = 0;
    for(i = 0; i < strlen(argv[1]);i++) {
        port[8 + i] = argv[1][i];
    }
    port[i + 8] = '\0';

    printf("Loading data...\n");
    Load(argv[1]);
    printf("Done!\n");

    void* context = zmq_ctx_new();
    if(context == NULL) Error();

    void* server = zmq_socket(context,ZMQ_REP);
    if( zmq_bind(server,port) ) Error();

    Reply reply;
    printf("Server started!\n");
    while(1) {
        Request* request = GetMessage(server);
        if(request->req == 1) {
            //PrintRequest(request);
            reply.success = 1;
            reply.val = AddCash(request);
            printf("%lf: added %u$\n",request->id,request->val);
            SendMessage(server,&reply);
        }

        if(request->req == 2) {
            GetCash(request,&reply);
            SendMessage(server,&reply);
            if(reply.success == 1) {
                printf("%lf: got %u$\n",request->id,request->val);
            }
            else {
                printf("%lf: failed to get %d$ form his account\n",request->id,request->val);
            }
        }

        if(request->req == 3) {
            Transfer(request,&reply);
            SendMessage(server,&reply);
            if(reply.success == 1) {
                printf("Successful transfer: %lf %d$ to %lf\n",request->id,request->val,request->recipientId);
            }
            else {
                printf("Failed transfer: %lf %d$ to %lf\n",request->id,request->val,request->recipientId);
            }
        }

        if(request->req == 4) {
            List* iter = Search(request->id);
            if(iter == NULL) {
                reply.success = -1;
            }
            else {
                reply.success = 1;
                reply.val = iter->balance;
            }

            SendMessage(server,&reply);

        }

        if(reply.success == 1 && request->req != 4) {
            Update(argv[1]);
        }
        free(request);

        if(request->req == 8) {
            break;
        }



    }

    if(zmq_close(server)) Error();

    if(zmq_ctx_destroy(context)) Error();

    return 0;

}

/*

        if(request->req == 0) {
            PrintRequest(request);
            free(request);
            scanf("%d%d",&reply.val,&reply.success);
            SendMessage(server,&reply);
        }

             zmq_msg_t message;
            if( zmq_msg_init(&message) ) Error();
            if( zmq_msg_recv(&message,server,0) == -1 ) Error();

            Message* msg = (Message*)zmq_msg_data(&message);
            printf("Received: %d %d\n",msg->first, msg->second);
            if( zmq_msg_close(&message) ) Error();

            int res = msg->second + msg->first;
            zmq_msg_t reply;
            zmq_msg_init_size(&reply, sizeof(int));
            memcpy(zmq_msg_data(&reply),&res, sizeof(int));

            if( zmq_msg_send(&reply,server,0) == -1 ) Error();
            if( zmq_msg_close(&reply) ) Error();
 */