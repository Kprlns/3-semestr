#include <stdio.h>

typedef struct req {
    double id;
    double recipientId;
    int req;
    unsigned int val;
}Request;

typedef struct rep {
    int success;
    unsigned int val;
}Reply;

struct list;

typedef struct list {
    double id;
    unsigned int balance;
    struct list* next;
}List;

typedef struct pair {
    double id;
    unsigned int balance;
}Pair;

typedef struct thr {
    Request* request;
    void* sender;
}ThreadRequest;

typedef struct str {
    char p1mark;
    char p2mark;

};