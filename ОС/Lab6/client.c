#include <zmq.h>
#include <stdio.h>
#include <stdlib.h>
#include "structures.h"
#include <string.h>
#include <czmq.h>
#include <errno.h>
#include <pthread.h>


void Error() {
    printf("Oops, smth wrong!\n");
    exit(1);
}

void Error1() {
    printf("vse ploho\n");
}

/*************************************************
 *                   Messages                    *
 *************************************************/
Reply* GetReply(void* server) {
    zmq_msg_t message;
    if( zmq_msg_init(&message) ) Error();
    if( zmq_msg_recv(&message,server,0) == -1 ) Error();

    Reply* rep = (Reply*)malloc(sizeof(Reply));
    memcpy(rep,zmq_msg_data(&message), sizeof(Reply));

    if( zmq_msg_close(&message) ) Error();
    return rep;
}

void SendRequest(void* server, Request* req) {
    zmq_msg_t request;
    zmq_msg_init_size(&request, sizeof(Request));
    memcpy(zmq_msg_data(&request),req, sizeof(Request));
    if( zmq_msg_send(&request,server,0) == -1 ) Error();
    if( zmq_msg_close(&request) ) Error();
}

void PrintReply(Reply* reply) {
    printf("Success: %d\n",reply->success);
    printf("Value:   %d\n",reply->val);
}

/*************************************************
 *                     Thread                    *
 *************************************************/
void PrintResult(Reply* reply) {
    if(!reply->success) {
        printf("\nError: Not enough funds\n");
    }
    else if(reply->success == -1) {
        printf("\nError: You haven't yet opened an account\n");
    }
    else if(reply->success == -2) {
        printf("\nError: Unknown recipient\n");
    }
    else if(reply->success == 1) {
        printf("\nSuccessful!\n");
        printf("Now your balance is: %d$\n",reply->val);
    }
}


void* thread(void* req) {

    ThreadRequest* request = (ThreadRequest*)req;
    SendRequest(request->sender,request->request);

    zmq_msg_t message;
    if( zmq_msg_init(&message) ) Error();
    if( zmq_msg_recv(&message,request->sender,0) == -1 ) Error();

    Reply* rep = (Reply*)malloc(sizeof(Reply));
    memcpy(rep,zmq_msg_data(&message), sizeof(Reply));
    if( zmq_msg_close(&message) ) Error();
    if(request->request->req == 4) {
        printf("Your balance: %d$\n",rep->val);
    }
    else {
        PrintResult(rep);
    }
    free(rep);
    return NULL;
}

/*************************************************
 *                      Main                     *
 *************************************************/

void Menu() {
    printf("1.     Add money to your balance\n");
    printf("2.     Get cash\n");
    printf("3.     Transfer\n");
    printf("4.     Check balance\n");
    printf("5.     Exit\n");
}



//балланс
//внести
//снять
//перевод

int main(int argc, char* argv[]) {

    if(argc != 2) {
        printf("Wrong args\n");
        exit(1);
    }

    char* port = (char*)malloc(32 * sizeof(char));
    memcpy(port,"tcp://localhost:",strlen("tcp://localhost:"));
    int i = 0;
    for(i = 0; i < strlen(argv[1]);i++) {
        port[16 + i] = argv[1][i];
    }
    port[i + 16] = '\0';
    void* context = zmq_ctx_new();
    if(context == NULL) Error();

    void* sender = zmq_socket(context,ZMQ_REQ);
    if( zmq_connect(sender,port) ) Error();



    double id;
    printf("Enter your id:\n");
    scanf("%lf",&id);


    while(1) {

        int cmd;
        Menu();
        scanf("%d",&cmd);


        pthread_t query;
        ThreadRequest thrRequest;
        Request request;
        request.id = id;
        thrRequest.sender = sender;
        thrRequest.request = &request;


        if(cmd == 1) {
            request.req = 1;
            printf("Enter amount:\n");
            scanf("%u",&request.val);
        }

        if(cmd == 2) {
            request.req = 2;
            printf("Enter amount:\n");
            scanf("%u",&request.val);
        }

        if(cmd == 3) {
            request.req = 3;
            printf("Enter recipient:\n");
            scanf("%lf",&request.recipientId);
            printf("Enter amount:\n");
            scanf("%u",&request.val);
        }

        if(cmd == 4) {
            request.req = 4;
        }
        if(cmd == 5) {
            break;
        }
        printf("Sending request...\n");
        pthread_create(&query,NULL,thread,&thrRequest);
        pthread_detach(query);
    }
    if(zmq_close(sender)) Error();

    if(zmq_ctx_destroy(context)) Error();

}

/*
        if(cmd == 0) {
            Request req;
            scanf("%lf",&req.id);
            scanf("%lf",&req.recipientId);
            scanf("%d",&req.req);
            scanf("%d",&req.val);
            SendRequest(sender,&req);
            Reply* rep = GetReply(sender);
            PrintReply(rep);
            free(rep);
        }
             Message m;
            scanf("%d%d",&m.first,&m.second);

            zmq_msg_t request;
            if( zmq_msg_init_size(&request, sizeof(Message)) ) Error();
            memcpy(zmq_msg_data(&request),&m, sizeof(Message));

            if( zmq_msg_send(&request,sender,0) == -1 ) Error();

            zmq_msg_t reply;
            if( zmq_msg_init(&reply) ) Error();
            if( zmq_msg_recv(&reply, sender, 0) == -1 ) Error();

            int res;
            memcpy(&res,zmq_msg_data(&reply),sizeof(int));
            printf("Result: %d\n",res);
            zmq_msg_close(&reply);
 */