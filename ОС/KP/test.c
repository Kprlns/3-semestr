#include <stdlib.h>
#include <stdio.h>

void PrintfField(char** field, int size) {
    for(int i = 0; i < size; ++i) {
        for(int j = 0; j < size; ++j) {
            printf(" %c ", field[i][j]);
            if(j != (size - 1)) {
                printf("|");
            }
        }
        printf("\n");
        if(i != (size - 1)) {
            for(int j = 0; j < 4 * size - 1; j++) {
                printf("-");
            }
            printf("\n");
        }
    }
}

void CheckWin(char** field, int x, int y, int row, int size,char check) {
    //char a = 'X';
    int cnt = 0;
    //int c1 = y - (row - 1);
    for(int c1 = x - (row - 1); c1 <= x ;c1++) {
        cnt = 0;
        if(c1 < 0 || c1 >= size) {
            continue;
        }
        for(int i = 0; i < row; i++) {
            if((c1 + i) >= size || field[c1 + i][y] != check) {
                break;
            }
            cnt++;
        }
        if(cnt == row) {
            printf("Win: %d %d\n",c1 ,y);
        }
    }
}

void CheckWin1(char** field, int x, int y, int row, int size, char check) {
    int cnt = 0;
    for(int c1 = y - (row - 1); c1 <= y ; ++c1) {
        cnt = 0;
        if(c1 < 0 || c1 >= size) {
            continue;
        }
        for(int i = 0; i < row; i++) {
            if((c1 + i) >= size || field[x][c1 + i] != check) {
                break;
            }
            cnt++;
        }
        if(cnt == row) {
            printf("Win1: %d %d\n",x,c1);
        }
    }
}

void CheckWin2(char** field, int x, int y, int row, int size, char check) {
    int cnt = 0;
    //int c1 = row - 1;
    for(int c1 = row - 1; c1 >= 0; c1--) {
        cnt = 0;
        int x1 = x - c1;
        int y1 = y - c1;
        if( x1 < 0 || y1 < 0 || x1 >= size || y1 >= size) {
            continue;
        }

        for(int i = 0; i < row; ++i) {
            if((x1 + i) >= size || (y1 + i) >= size || field[x1 + i][y1 + i] != check) {
                break;
            }
            cnt++;
        }
        if(cnt == row) {
            printf("Win2: %d %d\n",x1,y1);
        }
    }
}

void CheckWin3(char** field, int x, int y, int row, int size, char check) {
    int cnt = 0;
    //int c1 = row - 1;
    for(int c1 = row - 1; c1 >= 0; c1--) {
        cnt = 0;
        int x1 = x + c1;
        int y1 = y - c1;
        if( x1 < 0 || y1 < 0 || x1 >= size || y1 >= size) {
            continue;
        }
        for(int i = 0; i < row; i++) {
            if((x1 - i) < 0 || (y1 + i) >= size || field[x1 - i][y1 + i] != check) {
                break;
            }
            cnt++;
        }
        if(cnt == row) {
            printf("Win3: %d %d\n",x1,y1);
        }
    }
}

int main() {
    int size;
    printf("Size:\n");

    scanf("%d",&size);
    int row;
    printf("Row: \n");
    scanf("%d",&row);
    char** field = (char**)malloc(size * sizeof(char*));
    for(int i = 0; i < size; ++i) {
        field[i] = (char*)malloc(size * sizeof(char));
    }
    for(int i = 0; i < size;i++) {
        for(int j = 0; j < size; ++j) {
            field[i][j] = ' ';
        }
    }

    int turnCnt = 0;
    PrintfField(field,size);
    int a,b;
    while(1) {
        //printf("X: ");
        scanf("%d%d",&a,&b);
        a--;
        b--;
        turnCnt++;
        field[a][b] = 'X';
        PrintfField(field,size);
        //CheckWin(field,a,b,row,size,'X');
        //CheckWin1(field,a,b,row,size,'X');
        //CheckWin2(field,a,b,row,size,'X');
        CheckWin3(field,a,b,row,size,'X');
        if(a == -1 || b == -1) {
            break;
        }

    }

}