#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <cmath>
void menu() {
    printf("1.     Print string\n");
    printf("2.     Search\n");
    printf("3.     Delete N symbols from specified position\n");
    printf("4.     Insert N symbols after specified position\n");
    printf("5.     Set bounds\n");
    printf("6.     Change file\n");
    printf("7.     Set pagesize\n");
    printf("8.     Info\n");
    printf("9.     Exit  \n");
}

typedef struct File {
    int pos;
    int descriptor;
    int linesSize;
    int lineCounter;
    int* lines;
    long pageSize;

    off_t offset;
    off_t fileSize;

    char* path;
    char* page;

}File;

void Open(File* file) {
    file->descriptor = open(file->path, O_RDONLY | O_CREAT);
    if(file->descriptor == -1) {
        printf("Error while openning file\n");
        exit(1);
    }
    //printf("%d\n",file->descriptor);
    struct stat file_info;
    fstat(file->descriptor, &file_info);
    file->fileSize = file_info.st_size;
    file->pageSize = sysconf(_SC_PAGESIZE);
}

void Rewind(File* file) {
    file->pos = 0;
    file->offset = 0;
    file->linesSize = 0;
    file->lineCounter = 0;
}

/*          search          */

char GetChar(File* file) {
    if(file->pos == file->fileSize) {
        if(munmap(file->page, (size_t)file->pageSize)) {
            printf("\nUNMAPPING ERROR!\n");
            exit(-1);
        }
        return -1;
    }
    if(file->pos % file->pageSize) {
        char c = file->page[file->pos % file->pageSize];
        file->pos++;
        return c;
    }
    else {
        if(file->pos) {
            if(munmap(file->page, (size_t)file->pageSize)) {
                printf("\nUNMAPPING ERROR!\n");
                exit(-1);
            }
            file->offset += file->pageSize;
        }

        file->page = (char*)mmap(NULL, (size_t)file->pageSize, PROT_READ, MAP_PRIVATE, file->descriptor, file->offset);
        if(file->page == MAP_FAILED) {
            printf("\nMAP FILED!\n");
            exit(1);
        }
        char c = file->page[file->pos % file->pageSize];
        file->pos++;
        return c;
    }
}

void ZFunc(int* z, char* pattern, int patternLength) {
    for(int i = 0; i < patternLength;i++) {
        z[i] = 0;
    }
    for(int i = 1; i < patternLength;i++) {
        for(int j = i; j < patternLength; ++j) {
            if(pattern[i] == pattern[j - i]) {
                z[i]++;
            }
            else {
                break;
            }
        }
    }
}

void SPCount(int* z, int* sp2, int patternLength) {
    sp2[0] = 0;
    for(int j = patternLength - 1; j > 0; j--) {
        int i = j + z[j] - 1;
        sp2[i] = z[j];
    }
}

void FindPosition(File* file, int position) {
    int i = file->lineCounter - 1;
    //for(int j = 0; j < i;j++) {
    //    printf("%d ",file->lines[j]);
    //}
    //printf("\n");
    while(file->lines[i] > position) {
        i--;
        if(i == 0) {
            break;
        }
    }
    int k = position - file->lines[i];
    printf("%d %d\n",i + 2,k);
}

void KMP(File* file, char* pattern, int patternLength) {
    if(patternLength == 0) {
        return;
    }
    Rewind(file);
    int* z = (int*)malloc(patternLength * sizeof(int));
    int* sp2 = (int*)malloc(patternLength * sizeof(int));

    ZFunc(z,pattern,patternLength);
    SPCount(z,sp2,patternLength);
    free(z);

    char text;
    bool entries = false;

    int n = patternLength - 1;
    int pCnt = 0;

    file->linesSize = 100;
    file->lines = (int*)malloc(file->linesSize * sizeof(int));
    while(1) {
        text = GetChar(file);

        if(text == '\n') {
            file->lines[file->lineCounter] = file->pos;
            file->lineCounter++;
            if(file->lineCounter == file->linesSize) {
                file->linesSize *= 2;
                file->lines = (int*)realloc(file->lines,(size_t)file->linesSize);
                if(file->lines == nullptr) {
                    printf("\nRealloc failed\n");
                    exit(11);
                }
            }
        }
        if(text == -1) {
            break;
        }
        if((pCnt <= n) && (pattern[pCnt] == text)) {
            pCnt++;
            while(1) {
                if(pCnt == n + 1) {
                    if(!entries) {
                        printf("Entries:\n");
                    }
                    FindPosition(file,file->pos - n);
                    entries = true;
                    break;
                }
                text = GetChar(file);
                if(text == -1) {
                    if(!entries) {
                        printf("No entries\n");
                        free(file->lines);
                        Rewind(file);
                        return;
                    }
                }
                if(text == '\n') {
                    file->lines[file->lineCounter] = file->pos;
                    file->lineCounter++;
                    if(file->lineCounter == file->linesSize) {
                        file->linesSize *= 2;
                        file->lines = (int*)realloc(file->lines,(size_t)file->linesSize);
                        if(file->lines == nullptr) {
                            printf("\nRealloc failed\n");
                            exit(11);
                        }
                    }
                }

                if(pattern[pCnt] == text) {
                    pCnt++;
                }
                else {
                    break;
                }
            }
        }
        if(pCnt == 0) {
            continue;
        }
        else {
            pCnt = sp2[pCnt - 1];
        }
    }
    if(!entries) {
        printf("No entries\n");
    }
    free(file->lines);
    Rewind(file);
}

/*          Del and Insert          */
int ScanFile(File* file) {
    if(file->fileSize == 0) {
        return 0;
    }
    Rewind(file);
    file->linesSize = 100;
    file->lines = (int*)malloc(file->linesSize * sizeof(int));
    while(1) {
        char text = GetChar(file);
        if(text == -1) {
            break;
        }
        if(text == '\n') {
            file->lines[file->lineCounter] = file->pos;
            file->lineCounter++;
            if(file->lineCounter == file->linesSize) {
                file->linesSize *= 2;
                file->lines = (int*)realloc(file->lines,file->linesSize * sizeof(int));
            }
        }
    }
    file->lines[file->lineCounter] = (int)file->fileSize;
    file->lineCounter++;
    int res = file->lineCounter;
    Rewind(file);
    return res;
}

bool Skip(File* file,int toSkip) {
    bool remap = false;
    while(toSkip >= file->pageSize) {
        file->offset += file->pageSize;
        file->pos += file->pageSize;
        toSkip -= file->pageSize;
        remap = true;
    }
    if(file->offset >= file->fileSize) {
        return true;
    }
    if(remap) {
        if(munmap(file->page, (size_t)file->pageSize)) {
            printf("\nUNMAPPING ERROR!\n");
            exit(-1);
        }
        file->page = (char*)mmap(NULL, (size_t)file->pageSize, PROT_READ, MAP_PRIVATE, file->descriptor, file->offset);
        if(file->page == MAP_FAILED) {
            printf("\nMAP FILED!\n");
            exit(1);
        }
    }
    file->pos += (toSkip - 1);
    if(file->pos >= file->fileSize) {
        return true;
    }

    return false;
}

void Del(File* file,int line, int pos, int toSkip) {
    Rewind(file);
    if(file->fileSize == 0) {
        printf("File is empty\n");
        return;
    }

    file->lineCounter = ScanFile(file);

    //for(int i = 0; i < file->lineCounter; ++i) {
    //    printf("%d ",file->lines[i]);
    //}
    //printf("\n");
    if(line > file->lineCounter + 1 || line <= 0) {
        printf("Wrong line\n");
        free(file->lines);
    }
    else {

        int cnt;
        if(line > 1) {
            cnt = file->lines[line - 1] - file->lines[line - 2];
        }
        else {
            cnt = file->lines[line - 1];
        }

        if( pos <= 0 || pos > cnt /*|| !((line == file->lineCounter + 2) && ((file->pos - file->lines[file->linesSize - 1]) > pos))*/) {
            printf("Wrong position\n");
            free(file->lines);
            return;
        }

        int tmp = open("tmpLab4",O_RDWR | O_CREAT | O_TRUNC,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if( tmp == -1 ) {
            printf("Error while openning tmp file\n");
            exit(1);
        }

        bool skipped = false;
        int lineCnt = 1;
        int posCnt = 0;
        while(1) {
            char text = GetChar(file);
            posCnt++;
            if(text == -1) {
                close(file->descriptor);
                close(tmp);
                if(remove(file->path)) {
                    printf("Error while deleting file\n");
                    exit(1);
                }
                if(rename("tmpLab4",file->path)) {
                    printf("Rename error\n");
                    exit(0);
                }
                Open(file);
                free(file->lines);
                return;
            }

            if(!skipped && lineCnt ==  line && pos == posCnt) {
                lineCnt++;
                if(Skip(file,toSkip)) {
                    close(file->descriptor);
                    close(tmp);
                    if(remove(file->path)) {
                        printf("Error while deleting file\n");
                        exit(1);
                    }
                    if(rename("tmpLab4",file->path)) {
                        printf("Rename error\n");
                        exit(0);
                    }
                    Open(file);
                    free(file->lines);
                    return;
                }
                skipped = true;
                continue;
            }

            if(!skipped && text == '\n') {
                lineCnt++;
                if((file->lines[lineCnt + 1] - file->lines[lineCnt]) == 1) {
                    if(lineCnt == line && pos == 1) {
                        Skip(file,toSkip);
                    }
                }
                posCnt = 0;
            }
            write(tmp,&text,sizeof(char));
        }
    }
}

void Insert(File* file, int line, int pos, char* str,int strlen) {
    Rewind(file);
    if(file->fileSize != 0 && line == 1 && pos == 0) {
        int tmp = open("tmpLab4", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if(tmp == -1) {
            printf("Error while oppening tmp file\n");
            exit(0);
        }
        write(tmp,str,strlen * sizeof(char));

        char text;
        while(1) {
            text = GetChar(file);
            if(text == -1) {
                break;
            }
            write(tmp,&text, sizeof(char));
        }
        close(file->descriptor);
        close(tmp);
        if(remove(file->path)) {
            printf("Error while deleting file\n");
            exit(1);
        }
        if(rename("tmpLab4",file->path)) {
            printf("Rename error\n");
            exit(1);
        }
        Open(file);
        Rewind(file);
        free(str);
        return;

    }
    if(file->fileSize == 0 && ((line == 0 && pos == 0) || (line == 1 && pos == 1))) {
        int tmp = open("tmpLab4", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if(tmp == -1) {
            printf("Error while oppening tmp file\n");
            exit(0);
        }

        write(tmp, str, strlen * sizeof(char));
        close(file->descriptor);
        close(tmp);
        if(remove(file->path)) {
            printf("Error while deleting file\n");
            exit(1);
        }
        if(rename("tmpLab4",file->path)) {
            printf("Rename error\n");
            exit(1);
        }
        free(str);
        Open(file);
        Rewind(file);
        return;
    }
    if(file->fileSize == 0) {
        printf("Wrong position\n");
        free(str);
        return;
    }

    file->lineCounter = ScanFile(file);
    if(line > file->lineCounter + 1 || line <= 0) {
        printf("Wrong line\n");
        free(file->lines);
        free(str);
        return;
    }
    else {
        if(pos == 0) {
            line--;
            pos = file->lines[line - 1] - file->lines[line - 2];
        }
        int cnt;
        if(line > 1) {
            cnt = file->lines[line - 1] - file->lines[line - 2];
        }
        else {
            cnt = file->lines[line - 1];
        }

        if( pos <= 0 || pos > cnt ) {
            printf("Wrong position\n");
            free(file->lines);
            free(str);
            return;
        }
        int tmp = open("tmpLab4", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if(tmp == -1) {
            printf("Error while oppening tmp file\n");
            exit(0);
        }

        int posCnt = 0;
        int lineCnt = 1;
        bool inserted = false;
        while(1) {
            char text = GetChar(file);
            posCnt++;
            if(text == -1) {
                close(file->descriptor);
                close(tmp);
                if(remove(file->path)) {
                    printf("Error while deleting file\n");
                    exit(1);
                }
                if(rename("tmpLab4",file->path)) {
                    printf("Rename error\n");
                    exit(1);
                }
                Open(file);
                free(file->lines);
                free(str);
                return;
            }

            if( !inserted &&lineCnt == line && posCnt == pos) {
                write(tmp,&text,sizeof(char));
                write(tmp,str,strlen * sizeof(char));
                inserted = true;
                continue;
            }

            if( !inserted && text == '\n') {
                posCnt = 0;
                lineCnt++;
            }

            write(tmp,&text,sizeof(char));
        }
    }

}

void PrintLine(File* file,int line) {
    Rewind(file);
    if(file->fileSize == 0) {
        printf("File is empty\n");
        return;
    }
    if(line <= 0) {
        printf("Wrong line number\n");
        return;
    }
    bool printed;
    int lineCnt = 1;
    char text;
    while(1) {
        text = GetChar(file);
        if(text == -1) {
            break;
        }
        if(text == '\n') {
            if(lineCnt == line) {
                printf("%c",text);
                printed = true;
            }
            lineCnt++;
            continue;
        }
        if(lineCnt == line) {
            printf("%c",text);
            printed = true;
        }
    }
    if(!printed) {
        lineCnt--;
        printf("Wrong line number. Number of lines: %d\n", lineCnt);
    }
    Rewind(file);
    printf("\n");
}

/*          main          */

int main(int argc, char* argv[]) {
    int lowerBound = -1;
    int upperBound = -1;
    File* file = (File*)malloc(sizeof(File));
    bool fileOpened = false;
//l - printline       DONE
//s - search          DONE
//d - del             DONE
//i - insert
//bu - upper bound
//bl - lower bound
//f - file            DONE

    if(argc > 1) {
        for(int i = 1; i < argc;i++) {
            if(argv[i][0] == 'f' && argv[i][1] == ':') {
                char* path = (char*)malloc(sizeof(char) * 256);
                int j = 2;
                while(argv[i][j] != '\0') {
                    path[j - 2] = argv[i][j];
                    j++;
                }
                path[j - 2] = '\0';

                if(j == 2) {
                    printf("Incorrect filename\n");
                    exit(1);
                }

                int newDescriptor = open(path, O_RDONLY | O_CREAT);
                if(newDescriptor == -1) {
                    printf("Error while openning file\n");
                    free(path);
                    continue;
                }

                struct stat file_info;
                fstat(newDescriptor, &file_info);
                off_t fs = file_info.st_size;

                if(upperBound != -1 && (fs > upperBound)) {
                    printf("Denied, your file is too big\n");
                    free(path);
                    continue;
                }
                if(lowerBound != -1 && (fs < lowerBound)) {
                    printf("Denied, your file is too small\n");
                    free(path);
                    continue;
                }
                if(fileOpened) {
                    free(file->path);
                    close(file->descriptor);
                }
                file->path = path;
                file->descriptor = newDescriptor;
                fileOpened = true;
                file->fileSize = fs;
                file->pageSize = sysconf(_SC_PAGESIZE);
            }

            if(argv[i][0] == 'l' && argv[i][1] == ':') {
                if(!fileOpened) {
                    printf("File wasn't opened!\n");
                    continue;
                }
                int j = 2;
                int line = 0;
                while(argv[i][j] != '\0') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        line *= 10;
                        line += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                if(line == 0) {
                    printf("Wrong argument!\n");
                    continue;
                }
                PrintLine(file,line);
            }

            if(argv[i][0] == 's' && argv[i][1] == ':') {
                if(!fileOpened) {
                    printf("File wasn't opened!\n");
                    continue;
                }
                int len = 0;
                int j = 2;
                while(argv[i][j] != '/') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        len *= 10;
                        len += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                if(len == 0) {
                    printf("Wrong argument!\n");
                    exit(1);
                }
                char* pattern = (char*)malloc(len * sizeof(char));
                j++;
                for(int z = 0;z < len;z++) {
                    pattern[z] = argv[i][z + j];
                }
                KMP(file,pattern,len);
                free(pattern);
            }

            if(argv[i][0] == 'd' && argv[i][1] == ':') {
                if(!fileOpened) {
                    printf("File wasn't opened!\n");
                    continue;
                }
                int j = 2;
                int line = 0;
                int pos = 0;
                int num = 0;
                while(argv[i][j] != '/') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        line *= 10;
                        line += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                j++;
                while(argv[i][j] != '/') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        pos *= 10;
                        pos += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                j++;
                while(argv[i][j] != '\0') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        num *= 10;
                        num += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                if(lowerBound != -1 && ((file->fileSize - num) < lowerBound)) {
                    printf("Denied, filesize is to low\n");
                    continue;
                }
                Del(file,line,pos,num);

            }

            if(argv[i][0] == 'i' && argv[i][1] == ':') {
                if(!fileOpened) {
                    printf("File wasn't opened!\n");
                    continue;
                }
                int j = 2;
                int line = 0;
                int pos = 0;
                int num = 0;
                while(argv[i][j] != '/') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        line *= 10;
                        line += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                j++;
                while(argv[i][j] != '/') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        pos *= 10;
                        pos += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                j++;
                while(argv[i][j] != '/') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        num *= 10;
                        num += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }
                char* str = (char*)malloc(num * sizeof(char));
                j++;
                for(int z = 0; z < num;z++) {
                    str[z] = argv[i][j + z];
                }
                if(lowerBound != -1 && ((file->fileSize + num) > upperBound)) {
                    printf("Denied, filesize is to high\n");
                    free(str);
                    continue;
                }
                Insert(file,line,pos,str,num);
            }

            if(argv[i][0] == 'b' && argv[i][1] == 'u' && argv[i][2] == ':') {
                int ub = 0;
                int j = 3;
                while(argv[i][j] != '\0') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        ub *= 10;
                        ub += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }

                upperBound = ub;
            }

            if(argv[i][0] == 'b' && argv[i][1] == 'l' && argv[i][2] == ':') {
                int ub = 0;
                int j = 3;
                while(argv[i][j] != '\0') {
                    if(argv[i][j] >= '0'  && argv[i][j] <= '9') {
                        ub *= 10;
                        ub += (argv[i][j] - '0');
                        j++;
                    }
                    else {
                        printf("Wrong argument!\n");
                        exit(1);
                    }
                }

                lowerBound = ub;
            }
        }
    }

    if(!fileOpened) {
        printf("Enter filename:\n");
        file->path = (char*)malloc(256 * sizeof(char));
        scanf("%s", file->path);
        //printf("%s\n",file->path);
        Open(file);
    }

    while(1) {
        int cmd;
        menu();
        scanf("%d", &cmd);

        if(cmd == 1) {
            printf("Enter number of your line:\n");
            int line;
            scanf("%d", &line);
            PrintLine(file, line);

        }
        if(cmd == 2) {
            printf("Enter length of your pattern:\n");
            int len;
            scanf("%d", &len);
            char c;
            scanf("%c", &c);
            char* pattern = (char*)malloc(len * sizeof(char));
            printf("Enter your pattern:\n");
            for(int i = 0; i < len; ++i) {
                scanf("%c", &pattern[i]);
            }
            scanf("%c", &c);
            KMP(file, pattern, len);
        }
        if(cmd == 3) {
            printf("Enter line and position:\n");
            int line, pos;
            scanf("%d%d", &line, &pos);
            printf("How much symbols you want to delete?\n");
            int toSkip;
            scanf("%d",&toSkip);

            if(lowerBound != -1 && ((file->fileSize - toSkip) < lowerBound)) {
                printf("Denied, filesize is to low\n");
                continue;
            }
            Del(file,line,pos,toSkip);
        }
        if(cmd == 4) {
            printf("Enter line and position:\n");
            int line, pos;
            scanf("%d%d", &line, &pos);

            printf("How much symbols you want to insert?\n");
            int cnt;
            char tmp;
            scanf("%d%c",&cnt,&tmp);
            if(upperBound != -1 && (file->fileSize + cnt) > upperBound) {
                printf("Denied, file is too large\n");
                continue;
            }
            printf("Enter your symbols:\n");
            char* str = (char*)malloc(cnt * sizeof(char));
            for(int i = 0; i < cnt; ++i) {
                scanf("%c",&str[i]);
            }
            Insert(file,line,pos,str,cnt);

        }
        if(cmd == 5) {
            printf("1.     Set upper bound\n");
            printf("2.     Set lower bound\n");
            printf("3.     Delete upper bound\n");
            printf("4.     Delete lower bound\n");
            int cmd1;
            scanf("%d",&cmd1);
            if(cmd1 == 1) {
                printf("Enter upper bound:\n");
                scanf("%d",&upperBound);
                if(upperBound < file->fileSize) {
                    printf("Denied, your file is too big\n");
                    upperBound = -1;
                }
            }
            if(cmd1 == 2) {
                printf("Enter lower bound:\n");
                scanf("%d",&lowerBound);
                if(lowerBound > file->fileSize) {
                    printf("Denied,your file is too small\n");
                    lowerBound = -1;
                }
            }
            if(cmd1 == 3) {
                upperBound = -1;
            }
            if(cmd1 == 4) {
                lowerBound = -1;
            }
        }
        if(cmd == 6) {
            char* newFile = (char*)malloc(sizeof(char) * 256);
            printf("Enter file name:\n");
            scanf("%s",newFile);
            int newDescriptor = open(newFile, O_RDONLY | O_CREAT);
            if(newDescriptor == -1) {
                printf("Error while openning file\n");
                free(newFile);
                continue;
            }

            struct stat file_info;
            fstat(file->descriptor, &file_info);
            off_t fs = file_info.st_size;

            if(upperBound != -1 && (fs > upperBound)) {
                printf("Denied, your file is too big\n");
                free(newFile);
                continue;
            }
            if(lowerBound != - 1 && (fs < lowerBound)) {
                printf("Denied, your file is too small\n");
                free(newFile);
                continue;
            }

            free(file->path);
            file->path = newFile;
            close(file->descriptor);
            file->descriptor = newDescriptor;
        }
        if(cmd == 7) {
            printf("\n1.     Default\n");
            printf("2.     Your size\n");
            long cmd1;
            scanf("%ld", &cmd1);
            if(cmd1 == 1) {
                file->pageSize = sysconf(_SC_PAGESIZE);
            }
            if(cmd1 == 2) {
                printf("How many pages to map?:\n");
                scanf("%ld", &cmd1);
                file->pageSize = cmd1 * sysconf(_SC_PAGE_SIZE);
            }
        }
        if(cmd == -1) {
            Rewind(file);
            while(1) {
                char text;
                text = GetChar(file);
                if(text == -1) {
                    break;
                }
                printf("%c", text);
            }
            Rewind(file);
            printf("\n");
        }
        if(cmd == 8) {
            printf("Filesize: %d\n",(int)file->fileSize);
            int cnt = ScanFile(file);
            if(cnt != 0) {
                free(file->lines);
            }
            Rewind(file);
            if(cnt == 0) {
                cnt++;
            }
            printf("Number of lines: %d\n",cnt -1);
            printf("Upper bound: %d\n",upperBound);
            printf("Lower bound: %d\n",lowerBound);
        }
        if(cmd == 9) {
            free(file->path);
            close(file->descriptor);
            free(file);
            break;
        }
    }
}

//letitfork@gmail.com