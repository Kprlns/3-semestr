#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
// 0 читать
// 1 писать

int main(int argc,char* argv[]) {

    int pipeToChild[2];
    pipe(pipeToChild);

    int pipeToParent[2];
    pipe(pipeToParent);

    pid_t child = fork();
    if(child == 0) {

        close(pipeToParent[0]);
        close(pipeToChild[1]);

        //FILE* file;
        //read(pipeToChild[0],&file,sizeof(FILE*));
        char c;
        

        //fread(&c,sizeof(char),1,file);

        write(pipeToParent[1],&c,sizeof(char));
        close(pipeToParent[1]);
    }
    else {
        close(pipeToChild[0]);
        close(pipeToParent[1]);
        
        int pathlen = strlen(argv[1]);
        write(pipeToChild[1],&pathlen,sizeof(int));
        for(int i = 0; i < pathlen; i++) {
            char c = argv[1][i];
            write(pipeToChild[1],&c,sizeof(char));
        }
        //write(pipeToChild[1],&f,sizeof(FILE*));

        char b;
        read(pipeToParent[0],&b,sizeof(char));
        std::cout <<b;
        close(pipeToParent[0]);
        close(pipeToParent[1]);
    }
}
