#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
// 0 читать
// 1 писать
int main(int argc,char* argv[]) {

    int pipe1[2];
    pipe(pipe1);

    int pipe2[2];
    pipe(pipe2);
    pid_t child = fork();

    if(child == 0) {


        close(1);
        dup(pipe1[1]);

        close(0);
        dup(pipe2[0]);
        char* str = strdup(argv[1]);
        execl("./t1",argv[1],(char*)0);
    }
    else {
        int c;
        read(pipe1[0],&c,sizeof(int));
        std::cout << c <<'\n';
        char a;

        FILE* f = fopen(argv[1],"rt");
        write(pipe2[1],f,sizeof(FILE*));
        read(pipe1[0],&a,sizeof(char));
        std::cout << a <<'\n';

    }

}