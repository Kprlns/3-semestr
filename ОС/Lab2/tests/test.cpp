#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
// 0 читать
// 1 писать

int main(int argc,char* argv[]) {

    int pipeToChild[2];
    pipe(pipeToChild);

    int pipeToParent[2];
    pipe(pipeToParent);

    pid_t child = fork();
    if(child == 0) {

        close(pipeToParent[0]);
        close(pipeToChild[1]);

        int cnt;
        char c;

        read(pipeToChild[0],&cnt,sizeof(int));
        char* str = (char*)malloc((cnt)*sizeof(char));
        read(pipeToChild[0],str,cnt*sizeof(char));
        str[cnt] = '\0';
        //printf("%s", str);
        //std::cout << "\n" << cnt << "\n";

        FILE* f = fopen(str,"rb");
        fread(&c,sizeof(char),1,f);
        //std::cout << c;

        //std::cout << c <<"\n";
        write(pipeToParent[1],&c,sizeof(char));
        //close(pipeToParent[1]);
    }
    else {
        close(pipeToChild[0]);
        close(pipeToParent[1]);


        int cnt = strlen(argv[1]);
        write(pipeToChild[1],&cnt,sizeof(int));
        write(pipeToChild[1],argv[1],sizeof(char)*cnt);

        char b;
        read(pipeToParent[0],&b,sizeof(char));
        std::cout << b;
        close(pipeToParent[0]);
        close(pipeToParent[1]);
    }
}