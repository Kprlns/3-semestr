#include <iostream>
#include <unistd.h>

int main() {

    pid_t child1 = fork();
    if(child1 == 0) {
        pid_t a= getpid();
        std::cout << a << " " << "kek\n";
    }
    else {
        pid_t child2 = fork();
        if(child2 == 0) {
            pid_t b = getpid();
            std::cout << b << " " << "lol\n";
        }
        else {
            pid_t c = getpid();
            std::cout << c << " " << "mda\n";
        }
    }
}