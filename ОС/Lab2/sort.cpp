#include <stdio.h>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

const int START_ARR_SIZE = 100;
const int STR_LEN = 1024;

int main(int argc, char* argv[]) {

    int a;
    read(0,&a,sizeof(int));
    char* path = (char*)malloc(sizeof(char) * a);
    read(0,path,sizeof(char) * a);
    path[a] = '\0';
//    printf("%s",path);
    FILE* file = fopen(path,"rt");
    char** arr = (char**)malloc(START_ARR_SIZE * sizeof(char*));
    int arr_cnt = 0;
    int arr_max = START_ARR_SIZE;

    for (arr_cnt; ;arr_cnt++) {
        char* str = (char*)malloc(STR_LEN * sizeof(char));
        if(!fgets(str,STR_LEN,file)){
            free(str);
            break;
        }
        else {
            arr[arr_cnt] = str;
            if(arr_cnt == (arr_max - 1)) {
                arr_max *= 2;
                str = (char*)realloc(arr,arr_max * sizeof(char*));
            }
        }
    }
    fclose(file);

    for(int i = 1; i < arr_cnt; i++) {
            char* swap = arr[i];

            for(int j = i; j > 0; j--) {
                if(strcmp(arr[j],arr[j - 1]) < 0) {
                    swap = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = swap;
                }
                else {
                    break;
                }
            }
        //}
    }

    write(1,&arr_cnt,sizeof(int));
    for(int i = 0; i < arr_cnt; i++) {
//        printf("%s",arr[i]);
        int strl = strlen(arr[i]);
        write(1,&strl,sizeof(int));
        write(1,arr[i],sizeof(char) * strl);


    }

    for(int i = 0; i < arr_cnt; i++) {
        free(arr[i]);
    }
    free(arr);

}