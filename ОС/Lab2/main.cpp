#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>

const int STR_LEN = 1024;

int main(int argc, char* argv[]) {
    pid_t child1;
    pid_t child2;
    pid_t current = getpid();


    int pipeToParent1[2];
    int pipeToParent2[2];
    pipe(pipeToParent1);
    pipe(pipeToParent2);

    int pipeToChild1[2];
    int pipeToChild2[2];
    pipe(pipeToChild1);
    pipe(pipeToChild2);

    child1 = fork();

    if(child1 == 0){
        close(0);
        close(1);
        dup(pipeToChild1[0]);
        dup(pipeToParent1[1]);

        execl("./s",argv[1],(char*)0);
        //close(pipeToParent1[1]);
    }
    else {

        pid_t child2 = fork();
        if(child2 == 0) {
            close(0);
            close(1);

            dup(pipeToChild2[0]);
            dup(pipeToParent2[1]);

            execl("./s",argv[2],(char*)0);
        }
        else {
            //первый процесс
            close(pipeToParent1[1]);
            close(pipeToChild1[0]);
            int cnt = strlen(argv[1]);
            write(pipeToChild1[1], &cnt, sizeof(int));
            write(pipeToChild1[1], argv[1], sizeof(char) * cnt);

            //второй процесс
            close(pipeToParent2[1]);
            close(pipeToChild2[0]);
            cnt = strlen(argv[2]);
            write(pipeToChild2[1], &cnt, sizeof(int));
            write(pipeToChild2[1], argv[2], sizeof(char) * cnt);


            cnt = 0;
            int cnt1,cnt2;
            read(pipeToParent1[0], &cnt1, sizeof(int));
            read(pipeToParent2[0], &cnt2, sizeof(int));

            int strl;
            char *str = (char *) malloc(1024 * sizeof(char));

            while(cnt1 || cnt2) {
                if(cnt1 > 0) {
                    read(pipeToParent1[0], &strl, sizeof(int));
                    read(pipeToParent1[0], str, sizeof(char) * strl);
                    str[strl] = '\0';
                    printf("%s", str);
                    cnt1--;
                }
                if(cnt2 > 0) {
                    read(pipeToParent2[0], &strl, sizeof(int));
                    read(pipeToParent2[0], str, sizeof(char) * strl);
                    str[strl] = '\0';
                    printf("%s", str);
                    cnt2--;
                }
            }
            free(str);
        }
    }
}

/*

 for (int i = 0; i < cnt; i++) {
                int strl;
                read(pipeToParent1[0], &strl, sizeof(int));
                read(pipeToParent1[0], str, sizeof(char) * strl);
                str[strl] = '\0';
                printf("%s", str);
            }


 */
/*
while (1) {
            if (read(pipeToParent1[0], &cnt, sizeof(int)) > 0) {
                std::cout <<"kek\n";
                for (int i = 0; i < cnt; i++) {
                    read(pipeToParent1[0], &str[i], sizeof(char));
                }
                str[cnt] = '\0';
                printf("%s", str);
            }
            else {
                break;
            }
        }
 */