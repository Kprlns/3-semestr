color(red).
color(white).
color(blue).

leng([],0). % длина списка
leng([_|Y],N):-
    leng(Y,M),
    N is M+1.

leng_cmp(X,Y):-
	leng(X,A),
	leng(Y,B),
	A =:= B