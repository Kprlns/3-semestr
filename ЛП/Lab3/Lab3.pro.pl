leng([],0). % длина списка
leng([_|Y],N):-
    leng(Y,M),
    N is M+1.

memb(X,[X|_]). % проверка на принадлежность
memb(X,[_|Y]):-
    memb(X,Y).

app([],X,X). % слияние списков
app([X|Y],Z,[X|T]):-
    app(Y,Z,T).

comparator(X,Y,Z):- %  предикат сравнения чисел
	Y @> X,
	Z @> Y.

color(red).
color(white).
color(blue).

getNthElem([H|_],H,0):-!. % получение элемента списка
getNthElem([H|T],X,N):-
	N1 is N - 1,
	getNthElem(T,X,N1).

swap_elems(List,M,N,ResList) :- % поменять местами 2 элемента списка

	length(List,Len1),
	length(ResList,Len2),
	Len1 =:= Len2,

  append(MHead,[Mth|MTail],List),
  append(MHead,[Nth|MTail],Tmp),

	append(NHead,[Nth|NTail],Tmp),
  append(NHead,[Mth|NTail],ResList),

	length(MHead,M),
  length(NHead,N),!.

check_correct(List,N,M):-
	getNthElem(List,X,N),
	getNthElem(List,Y,M),

	( (X == blue, Y == red);
		(X == blue, Y == white);
	(X == white, Y == red)),
	!.


move([H|T],Res):- % переход между состояниями
	move(H,Res).

move(struct(List),struct(ResList)):-
	length(List,Len),
	Len1 is Len - 1,

	%comparator(0,A,Len1),
	%comparator(0,B,Len1),
	between(0,Len1,A),
	between(0,Len1,B),

	A @< B,
	check_correct(List,A,B),
	swap_elems(List,A,B,X),

	ResList = X.

prolong([X|T],[Y,X|T]):- % продление пути без зацикливания
	move(X,Y),
	\+ member(Y,[X|T]).

/*          Поиск в глубину          */
search_dpth(A,B,P):-
	dpth([A],B,L),reverse(L,P).

dpth([X|T],X,[X|T]).
dpth(P,F,L):-
	prolong(P,P1),
	dpth(P1,F,L).

/*          Поиск в ширину          */
search_bdth(X,Y,P):-
	bdth([[X]],Y,L),
	reverse(L,P).

bdth([[X|T]|_],X,[X|T]).
bdth([P|QI],X,R):-
	findall(Z,prolong(P,Z),T),
	append(QI,T,Q0),
	bdth(Q0,X,R).

bdth([_|T],Y,L):-
	bdth(T,Y,L).

/*          Поиск с итерационным заглублением          */
integer1(1).
integer1(X):-
	integer1(Y),
	X is Y + 1.

search_id(Start,Finish,Path,DepthLimit):-
	depth_id([Start],Finish,Res,DepthLimit),
	reverse(Res,Path).

depth_id([Finish|T],Finish,[Finish|T],0).
depth_id(Path,Finish,R,N):-
	N @> 0,
	prolong(Path,NewPath),
	N1 is N - 1,
	depth_id(NewPath,Finish,R,N1).

search_id(Start,Finish,Path):-
	integer1(Level),
	search_id(Start,Finish,Path,Level).
