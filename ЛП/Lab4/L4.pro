parent(alexei,tolia).
parent(alexei,volodia).
parent(tolia,tima).

% примеры запроса:
% answer([who,is,tima,father,?],X).
% X = tolia
% answer([is,volodia,tolia,brother,?],X).
% X = true
% проверка на принадлежность
who_check(X):-
  member(X,[who]).

is_check(X):-
  member(X,[is]).

name_check(X):-
  member(X,[alexei,tolia,volodia,tima]).

relation_check(X):-
  member(X,[grandfather,dad,uncle,brother,son,grandson,nephew]).

sign_check(X):-
  member(X,['?']).

/*          Отношения родства          */
relationship(X,Y,grandfather):-
  parent(X,Z),
  parent(Z,Y),
  !.

relationship(X,Y,dad):-
  parent(X,Y).

relationship(X,Y,uncle):-
 parent(A,X),
 parent(B,Y),
 relationship(X,B,brother),
 !.

relationship(X,Y,brother):-
  parent(A,X),
  parent(A,Y),
  X \= Y,
  !.

relationship(X,Y,son):-
  parent(Y,X).

relationship(X,Y,grandson):-
  parent(Z,X),
  parent(Y,Z).

relationship(X,Y,nephew):-
  relationship(Y,X,uncle).

/*          Обработка входных сообщений          */
  % who is tima father ? 1ый тип
  % is tolya tima father ? 2ой тип

question(L,list(A,B,C,D)):-
  who_check(A1),
  is_check(B1),
  name_check(C1),
  relation_check(D1),
  sign_check(Sign),

  L == [A1,B1,C1,D1,Sign],

  A = who(A1),
  B = is(B1),
  C = name_who(C1),
  D = relation(D1).

question(L,list(A,B,C,D)):-
  is_check(A1),
  name_check(B1),
  name_check(C1),
  relation_check(D1),
  sign_check(Sign),

  L = [A1,B1,C1,D1,Sign],

  A = is(A1),
  B = name_who(B1),
  C = name_whom(C1),
  D = relation(D1).

ask(list(is(Q),name_who(X),name_whom(Y),relation(R)),Result):-
  relationship(X,Y,R),
  !,
  Result = true.

ask(list(who(Q1),is(Q2),name_who(X),relation(R)),Result):-
  relationship(Result,X,R).

answer(L,R):-
  question(L,X),
  ask(X,R).
