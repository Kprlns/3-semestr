parent(alexei,tolia).
parent(alexei,volodia).
parent(tolia,tima).

case(alexei,alexeia).
case(tolia,toli).
case(volodia,volodi).
case(tima,timi).


% проверка на соответствие
who_check(X):-
  member(X,[kto]).

whom_check(X):-
  member(X,[komu,chei]).

name_check(X):-
  member(X,[alexei,tolia,volodia,tima]).

name_check1(X):-
  member(X,[alexeia,toli,volodi,timi]).

relation_check(X):- %список определенных отношений отношений
  member(X,[ded,otec,diadia,brat,sin,vnuk,plemiannik]).

sign_check(X):-
  member(X,['?']).

/*          Отношения родства          */
relationship(X,Y,ded):-
  parent(X,Z),
  parent(Z,Y),
  !.

relationship(X,Y,otec):-
  parent(X,Y).

relationship(X,Y,diadia):-
 parent(A,X),
 parent(B,Y),
 relationship(X,B,brat),
 !.

relationship(X,Y,brat):-
  parent(A,X),
  parent(A,Y),
  X \= Y,
  !.

relationship(X,Y,sin):-
  parent(Y,X).

relationship(X,Y,vnuk):-
  parent(Z,X),
  parent(Y,Z).

relationship(X,Y,plemiannik):-
  relationship(Y,X,diadia),
  !.

/*          Обработка входных сообщений          */

  % kto brat toli ? тип 1
  % komu/chei brat tolia ? тип 2
  % tolia brat volodi ? тип 3

question(L,list(X,Y,Z)):- % тип 1
  who_check(X1),
  relation_check(Y1),
  name_check1(Z1),
  sign_check(Sign),

  L = [X1,Y1,Z1,Sign],

  X = who(X1),
  Y = relation(Y1),
  Z = name_whom(Z1),
  !.

question(L,list(X,Y,Z)):- % тип 2
  whom_check(X1),
  relation_check(Y1),
  name_check(Z1),
  sign_check(Sign),

  L = [X1,Y1,Z1,Sign],

  X = whom(X1),
  Y = relation(Y1),
  Z = name_who(Z1),
  !.

question(L,list(X,Y,Z)):- % тип 3
  name_check(X1),
  relation_check(Y1),
  name_check1(Z1),
  sign_check(Sign),

  L = [X1,Y1,Z1,Sign],

  X = name_who(X1),
  Y = relation(Y1),
  Z = name_whom(Z1),
  !.

ask(list(who(_),relation(Y),name_whom(Z1)),Res):- % тип 1
  case(Z,Z1),
  relationship(Res,Z,Y).

ask(list(whom(_),relation(Y),name_who(Z)),Res):- % тип 2
  relationship(Res,Z,Y).

ask(list(name_who(X),relation(Y),name_whom(Z1)),Res):- % тип 3
  case(Z,Z1),
  relationship(X,Z,Y),
  !,
  Res = true.

answer(L,Res):- % запрос
  question(L,X),
  ask(X,Res).
