parent(alexei,tolia).
parent(alexei,volodia).
parent(tolia,tima).

% примеры запроса:
% ask([who,is,tima,father,?],X).
% X = tolia
% ask([is,volodia,tolia,brother,?],X).

% проверка на принадлежность
who_check(X):-
  member(X,[who]).

is_check(X):-
  member(X,[is]).

name_check(X):-
  member(X,[alexei,tolia,volodia,tima]).


relation_check(X):-
  member(X,[grandfather,dad,uncle,brother,son,grandson,nephew]).

sign_check(X):-
  member(X,['?']).

/*          Отношения родства          */
relationship(X,Y,grandfather):-
  parent(X,Z),
  parent(Z,Y),
  !.

relationship(X,Y,dad):-
  parent(X,Y).

relationship(X,Y,uncle):-
 parent(A,X),
 parent(B,Y),
 relationship(X,B,brother),
 !.

relationship(X,Y,brother):-
  parent(A,X),
  parent(A,Y),
  X \= Y,
  !.

relationship(X,Y,son):-
  parent(Y,X).

relationship(X,Y,grandson):-
  parent(Z,X),
  parent(Y,Z).

relationship(X,Y,nephew):-
  relationship(Y,X,uncle).

/*          Обработка входных сообщений          */
  % is tolya tima father ? 1ый тип
  % who is tima father ? 2ой тип
getQuestion(L,question(X)):- % 1 тип
  is_check(Y),
  L == [Y],
  X = is(Y),
  !.
getQuestion(L,question(X,Y)):- % 2 тип

  who_check(X1),
  is_check(Y1),

  L == [X1,Y1],
  X = who(X1),
  Y = is(Y1),
  !.

getTask(L,task(X,Y,Z)):- % 1 тип

  name_check(X1),
  name_check(Y1),
  relation_check(Z1),
  sign_check(Sign),

  L == [X1,Y1,Z1,Sign],
  X = name_who(X1),
  Y = name_whom(Y1),
  Z = relation(Z1),
  !.

getTask(L,task(X,Z)):- % 2 тип

  name_check(X1),
  relation_check(Z1),
  sign_check(Sign),

  L == [X1,Z1,Sign],
  X = name_who(X1),
  Z = relation(Z1),
  !.

quest(L,list(A,B,C,D)):-
  who_check(A1),
  is_check(B1),
  name_check(C1),
  relation_check(D1),
  sign_check(Sign),

  L == [A1,B1,C1,D1,Sign],

  A = who(A1),
  B = is(B1),
  C = name_who(C1),
  D = relation(D1).

quest(L,list(A,B,C,D)):-
  is_check(A1),
  name_check(B1),
  name_check(C1),
  relation_check(D1),
  sign_check(Sign),

  L = [A1,B1,C1,D1,Sign],

  A = is(A1),
  B = name_who(B1),
  C = name_whom(C1),
  D = relation(D1).

check(list(question(is(Q)),task(name_who(X),name_whom(Y),relation(R))),Result):- % предикат проверки для вопросов 1 типа
  relationship(X,Y,R),
  !,
  Result = true.

check(list(question(who(Q1),is(Q2)),task(name_who(X),relation(R))),Result):- % предикат для проверки вопросов 2 типа
  relationship(Result,X,R).

ask(list(is(Q),name_who(X),name_whom(Y),relation(R)),Result):-
  relationship(X,Y,R),
  !,
  Result = true.

ask(list(who(Q1),is(Q2),name_who(X),relation(R)),Result):-
  relationship(Result,X,R).

ans(L,R):-
  quest(L,X),
  ask(X,R).


answer(L,Result):- % предикат, получающий запрос
  append(A,B,L),
  getQuestion(A,X),
  getTask(B,Y),
  check(list(X,Y),Result).
