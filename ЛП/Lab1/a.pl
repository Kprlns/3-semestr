leng([],0). % длина списка
leng([_|Y],N):-
    leng(Y,M),
    N is M+1.

memb(X,[X|_]). % проверка на принадлежность
memb(X,[_|Y]):-
memb(X,Y).

app([],X,X). % слияние списков
app([X|Y],Z,[X|T]):-
    app(Y,Z,T).

subl(Sub,List):- % проверка вхождения
    app(_,Y2,List), %списка в качестве подсписка
    app(Sub,_,Y2).

rem(X, [X|T], T). % удаление из списка
rem(X, [Y|T], [Y|T1]):-
    rem(X,T,T1).

perm([],[]):-!. % перестановка списка
perm(L,[X|T]):-
    rem(X,L,R),
    perm(R,T).

perm1([],[]):-!. % перестановка списка
perm1(L,[X|T]):-
    rem(X,L,R),
    perm1(R,T), !.

/*YpastX(X,Y,[X|T]):-
    memb(Y,T).
YpastX(X,Y,[H|T]):-
    YpastX(X,Y,T).
*/
surname(borisov).
surname(kirillov).
surname(danin).
surname(savin).

prof(mechanic).
prof(chemist).
prof(builder).
prof(radioman).


chesswin(p(borisov,_), p(danin,_)).%Борисов выигрывает у Данина
chesswin(p(savin,_), p(borisov,_)).%Данин выигрывает у Борисова
chesswin(p(_,mechanic), p(_,builder)).

theater(p(_,chemist), p(_,mechanic)).
theater(p(_,builder), p(_,chemist)).

skiwin(p(_,builder), p(_,radioman)).

profnum(P,[p(_,P)|_],1):-!.
profnum(X,[_|T],N):-
    profnum(X,T,M),
    N is M+1 .

surnum(P,[p(P,_)|_],1):-!.
surnum(X,[_|T],N):-
    surnum(X,T,M),
    N is M+1 .

pnum(p(X,Y),[p(X,Y)|_],1):-!.
pnum(p(X,Y),[_|T],N):-
    pnum(p(X,Y),T,M),
    N is M+1.

last([_,_,_,D],D).
third([_,_,C,_],C).
second([_,B,_,_],B).
first([A,_,_,_],A).

xy(X,Y,[X,Y|_]).
xy(X,Y,[_|T]):-
    xy(X,Y,T).
