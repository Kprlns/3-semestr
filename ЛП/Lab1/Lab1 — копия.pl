leng([],0). % длина списка
leng([_|Y],N):-
    leng(Y,M),
    N is M+1.

memb(X,[X|_]). % проверка на принадлежность
memb(X,[_|Y]):-
    memb(X,Y).

app([],X,X). % слияние списков
app([X|Y],Z,[X|T]):-
    app(Y,Z,T).

subl(Sub,List):- % проверка вхождения
    app(_,Y2,List), %списка в качестве подсписка
    app(Sub,_,Y2).

deln([_|Y],1,Y):-!. % удаление элемента, стоящего
  deln([X|Y],N,[X|L]):- % на n-ой позиции
   N1 is N-1,!,
   deln(Y,N1,L).

numltol([],[]). % перевод нумерованного списка в обычный
numltol([e(A,B)|Y],[A|Y1]):-
    numltol(Y,Y1).

numsubl(Sub,List):- %  поиск подсписка в нумерованном списке
    numltol(List,List1),
    subl(Sub,List1).

nummemb(A,[e(A,_)|_]). % проверка на принадлежность
nummemb(A,[_|Y]):-
    nummemb(A,Y).

numapp(X,[],X). % слияние нумерованных списков
numapp(X,[e(C,D)|Y],M):-
    leng(X,N),
    N1 is N + 1,
    app(X,[e(C,N1)],L),
    numapp(L,Y,M).

rem(X, [X|T], T). % удаление из списка
rem(X, [Y|T], [Y|T1]):-
    rem(X,T,T1).

decn([e(A,B)],[e(A,C)]):- % декремент номеров
    C is B - 1 . % нумерованного списка
decn([e(A,B)|Z],[e(A,C)|Z1]):-
    C is B - 1,
    decn(Z,Z1).

numrem(A,[e(A,N)|T],L):- % удаление
    decn(T,L). % из нумерованного списка
numrem(A,[e(A,_)|[]], []).
numrem(X, [Y|T], [Y|T1]):-
    numrem(X,T,T1).

permute([],[]). % перестановка списка
permute(L,[X|T]):-
    rem(X,L,R),
    permute(R,T).

numpermute([],[]). % -||- нумерованного списка
numpermute(L,X):-
    permute(L,M),
    numapp([],M,X).

sum([],0). % сумма элементов списка
sum([X|Y],N):-
    sum(Y,M),
    N is M+X.

numsum([],0). % -||- нумерованного списка
numsum([e(A,B)|Y],N):-
    numsum(Y,M),
    N is M+A.

cutlist(_,0,[]). % усенение списка до указанной длины без
cutlist([X|Y],K,[X|Y1]):- % использования стандартных предикатов
    K1 is K-1,
    cutlist(Y,K1,Y1).

cutlist1(X,N,X):-
    leng(X,L),
    N > L.
cutlist1(Z, N, X):- % -||-
    length(X, N), % c использованием стандартных предикатов
    append(X, _, Z).
% оба предиката по усечению списков работают как
% с обычными списками, так и с нумерованными.

average(X,Z):- % вычисление среднего всех
    sum(X,Sum), % элементов списка
    leng(X,L),
    L > 0,
    Z is Sum / L.

numaverage(X,Z):- % -||- нумерованного списка
    numsum(X,Sum),
    leng(X,L),
    L > 0,
    Z is Sum / L.
