leng([],0). % длина списка
leng([_|Y],N):-
    leng(Y,M),
    N is M+1.

memb(X,[X|_]). % проверка на принадлежность
memb(X,[_|Y]):-
memb(X,Y).

app([],X,X). % слияние списков
app([X|Y],Z,[X|T]):-
    app(Y,Z,T).

subl(Sub,List):- % проверка вхождения
    app(_,Y2,List), %списка в качестве подсписка
    app(Sub,_,Y2).

rem(X, [X|T], T). % удаление из списка
rem(X, [Y|T], [Y|T1]):-
    rem(X,T,T1).

perm([],[]):-!. % перестановка списка
perm(L,[X|T]):-
    rem(X,L,R),
    perm(R,T).

/*          task          */

surname(borisov).
surname(kirillov).
surname(danin).
surname(savin).

prof(mechanic).
prof(chemist).
prof(builder).
prof(radioman).

chess(p(borisov,_),p(danin,_)).
chess(p(savin,_),p(borisov,_)).
chess(p(savin,_),p(danin,_)).

chess(p(_,mechanic),p(_,builder)).

theater(p(_,chemist),p(_,mechanic)).
theater(p(_,builder),p(_,chemist)).
theater(p(_,builder),p(_,mechanic)).
ski(p(_,radioman),p(_,builder)).

last([_,_,_,D],D).
third([_,_,C,_],C).
second([_,B,_,_],B).
first([A,_,_,_],A).

xy(X,Y,[X|T]):-
    memb(Y,T).
xy(X,Y,[A|T]):-
    xy(X,Y,T).

solve(Res):-
    perm([borisov, kirillov, danin, savin], [X1,X2,X3,X4]),
    perm([mechanic, chemist, builder, radioman],[Y1,Y2,Y3,Y4]),

    Res = [p(X1,Y1),p(X2,Y2),p(X3,Y3),p(X4,Y4)],

    last(Res,Oldest),
    \+theater(_,Oldest),
    \+chess(_,Oldest),

    \+last(Res,p(A,chemist)),
    \+last(Res,p(borisov,B)),

    \+first(Res,p(borisov,B)).
    \+first(Res,p(A,chemist)),

    first(Res,Youngest),
    \+ski(_,Youngest),

    xy(p(C,kirillov),D,Res),
    theater(p(borisov,B),D).




/*
    \+first(Age,p(borisov,_)),
    \+first(Age,p(_,chemist)),

    \+last(Age,p(_,chemist)),
    \+last(Age,p(kirillov,_)),
    \+last(Age,p(borisov,_)),
    xy(A,p(borisov,_),Age),
    ski(p(borisov,_),A),

    xy(p(kirillov,_),B,Age),
    theater(p(borisov,_),B),

    last(Age, Oldest),
    \+chess(_,Oldest),
    \+theater(_,Oldest),

    first(Age, Youngest),
    \+ski(_,Youngest).
*/
