leng([],0). % длина списка
leng([_|Y],N):-
    leng(Y,M),
    N is M+1.

memb(X,[X|_]). % проверка на принадлежность
memb(X,[_|Y]):-
memb(X,Y).

app([],X,X). % слияние списков
app([X|Y],Z,[X|T]):-
    app(Y,Z,T).

subl(Sub,List):- % проверка вхождения
    app(_,Y2,List), %списка в качестве подсписка
    app(Sub,_,Y2).

rem(X, [X|T], T). % удаление из списка
rem(X, [Y|T], [Y|T1]):-
    rem(X,T,T1).

perm([],[]):-!. % перестановка списка
perm(L,[X|T]):-
    rem(X,L,R),
    perm(R,T).

perm1([],[]):-!. % перестановка списка
perm1(L,[X|T]):-
    rem(X,L,R),
    perm1(R,T), !.


surname(borisov).
surname(kirillov).
surname(danin).
surname(savin).

prof(mechanic).
prof(chemist).
prof(builder).
prof(radioman).

profnum(P,[p(_,P)|_],1):-!.
profnum(X,[_|T],N):-
    profnum(X,T,M),
    N is M+1 .

surnum(P,[p(P,_)|_],1):-!.
surnum(X,[_|T],N):-
    surnum(X,T,M),
    N is M+1 .

pnum(p(X,Y),[p(X,Y)|_],1):-!.
pnum(p(X,Y),[_|T],N):-
    pnum(p(X,Y),T,M),
    N is M+1.

last1([_,_,_,D],D).
third([_,_,C,_],C).
second([_,B,_,_],B).
first([A,_,_,_],A).

xy(X,Y,[X|T]):-
    memb(Y,T).
xy(X,Y,[A|T]):-
    xy(X,Y,T).

solve(Res):-
    perm([borisov, kirillov, danin, savin], [X1,X2,X3,X4]),
    perm([mechanic, chemist, builder, radioman],[Y1,Y2,Y3,Y4]),

    Res = [p(X1,Y1),p(X2,Y2),p(X3,Y3),p(X4,Y4)],
    perm(Res,Age),

    \+first(Age,p(borisov,_)),
    \+last1(Age,p(borisov,_)),

    \+last1(Age,p(kirillov,_)),

    \+first(Age,p(_,chemist)),
    \+last1(Age,p(_,chemist)),

    perm(Res,Chess),

    last1(Age,Oldest),
    last1(Chess,Oldest),
    xy(p(danin,_),p(borisov,_),Chess),

    xy(p(borisov,_),p(savin,_),Chess),

    xy(p(_,builder),p(_,mechanic),Chess),

    perm(Res,Theater),

    last1(Theater,Oldest),
    xy(p(_,mechanic),p(_,chemist),Theater),
    xy(p(_,chemist),p(_,builder),Theater),

    xy(p(kirillov,_),A,Age),
    xy(A,p(borisov,_),Theater),

    perm(Res,Ski),

    first(Age,Youngest),
    last1(Ski,Youngest),
    xy(p(_,builder),p(_,radioman),Ski).

    xy(F,p(_,borisov),Age),
    xy(F,p(_,borisov),Ski),!.











%
