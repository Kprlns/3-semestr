leng([],0). % длина списка
leng([_|Y],N):-
    leng(Y,M),
    N is M+1.

memb(X,[X|_]). % проверка на принадлежность
memb(X,[_|Y]):-
memb(X,Y).

app([],X,X). % слияние списков
app([X|Y],Z,[X|T]):-
    app(Y,Z,T).

subl(Sub,List):- % проверка вхождения
    app(_,Y2,List), %списка в качестве подсписка
    app(Sub,_,Y2).

rem(X, [X|T], T). % удаление из списка
rem(X, [Y|T], [Y|T1]):-
    rem(X,T,T1).

perm([],[]):-!. % перестановка списка
perm(L,[X|T]):-
    rem(X,L,R),
    perm(R,T).

perm1([],[]):-!. % перестановка списка
perm1(L,[X|T]):-
    rem(X,L,R),
    perm1(R,T), !.

/*YpastX(X,Y,[X|T]):-
    memb(Y,T).
YpastX(X,Y,[H|T]):-
    YpastX(X,Y,T).
*/
surname(borisov).
surname(kirillov).
surname(danin).
surname(savin).

prof(mechanic).
prof(chemist).
prof(builder).
prof(radioman).

/*
chesswin(p(borisov,_), p(danin,_)).%Борисов выигрывает у Данина
chesswin(p(savin,_), p(borisov,_)).%Данин выигрывает у Борисова
chesswin(p(savin,_),p(danin,_)).
chesswin(p(_,mechanic), p(_,builder)).

theater(p(_,chemist), p(_,mechanic)).
theater(p(_,builder), p(_,chemist)).
theater(p(_,builder),p(_,mechanic)).

skiwin(p(_,builder), p(_,radioman)).
*/
profnum(P,[p(_,P)|_],1):-!.
profnum(X,[_|T],N):-
    profnum(X,T,M),
    N is M+1 .

surnum(P,[p(P,_)|_],1):-!.
surnum(X,[_|T],N):-
    surnum(X,T,M),
    N is M+1 .

pnum(p(X,Y),[p(X,Y)|_],1):-!.
pnum(p(X,Y),[_|T],N):-
    pnum(p(X,Y),T,M),
    N is M+1.

last([_,_,_,D],D).
third([_,_,C,_],C).
second([_,B,_,_],B).
first([A,_,_,_],A).

xy(X,Y,[X|T]):-
    memb(Y,T).
xy(X,Y,[A|T]):-
    xy(X,Y,T).

solve(Res):- %Борисов, Кириллов, Данин, Санин
    perm([borisov, kirillov, danin, savin], [X1,X2,X3,X4]),
    perm([mechanic, chemist, builder, radioman],[Y1,Y2,Y3,Y4]),

    Res = [p(X1,Y1),p(X2,Y2),p(X3,Y3),p(X4,Y4)],
    perm(Res,Age),
    perm(Res,Chess),
    perm(Res,Ski),
    perm(Res,Theater),

    %borisov
    \+first(Age,p(borisov,_)),
    \+last(Age,p(kirillov,_)),
    xy(p(danin,_), p(borisov,_), Chess),
    xy(p(borisov,_), p(savin,_), Chess),

    xy(A, p(borisov,_), Age),
    xy(A, p(borisov,_), Ski),

    xy(p(kirillov,_), B, Age),
    xy(B,p(borisov,_), Theater),

    %chemist
    xy(p(_,mechanic),p(_,chemist),Theater),
    xy(p(_,chemist),p(_,builder),Theater),

    \+first(Age,p(_,chemist)),
    \+last(Age,p(_,chemist)),

    %builder
    xy(p(_,builder), p(_,radioman),Ski),
    %xy(p(_,builder), p(_,mechanic),Chess),

    %The Oldest
    last(Age,Oldest),

    last(Chess,Oldest),
    last(Theater,Oldest),

    %The Youngest
    first(Age,Youngest),
    first(Ski,Youngest).
/*    \+first(Age,p(_,chemist)),
    \+last(Age,p(_,chemist)),
    \+first(Age,p(borisov,_)),
    \+last(Age,p(kirillov,_)),



    last(Age,Oldest),
    pnum(Oldest,Chess,4),

    xy(p(borisov,_), p(savin,_), Chess),
    xy(p(danin,_), p(borisov,_), Chess).
    xy(p(_,builder),p(_,mechanic),Chess).


    first(Age,Skiwinner),
    last(Ski,Skiwinner),
    \+fisrt(Ski,p(borisov,_)),
    xy(M,p(borisov,_),Ski),
    xy(M,p(borisov,_),Age),
    xy(p(_,builder),p(_,radioman),Ski),


    last(Theater,Oldest),

    xy(p(kirillov,_),S,Age),
    xy(S,p(borisov,_),Theater),
    xy(p(_,mechanic),p(_,chemist),Theater),
    xy(p(_,chemist),p(_,builder),Theater).
*/
%
/*
\+profnum(chemist,Age,1),
\+profnum(chemist,Age,4),
\+surnum(borisov,Age,1),
\+surnum(borisov,Age,4),



 last(Age,P4),
    third(Age,P3),
    second(Age,P2),
    first(Age,P1)

    chesswin(P4,p(danin,_)),
    chesswin(P4,p(borisov,_)),
    chesswin(P4,p(_,builder)),

    theater(P4,_),

    first(Age,M),
    skiwin(M,_).
*/
