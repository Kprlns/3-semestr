import re

inputFile = open("file", "r")
outputFile = open("out.pro", "w")
printed = 0
hasChild = 1

data = {}

male = []
female = []
married = []

currentIndex = ""
currentName = ""
currentFather = ""
currentMother = ""

for string in inputFile:

    parsedLine1 = re.search(r"^0\s+@I(\d+)@\s+INDI$", string)  # получаем id человека
    if not (parsedLine1 is None):
        currentIndex = parsedLine1.group(1)
        continue

    parsedLine2 = re.search(r"^1\s+NAME\s+(.+)\s+/(.+)/$", string)  # имя и фамилия
    if not (parsedLine2 is None):
        currentName = parsedLine2.group(1) + " " + parsedLine2.group(2)
        data[currentIndex] = currentName
        continue

    parsedLine3 = re.search(r"^1\s+SEX\s+([F|M])$", string)  # пол
    if not (parsedLine3 is None):
        if parsedLine3.group(1) == "F":
            # print("female( \"{0}\" ).".format(currentName))
            # outputFile.write("female( \'{0}\ ).\n".format(currentName))
            female.append(currentName)
        elif parsedLine3.group(1) == "M":
            # print("male( \"{0}\" ).".format(currentName))
            # outputFile.write("male( \'{0}\' ).\n".format(currentName))
            male.append(currentName)
        continue

    lineHelper = re.search(r"0\s+@F(\d+)@ FAM", string)  # отделение каждой семьи переводом строки
    if not (lineHelper is None):
        if not printed:
            printed = 1
            for i in range(len(male)):
                outputFile.write("male( \'{0}\' ).\n".format(male[i]))
            outputFile.write("\n")
            for i in range(len(female)):
                outputFile.write("female( \'{0}\' ).\n".format(female[i]))
            outputFile.write("\n")
        if not hasChild:
            tmp = "married( \'" + currentFather + "\', \'" + currentMother + "\' ).\n"
            married.append(tmp)
        hasChild = 0

        outputFile.write("\n")
        continue

    parsedLine4 = re.search(r"1\s+HUSB\s+@I(\d+)@", string)  # получение id мужа
    if not (parsedLine4 is None):
        currentFather = data[parsedLine4.group(1)]
        continue

    parsedLine5 = re.search(r"1\s+WIFE\s+@I(\d+)@", string)  # получение id ребенка
    if not (parsedLine5 is None):
        currentMother = data[parsedLine5.group(1)]
        continue

    parsedLine6 = re.search(r"1\s+CHIL\s+@I(\d+)@", string)  # запись ребенка
    if not (parsedLine6 is None):
        # print("child( \"{0}\", \"{1}\" ).".format(data[parsedLine6.group(1)], currentMother))
        # print("child( \"{0}\", \"{1}\" ).".format(data[parsedLine6.group(1)], currentFather))
        hasChild = 1
        outputFile.write("child( \'{0}\', \'{1}\' ).\n".format(data[parsedLine6.group(1)], currentMother))
        outputFile.write("child( \'{0}\', \'{1}\' ).\n".format(data[parsedLine6.group(1)], currentFather))
        continue


if not hasChild:
    tmp = "married( \'" + currentFather + "\', \'" + currentMother + "\' ).\n"
    married.append(tmp)

for i in range(len(married)):
    outputFile.write(married[i])

inputFile.close()
outputFile.close()

