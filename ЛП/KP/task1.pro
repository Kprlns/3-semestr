male( 'Albus Severus Potter' ).
male( 'Unknown Potter' ).
male( 'James Potter' ).
male( 'Percy Ignatius Weasley' ).
male( 'Ronald Bilius Weasley' ).
male( 'Dudley Dursley' ).
male( 'Harry James Potter' ).
male( 'Arthur Weasley' ).
male( 'Unknown Evans' ).
male( 'Charles Weasley' ).
male( 'George Weasley' ).
male( 'Vernon Dursley' ).
male( 'James Sirius Potter' ).
male( 'William Arthur Weasley' ).
male( 'Fred Weasley' ).

female( 'Ginerva Molly Weasley' ).
female( 'Molly Prewett' ).
female( 'Unknown1 Unknown1' ).
female( 'Lily Luna Potter' ).
female( 'Unknown2 Unknown2' ).
female( 'Lily Evans' ).
female( 'Petunia Evans' ).
female( 'Hermione Jean Granger' ).

child( 'Lily Evans', 'Unknown1 Unknown1' ).
child( 'Lily Evans', 'Unknown Evans' ).
child( 'Petunia Evans', 'Unknown1 Unknown1' ).
child( 'Petunia Evans', 'Unknown Evans' ).

child( 'James Potter', 'Unknown2 Unknown2' ).
child( 'James Potter', 'Unknown Potter' ).

child( 'Albus Severus Potter', 'Ginerva Molly Weasley' ).
child( 'Albus Severus Potter', 'Harry James Potter' ).
child( 'James Sirius Potter', 'Ginerva Molly Weasley' ).
child( 'James Sirius Potter', 'Harry James Potter' ).
child( 'Lily Luna Potter', 'Ginerva Molly Weasley' ).
child( 'Lily Luna Potter', 'Harry James Potter' ).

child( 'Ginerva Molly Weasley', 'Molly Prewett' ).
child( 'Ginerva Molly Weasley', 'Arthur Weasley' ).
child( 'William Arthur Weasley', 'Molly Prewett' ).
child( 'William Arthur Weasley', 'Arthur Weasley' ).
child( 'Charles Weasley', 'Molly Prewett' ).
child( 'Charles Weasley', 'Arthur Weasley' ).
child( 'Percy Ignatius Weasley', 'Molly Prewett' ).
child( 'Percy Ignatius Weasley', 'Arthur Weasley' ).
child( 'Fred Weasley', 'Molly Prewett' ).
child( 'Fred Weasley', 'Arthur Weasley' ).
child( 'George Weasley', 'Molly Prewett' ).
child( 'George Weasley', 'Arthur Weasley' ).
child( 'Ronald Bilius Weasley', 'Molly Prewett' ).
child( 'Ronald Bilius Weasley', 'Arthur Weasley' ).

child( 'Dudley Dursley', 'Vernon Dursley' ).
child( 'Dudley Dursley', 'Petunia Evans' ).

child( 'Harry James Potter', 'Lily Evans' ).
child( 'Harry James Potter', 'James Potter' ).

married( 'Ronald Bilius Weasley', 'Hermione Jean Granger' ).
%брат жены – ШУРИН
shurin(Y,X):- %X- шурин для Y
  male(X),!,
  male(Y),
  check(Z,X,wife),
  check(Z,Y,sister).
/*         отношения родства          */
check(X,Y,father):-
  child(Y,X),
  male(X).

check(X,Y,mother):-
  child(Y,X),
  female(X).

check(X,Y,sister):-
  female(X),
  child(X,Z),
  child(Y,Z),
  X \= Y.

check(X,Y,brother):-
  male(X),
  child(X,Z),
  child(Y,Z),
  X \= Y.

check(X,Y,son):-
  child(X,Y),
  male(X).

check(X,Y,daughter):-
  child(X,Y),
  female(X).

check(X,Y,husband):-
  male(X),
  female(Y),
  child(Z,X),
  child(Z,Y),!.

check(X,Y,husband):-
  married(X,Y).

check(X,Y,wife):-
  female(X),
  male(Y),
  child(Z,X),
  child(Z,Y),!.

check(X,Y,wife):-
  married(Y,X).

check_relation(X):-
  member(X,[father,mother,sister,brother,son,daughter,husband,wife]).


/*          поиск          */
relative1(X,Y,Res):- % цепочка людей, через которых связаны 2 человека
  search_bdth(X,Y,Res).

ask_relative(X,Y,Res):- % цепочка родства, через которую связаны 2 человека
  check_relation(Res),!,
  check(X,Y,Res).

relative(X,Y,Res):-
  search_bdth(X,Y,Res1),!,
  transform(Res1,Res).

transform([_],[]):-!. %переделевает цепочку родственников в цепочку родства
transform([First,Second|Tail],ResList):-
  length([First,Second|Tail],B),
  A is B - 1,
  length(ResList,A),
  check(First,Second,Relation),
  %append(Relation,Tmp,ResList),
  ResList = [Relation|Tmp],
  transform([Second|Tail],Tmp),!.

prolong([X|T],[Y,X|T]):-
	move(X,Y),
  \+ member(Y,[X|T]).

move(X,Y):-
  check(X,Y,_).

search_bdth(X,Y,P):-
	bdth([[X]],Y,L),
  reverse(L,P).

bdth([[X|T]|_],X,[X|T]).
bdth([P|QI],X,R):-
  findall(Z,prolong(P,Z),T),
	append(QI,T,Q0),
  bdth(Q0,X,R),!.

bdth([_|T],Y,L):-
	bdth(T,Y,L).

%          обработка вопросов          %
questionWord(X):-
  member(X,[how,who]).

quantity(X):-
  member(X,[much,many]).

purals(X):-
  member(X,[sisters,brothers,sons,daughters,husbands,wives]).
pural(sister,sisters).
pural(brother,brothers).
pural(son,sons).
pural(daughter,daughters).
pural(husband,husbands).
pural(wife,wives).

helpWord(X):-
  member(X,[do,does]).

haveHas(X):-
  member(X,[have,has]).

is(X):-
  member(X,[is]).

particle(X):-
  member(X,["'s"]).

questionSign(X):-
  member(X,['?']).

hisHer(X):-
  member(X,[his,her,he,she]).

/*          вопросы          */
askQuestion(List):-  %how many brothers does '*name*' have ?
  List = [A,B,C,D,E,F,H],
  questionWord(A),
  quantity(B),
  purals(C),
  helpWord(D),
  (male(E);female(E)),
  nb_setval(lastName,E),
  haveHas(F),
  questionSign(H),

  pural(C1,C),
  setof(X,ask_relative(X,E,C1),T),
  length(T,Res),!,
  write(E),
  write(" have "),
  ((Res =:= 1,write(Res),write(" "),write( C1));(\+(Res =:= 1),write(Res),write(" "),write( C))),!.

askQuestion(List):- %how many brothers does he have ?
  List = [A,B,C1,D,E1,F,H],
  questionWord(A),
  quantity(B),
  purals(C1),
  helpWord(D),
  hisHer(E1),
  nb_getval(lastName,E),
  haveHas(F),
  questionSign(H),

  pural(C,C1),
  setof(X,ask_relative(X,E,C),T),
  length(T,Res),
  write(E),
  write(" have "),
  ((Res =:= 1,write(Res),write(" "),write(C));(\+(Res =:= 1),write(Res),write(" "),write(C1))),!.

askQuestion(List):-% who is '*name*' brother?
  List = [A,B,C,D,E,F],
  questionWord(A),
  is(B),
  (male(C);female(C)),
  nb_setval(lastName,C),
  particle(D),
  check_relation(E),
  questionSign(F), !,
  check(Res,C,E),
  write(Res),write(" is "), write(C),write("'s "),write(E).

askQuestion(List):- %who is her brother
  List = [A,B,C1,D,E],
  questionWord(A),
  is(B),
  hisHer(C1),
  nb_getval(lastName,C),
  %print(C),
  check_relation(D),
  questionSign(E),!,
  check(Res,C,D),
  write(Res),write(" is "), write(C),write("'s "),write(D).

askQuestion(List):- % is '*name*' '*name*' 's son?
  List = [A,B,C,D,E,F],
  nb_setval(lastName,C),
  is(A),
  (male(B);female(B)),
  (male(C);female(C)),
  particle(D),
  check_relation(E),
  questionSign(F),
  check(B,C,E),
  !.

askQuestion(List):-
  List = [A,B,C1,D,E],
  is(A),
  (male(B);female(B)),
  hisHer(C1),
  check_relation(D),
  questionSign(E),

  nb_getval(lastName,C),
  check(B,C,D),
  !.
/*          анализ предложений          */
% who is '*name*' 's brother?
%how many brothers does '*name*' have ?
% is '*name*' '*name*' 's son ?

analysis(List,Res):-
  append(L1,L2,List),
  questionType(L1,Q),
  append(M1,M2,L2),
  name(M1,Q1),
  append(N1,N2,M2),
  relationship(N1,Q2),
  questionsign(N2,Q3),
  Res = sentence(Q,Q1,Q2,Q3).

analysis(List,Res):-
  List = [A,B,C,D,E,F,G],
  questionType([A,B],Q),
  relationship([C],Q1),
  helpword([D],Q2),
  name([E],Q3),
  have_Has([F],Q4),
  questionsign([G],Q5),
  Res = sentence(Q,Q1,Q2,Q3,Q4,Q5).

relationship([X],Res):-
  check_relation(X),
  Res = relationship_(X).

relationship([X],Res):-
  purals(X),
  Res = relationship_(X).

helpword([X],Res):-
  helpWord(X),
  Res = helpword_(X).

have_Has([X],Res):-
  haveHas(X),
  Res = have_has(X).

questionType(L,question_type(X,Y)):-
  questionWord(X1),
  is(Y1),
  L = [X1,Y1],
  X = question_word(X1),
  Y = is_(Y1).

questionType(L,question_type(X,Y)):-
  questionWord(X1),
  quantity(Y1),
  L = [X1,Y1],
  X = question_word(X1),
  Y = much_many(Y1).

questionType(L,question_type(X)):-
  is(X1),
  L = [X1],
  X = is_(X1).

name(L,names(X)):-
  L = [X1,Y1],
  ((male(X1);female(X1)); (hisHer(X1))),
  particle(Y1),
  X = object(X1,Y1).


name(L,names(X)):-
  L = [X1],
  ((male(X1);female(X1)); (hisHer(X1))),
  X = object(X1).

name(L,names(X,Y)):-
  L = [X1,Y1,Z],
  (male(X1);female(X1)),
  ((male(Y1);female(Y1)) ;(hisHer(Y1))),
  particle(Z),
  X = subject(X1),
  Y = object(Y1,Z).

questionsign([X],Res):-
  questionSign(X),
  Res = question_sign(X).
%
