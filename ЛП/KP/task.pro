male( "Albus Severus Potter" ).
female( "Ginerva Molly Weasley" ).
male( "Unknown Potter" ).
female( "Molly Prewett" ).
male( "James Potter" ).
male( "Percy Ignatius Weasley" ).
female( "Unknown1 Unknown1" ).
male( "Ronald Bilius Weasley" ).
male( "Dudley Dursley" ).
male( "Harry James Potter" ).
male( "Arthur Weasley" ).
male( "Unknown Evans" ).
male( "Charles Weasley" ).
female( "Lily Luna Potter" ).
male( "George Weasley" ).
male( "Vernon Dursley" ).
female( "Unknown2 Unknown2" ).
male( "James Sirius Potter" ).
male( "William Arthur Weasley" ).
female( "Lily Evans" ).
male( "Fred Weasley" ).
female( "Petunia Evans" ).
female( "Hermione Jean Granger" ).

child( "Lily Evans", "Unknown1 Unknown1" ).
child( "Lily Evans", "Unknown Evans" ).
child( "Petunia Evans", "Unknown1 Unknown1" ).
child( "Petunia Evans", "Unknown Evans" ).

child( "James Potter", "Unknown2 Unknown2" ).
child( "James Potter", "Unknown Potter" ).

child( "Albus Severus Potter", "Ginerva Molly Weasley" ).
child( "Albus Severus Potter", "Harry James Potter" ).
child( "James Sirius Potter", "Ginerva Molly Weasley" ).
child( "James Sirius Potter", "Harry James Potter" ).
child( "Lily Luna Potter", "Ginerva Molly Weasley" ).
child( "Lily Luna Potter", "Harry James Potter" ).

child( "Ginerva Molly Weasley", "Molly Prewett" ).
child( "Ginerva Molly Weasley", "Arthur Weasley" ).
child( "William Arthur Weasley", "Molly Prewett" ).
child( "William Arthur Weasley", "Arthur Weasley" ).
child( "Charles Weasley", "Molly Prewett" ).
child( "Charles Weasley", "Arthur Weasley" ).
child( "Percy Ignatius Weasley", "Molly Prewett" ).
child( "Percy Ignatius Weasley", "Arthur Weasley" ).
child( "Fred Weasley", "Molly Prewett" ).
child( "Fred Weasley", "Arthur Weasley" ).
child( "George Weasley", "Molly Prewett" ).
child( "George Weasley", "Arthur Weasley" ).
child( "Ronald Bilius Weasley", "Molly Prewett" ).
child( "Ronald Bilius Weasley", "Arthur Weasley" ).

child( "Dudley Dursley", "Vernon Dursley" ).
child( "Dudley Dursley", "Petunia Evans" ).

child( "Harry James Potter", "Lily Evans" ).
child( "Harry James Potter", "James Potter" ).

%брат жены – ШУРИН
shurin(X,Y):- %X- шурин для Y
  male(Y),
  child(Y,Z),
  child(Ywife,Z),
  female(Ywife),

  child(Parent,Ywife),
  child(Parent,X).
